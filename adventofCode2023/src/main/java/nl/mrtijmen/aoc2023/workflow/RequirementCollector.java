package nl.mrtijmen.aoc2023.workflow;

import java.util.*;

public class RequirementCollector {

    private final Map<String, RuleSet> router = new HashMap<>();


    private final List<RequirementNode> heads = new ArrayList<>();

    public RequirementCollector(List<RuleSet> rules) {
        rules.forEach(r -> router.put(r.name(), r));


    }

    public void startInspect() {
        inspect(RequirementNode.root(), "in");


    }

    private void inspect(RequirementNode node, String target) {
        if (target.equals("A")) {
            heads.add(node);
            return;
        }
        if (target.equals("R")) {
            return;
        }

        RuleSet next = router.get(target);
        inspectRulesSet(node, next);
    }

    private void inspectRulesSet(RequirementNode node, RuleSet set) {


        var rules = set.rules();
        for (WorkflowRule rule : rules) {
            InspectablePredicate predicate = rule.operator();
            var posNode = RequirementNode.positive(node, predicate);


            inspect(posNode, rule.target());

            node = RequirementNode.negative(node, predicate);
        }
    }

    public List<RequirementNode> getHeads() {
        return heads;
    }

    private static class RequirementSet {
        private final EnumMap<GearProperty, List<InspectablePredicate>> requirements;

        private RequirementSet() {
            requirements = new EnumMap<>(GearProperty.class);
            for (GearProperty property : GearProperty.values()) {
                requirements.put(property, new ArrayList<>());
            }
        }
    }


}
