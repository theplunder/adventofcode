package nl.mrtijmen.aoc2023.workflow;

import java.util.function.Predicate;

import static nl.mrtijmen.aoc2023.workflow.InspectablePredicate.Type.*;

public class InspectablePredicate implements Predicate<Part> {

    Predicate<Part> predicate;
    GearProperty property;
    int value;

    Type type;

    static enum Type {GT, LT, ALL}

    private InspectablePredicate(Predicate<Part> predicate, GearProperty property, int value, Type type) {
        this.predicate = predicate;
        this.property = property;
        this.value = value;
        this.type = type;
    }

    public static InspectablePredicate acceptAll() {
        return new InspectablePredicate(part -> true, null, -1, ALL);
    }

    public static InspectablePredicate greaterThan(GearProperty property, int value) {
        return new InspectablePredicate(part -> (part.property(property) > value), property, value, GT);
    }

    public static InspectablePredicate lesserThan(GearProperty property, int value) {
        return new InspectablePredicate(part -> part.property(property) < value, property, value, LT);
    }

    @Override
    public String toString() {
        return "InspectablePredicate{" +
                property + " " + type + " " + value + '}';
    }

    @Override
    public boolean test(Part part) {
        return predicate.test(part);
    }
}
