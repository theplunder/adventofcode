package nl.mrtijmen.aoc2023.workflow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WorkflowRouter {

    private final Map<String, RuleSet> router = new HashMap<>();

    private final List<Part> acceptBucket = new ArrayList<>();

    private final List<Part> rejectBucket = new ArrayList<>();

    public WorkflowRouter(List<RuleSet> rules) {
        rules.forEach(r -> router.put(r.name(), r));
    }

    public void process(Part part) {
        String next = "in";
        while (true) {
            next = router.get(next).apply(part);
            if (next.equals("A")) {
                acceptBucket.add(part);
                break;
            }
            if (next.equals("R")) {
                rejectBucket.add(part);
                break;
            }

        }
    }

    public List<Part> getAcceptBucket() {
        return acceptBucket;
    }

    public List<Part> getRejectBucket() {
        return rejectBucket;
    }
}
