package nl.mrtijmen.aoc2023.workflow;


public class RequirementNode {
    RequirementNode previous;

    InspectablePredicate truePred;
    InspectablePredicate falsePred;
    boolean isRoot = false;

    private RequirementNode(RequirementNode previous) {
        this.previous = previous;
    }

    public static RequirementNode root() {
        var node = new RequirementNode(null);
        node.isRoot = true;
        return node;
    }

    public static RequirementNode positive(RequirementNode previous, InspectablePredicate pred) {
        var node = new RequirementNode(previous);
        node.truePred = pred;
        return node;
    }

    public static RequirementNode negative(RequirementNode previous, InspectablePredicate pred) {
        var node = new RequirementNode(previous);
        node.falsePred = pred;
        return node;
    }

    @Override
    public String toString() {
        return "RequirementNode{" +
                "previous=" + previous +
                ", truePred=" + truePred +
                ", falsePred=" + falsePred +
                ", isRoot=" + isRoot +
                '}';
    }
}