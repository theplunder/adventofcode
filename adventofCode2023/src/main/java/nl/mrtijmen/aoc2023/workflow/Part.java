package nl.mrtijmen.aoc2023.workflow;

public record Part(int x, int m, int a, int s) {


    public int property(GearProperty property) {
        return switch (property) {
            case X -> x;
            case M -> m;
            case A -> a;
            case S -> s;
        };

    }

    public long score() {
        return x + m + a + s;

    }

}
