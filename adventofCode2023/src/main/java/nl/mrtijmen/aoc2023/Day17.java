package nl.mrtijmen.aoc2023;

import nl.mrtijmen.aoc2023.crucible.*;
import nl.mrtijmen.aoccommons.util.FileNameUtil;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;
import nl.mrtijmen.aoccommons.util.ParseUtil;
import nl.mrtijmen.aoccommons.util.grid.Coordinate;
import nl.mrtijmen.aoccommons.util.grid.Grid;

import java.util.List;

public class Day17 {
    private static Grid<Integer> heatLossGrid;

    public static void main(String[] args) throws Exception {
    //     List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName());
        //       List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName(1));
        //List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName(2));

        //   List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName(3));

        List<String> data = FileReaderUtil.getStringList(FileNameUtil.dataFileName());


        var valueGrid = data.stream().map(l -> ParseUtil.lineToNumberList(l, "")).toList();


        heatLossGrid = new Grid<>(valueGrid);
        Coordinate target = new Coordinate(heatLossGrid.xSize() - 1, heatLossGrid.ySize() - 1);


        var traveller = new CrucibleDijkstraNextVersion(heatLossGrid);
        var result = traveller.calculateValueGrid(target);
        int result1 = result.getValue(target);
        System.out.println("Result Part 1 --> " + result1);


//        var roadTraveller = new RoadNodeCrucible(heatLossGrid);
//        var rest = roadTraveller.fillGrid(target);
//        System.out.println(rest);
//        int test2 = result.getValue(target);
//        System.out.println("Result Part 1 --> " + result1);
//
        var ultratraveller = new UltraCrucibleDijkstra(heatLossGrid);

        var travelResult = ultratraveller.calculateValueGrid(target);
        int result2 = travelResult.getValue(target);
        System.out.println("Result Part 2 --> " + result2);
    }


}