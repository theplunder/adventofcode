package nl.mrtijmen.aoc2023;

import nl.mrtijmen.aoc2023.numbers.Number;
import nl.mrtijmen.aoccommons.util.FileNameUtil;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;

import java.util.List;
import java.util.Optional;

public class Day01
{

    public static void main(String[] args) throws Exception
    {
        //List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName(1));
       // List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName(2));
         List<String> data = FileReaderUtil.getStringList(FileNameUtil.dataFileName());
    //    System.out.println(data);


          part1(data);

        int result2 = data.stream()
                          .map(Day01::findNumbers2)
                      //    .peek(System.out::println)
                          //    .map(Day01::filterChars)
                          .map(Day01::combineFirstAndLast)
                          .mapToInt(Integer::parseInt)
                   //       .peek(System.out::println)
                          .sum();
        System.out.println("Result part 2 -> " + result2);


    }

    private static void part1(List<String> data)
    {
        int result1 = data.stream()
                          .map(Day01::filterChars)
                          .map(Day01::combineFirstAndLast)
                          .mapToInt(Integer::parseInt)
                          .peek(System.out::println)
                          .sum();
        System.out.println("Result part 1 -> " + result1);
    }

    private static String filterChars(String string)
    {
        return string.replaceAll("\\D", "");
    }


    private static String findNumbers2(String string)
    {

        StringBuilder result = new StringBuilder();
        for (int i = 0; i < string.length(); i++)
        {
            if (Character.isDigit(string.charAt(i)))
            {
                result.append(string.charAt(i));
            } else

            {
                String subString = string.substring(i);
                for (Number number : Number.values())
                {
                    if (subString.startsWith(number.toWord()))
                    {
                        result.append(number.getValue());
                    }
                }
            }
        }
        return result.toString();
    }


    //Safe, because it isn't going to be fooled by eightwothree for example!
    private static String safeReplaceAllWrittenNumbers(String string)
    {
        System.out.println("Replacing in -> " + string);

        String previous;
        do
        {
            previous = string;
            string = replaceFirstWrittenNumbers(string);
        }
        while (!previous.equals(string));
        System.out.println(" No more numbers found -> " + string);

        return string;

    }


    private static String replaceFirstWrittenNumbers(String string)
    {
        for (int i = 2; i <= string.length(); i++)
        {
            String subString = string.substring(0, i);
            Optional<Number> foundNumber = findAnyNumber(subString);

            if (foundNumber.isPresent())
            {
                // System.out.println("found " + foundNumber.get() + " in Substring: " + subString);
                return string.replaceFirst(foundNumber.get().toWord(), String.valueOf(foundNumber.get()
                                                                                                 .getValue()));
            }

        }
        return string;
    }

    private static Optional<Number> findAnyNumber(String string)
    {
        for (Number number : Number.values())
        {
            if (string.contains(number.toWord()))
            {
                return Optional.of(number);
            }
        }
        return Optional.empty();
    }


    private static String combineFirstAndLast(String string)
    {
        return "" + string.charAt(0) + string.charAt(string.length() - 1);
    }

    private static Integer toNumber(String string)
    {
        return Integer.parseInt(string);
    }


}
