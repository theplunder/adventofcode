package nl.mrtijmen.aoc2023;

import nl.mrtijmen.aoccommons.util.FileNameUtil;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;
import nl.mrtijmen.aoccommons.util.ParseUtil;
import nl.mrtijmen.aoccommons.util.grid.Coordinate;
import nl.mrtijmen.aoccommons.util.grid.Direction;
import nl.mrtijmen.aoccommons.util.grid.Grid;

import java.util.*;

public class Day14
{


    public static List<Coordinate> walls;
    public static Grid<Cave> cave;


    public static void main(String[] args) throws Exception
    {
        Coordinate.UP_IS_NEGATIVE_Y = true;

        //  List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName());
        List<String> data = FileReaderUtil.getStringList(FileNameUtil.dataFileName());

        var parsed = data.stream().map(l -> ParseUtil.lineToObject(l, "", Cave::intialize)).toList();
        cave = new Grid<>(parsed);

        System.out.println(cave);

        walls = cave.find(Cave.WALL::equals);

        moveAllRocksUp();
        //      System.out.println(cave);

        long result1 = calculateLoad();
        System.out.println("Result part 1 --> " + result1);

        //RESET
        cave = new Grid<>(parsed);
        cycle(1000000000, true);
//        System.out.println(cave);
        long result2 = calculateLoad();
        System.out.println("Result part 2 --> " + result2);
    }

    private static Map<String, Long> caching = new HashMap<>();

    public static void cycle(long cyles, boolean doCaching)
    {
        long times = 4 * cyles;
        for (int i = 0; i < times; i++)
        {
            int cycleNumber = i / 4;
            // System.out.println(i + " cycle: " + cycleNumber);
            switch (i % 4)
            {
                case 0 -> moveAllRocksUp();
                case 1 -> moveAllRocksLeft();
                case 2 -> moveAllRocksDown();
                case 3 -> moveAllRocksRight();
            }

            if (doCaching && (i % 4 == 3))
            {
                long afterCycles = cycleNumber + 1;
                String caveString = cave.toString();
                if (caching.containsKey(caveString))
                {
                    long lastEqualPosition = caching.get(caveString);
                    System.out.println("Loop after " + afterCycles + " cycles");
                    System.out.println("Previous after  " + lastEqualPosition + " cycles");

                    System.out.println("Loop length is " + (afterCycles - lastEqualPosition) + " cycles");

                    System.out.println("therefore");

                    //  System.out.println(caveString);

                    System.out.println("should be equal to: ");
                    cycle((afterCycles - lastEqualPosition), false);

                    System.out.println("ASSUMPTION STILL VALID?: " + caveString.equals(cave.toString()));

                    // System.out.println(cave);

                    long moreStepsToGo = (cyles - afterCycles) % (afterCycles - lastEqualPosition);
                    System.out.println("do " + moreStepsToGo + " more boxHash");
                    cycle(moreStepsToGo, false);

                    System.out.println("validate");
                    caveString = cave.toString();
                    long cycle = caching.get(caveString);
                    System.out.println("cycle is " + cycle);

                    return;
                } else
                {
                    caching.put(caveString, afterCycles);
                }
            }


        }

    }

    public static long calculateLoad()
    {
        var rocks = cave.find(Cave.ROCK::equals);
        int height = cave.ySize();

        return rocks.stream().mapToLong(Coordinate::y).map(y -> height - y).sum();
    }

    public static void moveAllRocksUp()
    {
        var rocks = new ArrayList<>(cave.find(Cave.ROCK::equals));
        rocks.sort(topToBottom.thenComparing(leftToRight));

        for (Coordinate rock : rocks)
        {
            var newPosition = findNewRockPosition(rock, Direction.UP);
            cave.setValue(rock, Cave.DIRT);
            cave.setValue(newPosition, Cave.ROCK);

        }
    }

    public static void moveAllRocksLeft()
    {
        var rocks = new ArrayList<>(cave.find(Cave.ROCK::equals));
        rocks.sort(leftToRight.thenComparing(topToBottom));

        for (Coordinate rock : rocks)
        {
            var newPosition = findNewRockPosition(rock, Direction.LEFT);
            cave.setValue(rock, Cave.DIRT);
            cave.setValue(newPosition, Cave.ROCK);

        }

    }

    public static void moveAllRocksRight()
    {
        var rocks = new ArrayList<>(cave.find(Cave.ROCK::equals));
        rocks.sort(leftToRight.reversed().thenComparing(topToBottom));

        for (Coordinate rock : rocks)
        {
            var newPosition = findNewRockPosition(rock, Direction.RIGHT);
            cave.setValue(rock, Cave.DIRT);
            cave.setValue(newPosition, Cave.ROCK);
        }
    }

    public static void moveAllRocksDown()
    {
        var rocks = new ArrayList<>(cave.find(Cave.ROCK::equals));
        rocks.sort(topToBottom.reversed().thenComparing(leftToRight.reversed()));

        for (Coordinate rock : rocks)
        {
            var newPosition = findNewRockPosition(rock, Direction.DOWN);
            cave.setValue(rock, Cave.DIRT);
            cave.setValue(newPosition, Cave.ROCK);
        }
    }

    private static Coordinate findNewRockPosition(Coordinate startPoint, Direction direction)
    {
        var moved = startPoint.move(direction);
        Coordinate current = startPoint;
        while (cave.coordinateWithinRange(moved) && cave.getValue(moved).equals(Cave.DIRT))
        {
            current = moved;
            moved = current.move(direction);
        }
        return current;
    }

    static Comparator<Coordinate> topToBottom = Comparator.comparingLong(Coordinate::y);

    static Comparator<Coordinate> leftToRight = Comparator.comparingLong(Coordinate::x);


    public enum Cave
    {
        ROCK("O"), WALL("#"), DIRT(".");

        String token;

        Cave(String token)
        {
            this.token = token;
        }

        public static Cave intialize(String fromToken)
        {
            return Arrays.stream(Cave.values()).filter(t -> t.token.equals(fromToken)).findFirst().orElseThrow();
        }

        @Override
        public String toString()
        {
            return token;
        }
    }

    private record KnownResult(String cave)
    {


    }

}