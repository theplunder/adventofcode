package nl.mrtijmen.aoc2023.hail;

import nl.mrtijmen.aoccommons.util.grid.Coordinate3D;

public record HailStone(Coordinate3D initialPosition, Coordinate3D dirVector, Integer identifier)
{
}
