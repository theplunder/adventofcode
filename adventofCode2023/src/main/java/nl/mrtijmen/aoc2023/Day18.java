package nl.mrtijmen.aoc2023;

import nl.mrtijmen.aoccommons.util.FileNameUtil;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;
import nl.mrtijmen.aoccommons.util.ParseUtil;
import nl.mrtijmen.aoccommons.util.grid.Coordinate;
import nl.mrtijmen.aoccommons.util.grid.Direction;
import nl.mrtijmen.aoccommons.util.grid.Grid;
import nl.mrtijmen.aoccommons.util.object.Pair;

import java.util.*;

import static nl.mrtijmen.aoccommons.util.grid.Direction.*;

public class Day18
{


    record Instruction(Direction direction, long length, String color)
    {

        Instruction corrrect()
        {
            long newLength = Long.parseLong(color.substring(2, 7), 16);
            Direction newDir = switch (color.charAt(7))
            {
                case '0' -> RIGHT;
                case '1' -> DOWN;
                case '2' -> LEFT;
                case '3' -> UP;
                default -> throw new IllegalArgumentException("cannot be!");
            };


            return new Instruction(newDir, newLength, color);
        }

    }


    public static void main(String[] args) throws Exception
    {
        Coordinate.UP_IS_NEGATIVE_Y = true;
        //List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName());
        //List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName(2));
        List<String> data = FileReaderUtil.getStringList(FileNameUtil.dataFileName());

        List<Instruction> instructions = data.stream()
                                             .map(l -> ParseUtil.lineToStringList(l, " "))
                                             .map(l -> new Instruction(toDirection(l.get(0)), Integer.parseInt(l.get(1)), l.get(2)))
                                             .toList();


        List<Coordinate> circuventions = new ArrayList<>();
        var current = new Coordinate(0, 0);
        long xMin = Integer.MAX_VALUE;
        long yMin = Integer.MAX_VALUE;
        long xMax = Integer.MIN_VALUE;
        long yMax = Integer.MIN_VALUE;


        for (Instruction instr : instructions)
        {
            circuventions.add(current);
            current = instr.direction.travel(current, instr.length);
            xMin = Math.min(xMin, current.x());
            yMin = Math.min(yMin, current.y());
            xMax = Math.max(xMax, current.x());
            yMax = Math.max(yMax, current.y());
        }

        System.out.println(xMin + " " + yMin + " " + xMax + " " + yMax);
        final var xMinF = xMin;
        final var yMinF = yMin;

        circuventions = circuventions.stream().map(c -> new Coordinate(c.x() - xMinF, c.y() - yMinF)).toList();
        var newStart = circuventions.get(0);

        Grid<String> grid = new Grid<>((int) (xMax - xMin) + 1, (int) (yMax - yMin) + 1, ".");

        var wall = newStart;
        List<Coordinate> edges = new ArrayList<>();
        for (Instruction instr : instructions)
        {
            grid.setValue(wall, "#");
            for (int i = 0; i < instr.length(); i++)
            {
                wall = instr.direction.travel(wall);
                grid.setValue(wall, "#");
                edges.add(wall);
            }
        }

        Set<Coordinate> inside = floodFill(grid, new Coordinate(newStart.x() + 1, newStart.y() + 1));

        int result = inside.size() + edges.size();
        System.out.println("Result part 1 --> " + result);


        var correctedInstructions = instructions.stream().map(Instruction::corrrect).toList();
        System.out.println(correctedInstructions);

        List<Coordinate> newLoop = new ArrayList<>();

        for (Instruction instr : correctedInstructions)
        {
            newLoop.add(current);
            current = instr.direction.travel(current, instr.length);
        }


        long result2 = blockApproachOriginal(newLoop);

        System.out.println("Result Part 2 --> " + result2);
    }


    private static Set<Coordinate> floodFill(Grid<String> grid, Coordinate start)
    {
        LinkedList<Coordinate> unchecked = new LinkedList<>();
        List<Coordinate> visited = new ArrayList<>();
        Set<Coordinate> inner = new HashSet<>();
        unchecked.add(start);

        while (!unchecked.isEmpty())
        {
            var check = unchecked.pop();
            unchecked.remove(check);
            visited.add(check);
            inner.add(check);
            check.findNeighbours()
                 .stream()
                 .filter(c -> grid.getValue(c) == ".")
                 .filter(c -> !visited.contains(c))
                 .forEach(unchecked::add);

        }
        return inner;
    }


    private static long blockApproachOriginal(List<Coordinate> circuventions)
    {

//the slightly more complicated version
        circuventions = circuventions.stream().sorted(leftToRight.thenComparing(topToBottom)).toList();
        circuventions.stream().forEach(System.out::println);
        List<Long> interestingXValues = circuventions.stream().map(Coordinate::x).distinct().sorted().toList();

        List<Pair<Long, Long>> startEndPairs = new ArrayList<>(); //only Y coordinates


        long previousInterestingChangepoint = -1;
        long totalSurface = 0;
        for (Long changePoint : interestingXValues)
        {

            System.out.println("--------");

            System.out.println(startEndPairs);
            for (var pair : startEndPairs)
            {
                //+1 because we include the edges!
                long surfaceToAdd = (pair.second() - pair.first() + 1) * (changePoint - previousInterestingChangepoint);

                System.out.println("adding: " + surfaceToAdd);
                totalSurface += surfaceToAdd;
            }
            System.out.println("jump:" + (changePoint - previousInterestingChangepoint));
            System.out.println(changePoint);
            System.out.println("area: " + totalSurface);

            var pointsToProcess = circuventions.stream()
                                               .filter(c -> changePoint.equals(c.x()))
                                               .sorted(leftToRight)
                                               .toList();
            System.out.println(pointsToProcess);

            //they always come in pairs!

            for (int i = 0; i < pointsToProcess.size(); i = i + 2)
            {

                var point1 = pointsToProcess.get(i);
                var point2 = pointsToProcess.get(i + 1);

                /*
                   ---+
                      |
                   ---+
                 */
                //MERGE PAIRS

                {
                    var bottomPair = startEndPairs.stream()
                                                  .filter(pair -> pair.second().equals(point1.y())).findAny();
                    var topPair = startEndPairs.stream()
                                               .filter(pair -> pair.first().equals(point2.y())).findAny();
                    if (topPair.isPresent() && bottomPair.isPresent())
                    {
                        startEndPairs.remove(topPair.get());
                        startEndPairs.remove(bottomPair.get());
                        startEndPairs.add(new Pair<>(bottomPair.get().first(), topPair.get().second()));
                        continue;
                    }

                }
                {
                    /*
                    ---+
                       |
                    ---+
                     */

                    var pairToUpdate = startEndPairs.stream()
                                                    .filter(pair -> pair.first().equals(point1.y()) && pair.second()
                                                                                                           .equals(point2.y()))
                                                    .findAny();
                    if (pairToUpdate.isPresent())
                    {
                        startEndPairs.remove(pairToUpdate.get());
                        System.out.println("removed Pair --> " + pairToUpdate.get());

                        long correction = pairToUpdate.get().second() - pairToUpdate.get().first() + 1;
                        System.out.println("correcting with: " + correction);
                        totalSurface += correction;

                        continue;
                    }
                }

                 /*
                       |
                   +---+
                   |
                 */
                //RESIZE PAIR
                {//smaller from top
                    var pairToUpdate = startEndPairs.stream()
                                                    .filter(pair -> pair.second().equals(point2.y()))
                                                    .findAny();

                    if (pairToUpdate.isPresent())
                    {
                        startEndPairs.remove(pairToUpdate.get());
                        Pair<Long, Long> newPair = new Pair<>(pairToUpdate.get().first(), point1.y());
                        startEndPairs.add(newPair);

                        long correction = point2.y() - point1.y();
                        System.out.println("correcting with: " + correction);
                        totalSurface += correction;

                        continue;
                    }
                }
                {//bigger from top
                    var pairToUpdate = startEndPairs.stream()
                                                    .filter(pair -> pair.second().equals(point1.y())).findAny();

                    if (pairToUpdate.isPresent())
                    {
                        startEndPairs.remove(pairToUpdate.get());
                        Pair<Long, Long> updatedPair = new Pair<>(pairToUpdate.get().first(), point2.y());
                        startEndPairs.add(updatedPair);

                        continue;
                    }
                }
                { //smaller from bottom
                    var pairToUpdate = startEndPairs.stream()
                                                    .filter(pair -> pair.first().equals(point1.y())).findAny();

                    if (pairToUpdate.isPresent())
                    {
                        startEndPairs.remove(pairToUpdate.get());
                        startEndPairs.add(new Pair<>(point2.y(), pairToUpdate.get().second()));

                        long correction = point2.y() - point1.y();
                        System.out.println("correcting with: " + correction);
                        totalSurface += correction;

                        continue;
                    }
                }
                { //bigger from bottom
                    var pairToUpdate = startEndPairs.stream()
                                                    .filter(pair -> pair.first().equals(point2.y())).findAny();

                    if (pairToUpdate.isPresent())
                    {
                        startEndPairs.remove(pairToUpdate.get());
                        startEndPairs.add(new Pair<>(point1.y(), pairToUpdate.get().second()));
                        continue;
                    }
                }
                //CREATE OR SPLIT PAIR
                 /*
                   +---
                   |
                   +---
                 */
                {

                    var pairToSplit = startEndPairs.stream()
                                                   .filter(pair -> (pair.first() < point1.y() && pair.second() > point1.y()))
                                                   .findAny();
                    if (pairToSplit.isPresent())
                    {
                        startEndPairs.remove(pairToSplit.get());
                        startEndPairs.add(new Pair<>(pairToSplit.get().first(), point1.y()));
                        startEndPairs.add(new Pair<>(point2.y(), pairToSplit.get().second()));

                        long correction = point2.y() - point1.y() - 1;
                        System.out.println("correcting with: " + correction);
                        totalSurface += correction;

                        continue;
                    }
                }
                startEndPairs.add(new Pair<>(point1.y(), point2.y()));

            }//end updating pairs


            previousInterestingChangepoint = changePoint;
        }

        //we miss the last line

        return totalSurface;
    }


    private static Direction toDirection(String token)
    {
        return switch (token)
        {
            case "U" -> UP;
            case "D" -> DOWN;
            case "L" -> LEFT;
            case "R" -> RIGHT;
            default -> throw new IllegalArgumentException("Illegal Token: " + token);
        };
    }

    static Comparator<Coordinate> topToBottom = Comparator.comparingLong(Coordinate::y);

    static Comparator<Coordinate> leftToRight = Comparator.comparingLong(Coordinate::x);
}