package nl.mrtijmen.aoc2023.almanac;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class Mapping {
    private final String sourceCategory;
    private final String designationCategory;

    private final List<MappingRange> mappingRanges = new ArrayList<>();

    public Mapping(String sourceCategory, String designationCategory) {
        this.sourceCategory = sourceCategory;
        this.designationCategory = designationCategory;
    }

    public void addMapping(long destinationRangeStart, long sourceRangeStart, long rangeLength) {
        mappingRanges.add(new MappingRange(destinationRangeStart, sourceRangeStart, rangeLength));
    }

    public long map(long in) {
        for (MappingRange range : mappingRanges) {
            if (range.withinSourceRange(in)) {
                return range.map(in);
            }
        }
        return in;
    }

    public List<Range> mapRange(Range in) {

        for (MappingRange mappingRange : mappingRanges) {

            //range completely outside mapping range
            if (!mappingRange.intersects(in)) {
                continue;
            }

            //range completely within mapping range
            if (in.start() >= mappingRange.sourceRangeStart() && in.end() <= mappingRange.sourceRangeEnd()) {
                long newStart = mappingRange.map(in.start());
                long newEnd = mappingRange.map(in.end());
                return List.of(new Range(newStart, newEnd));
            }

            //range start outside, end inside
            if (in.start() < mappingRange.sourceRangeStart() && in.end() <= mappingRange.sourceRangeEnd()) {
                var split = in.splitAfter(mappingRange.sourceRangeStart() - 1);
                //range outside might still fall into another mapping range!

                //RECURSION!
                List<Range> recursed = mapRange(split.get(0));

                long newStart = mappingRange.map(split.get(1).start());
                long newEnd = mappingRange.map(split.get(1).end());

                List<Range> insideMapping = List.of(new Range(newStart, newEnd));

                return Stream.concat(recursed.stream(), insideMapping.stream()).toList();
            }

            //range start inside, end outside (always true at this point)
            if (in.start() >= mappingRange.sourceRangeStart()) {
                var split = in.splitAfter(mappingRange.sourceRangeEnd());
                //range outside might still fall into another mapping range!

                //RECURSION!
                List<Range> recursed = mapRange(split.get(1));

                long newStart = mappingRange.map(split.get(0).start());
                long newEnd = mappingRange.map(split.get(0).end());

                List<Range> insideMapping = List.of(new Range(newStart, newEnd));

                return Stream.concat(insideMapping.stream(), recursed.stream()).toList();
            }

            //range wraps around mapping range
            {
                var splitLeft = in.splitAfter(mappingRange.sourceRangeStart() - 1);
                //range outside might still fall into another mapping range!

                //RECURSION!
                List<Range> recursedLeft = mapRange(splitLeft.get(0));

                var splitRight = splitLeft.get(1).splitAfter(mappingRange.sourceRangeEnd());
                //range outside might still fall into another mapping range!

                //RECURSION!
                List<Range> recursedRight = mapRange(splitRight.get(1));

                long newStart = mappingRange.map(splitRight.get(1).start());
                long newEnd = mappingRange.map(splitRight.get(1).end());

                List<Range> insideMapping = List.of(new Range(newStart, newEnd));
                var intermediate = Stream.concat(recursedLeft.stream(), insideMapping.stream()).toList();

                return Stream.concat(intermediate.stream(), recursedRight.stream()).toList();
            }

        }
        return List.of(in);
    }


    public String getSourceCategory() {
        return sourceCategory;
    }

    public String getDesignationCategory() {
        return designationCategory;
    }

    @Override
    public String toString() {
        return "Mapping: " +
                sourceCategory + " to " + designationCategory + '\n' +
                ", mappingRanges=" + mappingRanges;
    }
}
