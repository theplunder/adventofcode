package nl.mrtijmen.aoc2023.almanac;

import java.util.Arrays;
import java.util.List;

public record Range(long start, long end) {

    public Range {
        if (start > end) {
            throw new IllegalStateException("start bigger than end! " + start + " > " + end);
        }
    }

    List<Range> splitAfter(long splitPoint) {
        return List.of(new Range(start, splitPoint), new Range(splitPoint + 1, end));
    }

}
