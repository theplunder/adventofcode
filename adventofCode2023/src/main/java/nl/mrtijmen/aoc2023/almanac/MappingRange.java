package nl.mrtijmen.aoc2023.almanac;

public record MappingRange(long destinationRangeStart, long sourceRangeStart, long rangeLength) {

    boolean withinDestignationRange(long value) {
        return value >= destinationRangeStart && value < (destinationRangeStart + rangeLength);
    }

    boolean withinSourceRange(long value) {
        return value >= sourceRangeStart && value < (sourceRangeStart + rangeLength);
    }

    long map(long in) {
        long offset = in - sourceRangeStart;
        return destinationRangeStart + offset;
    }

    long destignationRangeEnd() {
        return destinationRangeStart + rangeLength - 1;
    }

    //Inclusive
    long sourceRangeEnd() {
        return sourceRangeStart + rangeLength - 1;
    }

    public boolean intersects(Range range) {
        return !(range.end() < sourceRangeStart || range.start() > sourceRangeEnd());
    }

}
