package nl.mrtijmen.aoc2023.colourcubegame;

public record Hand(int red, int green, int blue)
{
    public int colourCount(CubeColour cubeColour)
    {
        return switch (cubeColour)
        {
            case RED -> red;
            case GREEN -> green;
            case BLUE -> blue;
        };
    }
}
