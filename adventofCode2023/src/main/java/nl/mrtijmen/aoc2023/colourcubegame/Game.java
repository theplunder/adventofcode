package nl.mrtijmen.aoc2023.colourcubegame;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;

import static java.lang.Math.max;

public class Game
{

    private final List<Hand> hands = new ArrayList<>();

    private final int id;

    public Game(int id)
    {
        this.id = id;
    }

    public void addHand(Hand hand)
    {
        hands.add(hand);
    }

    public int getId()
    {
        return id;
    }

    public EnumMap<CubeColour, Integer> minRequiredCubes()
    {
        EnumMap<CubeColour, Integer> map = new EnumMap<>(CubeColour.class);

        for (Hand hand : hands)
        {
            map.compute(CubeColour.RED, (k, v) -> maxOfColour(hand, v, k));
            map.compute(CubeColour.GREEN, (k, v) -> maxOfColour(hand, v, k));
            map.compute(CubeColour.BLUE, (k, v) -> maxOfColour(hand, v, k));
        }
        return map;


    }

    private static int maxOfColour(Hand hand, Integer v, CubeColour colour)
    {
        return (v == null) ? hand.colourCount(colour) : max(v, hand.colourCount(colour));
    }

    @Override
    public String toString()
    {
        return "Game{" +
                "Id='" + id + '\'' +
                ", hands=" + hands +
                '}';
    }
}
