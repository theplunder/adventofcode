package nl.mrtijmen.aoc2023.pulse;

import java.util.ArrayList;
import java.util.List;

import static nl.mrtijmen.aoc2023.pulse.FlipFlip.State.OFF;
import static nl.mrtijmen.aoc2023.pulse.FlipFlip.State.ON;

/**
 * Flip-flop modules (prefix %) are either on or off;
 * they are initially off. If a flip-flop module receives a high pulse,
 * it is ignored and nothing happens.
 * However, if a flip-flop module receives a low pulse,
 * it flips between on and off. If it was off, it turns on
 * and sends a high pulse. If it was on, it turns off and sends a low pulse.
 */
public class FlipFlip implements Module
{

    public final List<String> targets;

    public final String destroy = "destroy";


    private final String name;

    private State state = OFF;

    List<String> connected = new ArrayList<>();

    enum State
    {
        ON, OFF;

        public State flip()
        {
            return switch (this)
            {
                case ON -> OFF;
                case OFF -> ON;
            };
        }
    }

    public FlipFlip(String name, List<String> targets)
    {
        this.targets = targets;
        this.name = name;
    }

    public void registerConnected(String connectedModule)
    {
        connected.add(connectedModule);
    }


    @Override
    public List<PulseMessage> process(PulseMessage message)
    {
        if (message.getPulse().equals(PulseType.HIGH))
        {
            return List.of(message.route(message.getPulse(), destroy));
        }
        //then it is LOW

        PulseType newPulse;
        if (state == OFF)
        {
            state = ON;
            newPulse = PulseType.HIGH;
        } else //state is ON
        {
            state = OFF;
            newPulse = PulseType.LOW;
        }
        return targets.stream().map(t -> message.route(newPulse, t)).toList();
    }

    @Override
    public List<String> getTargets()
    {
        return targets;
    }

    @Override
    public String name()
    {
        return name;
    }
}
