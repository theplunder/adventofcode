package nl.mrtijmen.aoc2023.pulse;

import java.util.List;

public interface Module
{

    /**
     * Modifies the pulse Message
     *
     * @return list of new targets
     */
    List<PulseMessage> process(PulseMessage message);

    List<String> getTargets();

    String name();

    void registerConnected(String connected);

    default ModuleType type()
    {
        if (this.getClass().isAssignableFrom(FlipFlip.class)) return ModuleType.FLIPFLOP;
        if (this.getClass().isAssignableFrom(Conjuction.class)) return ModuleType.CONJUNTION;
        if (this.getClass().isAssignableFrom(Output.class)) return ModuleType.OUTPUT;
        if (this.getClass().isAssignableFrom(Broadcaster.class)) return ModuleType.BROADCAST;

        return null;
    }

}
