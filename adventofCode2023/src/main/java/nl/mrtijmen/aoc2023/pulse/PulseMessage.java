package nl.mrtijmen.aoc2023.pulse;

public class PulseMessage {

    private PulseType pulse;

    private String target;

    public static long buttonCount = 0;

    private String previousTarget = null;

    public PulseMessage(PulseType pulse, String target) {
        this.pulse = pulse;
        this.target = target;
    }


    public PulseMessage route(PulseType newPulseType, String target) {
        String previous = this.target;
        var newPulse = new PulseMessage(newPulseType, target);
        newPulse.previousTarget = previous;
        return newPulse;
    }

    public String getPreviousTarget() {
        return previousTarget;
    }

    public String getTarget() {
        return target;
    }

    public PulseType getPulse() {
        return pulse;
    }

    @Override
    public String toString() {
        return "PulseMessage{" +
                "pulse=" + pulse +
                ", target='" + target + '\'' +
                ", previousTarget='" + previousTarget + '\'' +
                '}';
    }
}
