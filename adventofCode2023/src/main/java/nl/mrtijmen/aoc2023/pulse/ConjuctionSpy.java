package nl.mrtijmen.aoc2023.pulse;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Conjunction modules (prefix &) remember the type of the
 * most recent pulse received from each of their
 * connected input modules; they initially default to
 * remembering a low pulse for each input.
 * When a pulse is received, the conjunction module
 * first updates its memory for that input.
 * Then, if it remembers high pulses for all inputs,
 * it sends a low pulse; otherwise, it sends a high pulse.
 */

public class ConjuctionSpy extends Conjuction
{
    public Map<String, Long> interval = new HashMap<>();

    public long allHigh = 0;

    public boolean done = false;

    public ConjuctionSpy(Module module)
    {
        super(module.name(), module.getTargets());

    }

    @Override
    public void registerConnected(String connectedModule)
    {
        super.registerConnected(connectedModule);
        interval.put(connectedModule, 0l);
    }


    @Override
    public List<PulseMessage> process(PulseMessage message)
    {
        var result = super.process(message);

        if (message.getPulse().equals(PulseType.HIGH))
        {
            long inter = interval.get(message.getPreviousTarget());

            if (inter == 0)
            {
                System.out.println("adding " + message.getPreviousTarget() + " " + PulseMessage.buttonCount);
                interval.put(message.getPreviousTarget(), PulseMessage.buttonCount);
            }

            if (allHigh())
            {
                allHigh = PulseMessage.buttonCount;
                done = true;
            }
        }
        return result;
    }

    public boolean isDone()
    {
        return interval.values().stream().noneMatch(l -> l == 0);
    }
}
