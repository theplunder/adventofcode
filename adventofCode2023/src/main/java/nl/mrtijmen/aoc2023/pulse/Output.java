package nl.mrtijmen.aoc2023.pulse;

import java.util.ArrayList;
import java.util.List;

public class Output implements Module
{

    private final String name;

    public Output(String name)
    {
        this.name = name;
    }

    public List<String> connected = new ArrayList<>();

    boolean hasReceivedPulse = false;

    @Override
    public List<PulseMessage> process(PulseMessage message)
    {
        if (message.getPulse() == PulseType.LOW)
        {
            hasReceivedPulse = true;
            System.out.println("PONG");
        }
        //    System.out.println("PING");
        return List.of(message.route(message.getPulse(), "destroy"));
    }

    @Override
    public List<String> getTargets()
    {
        return List.of("destroy");
    }

    @Override
    public String name()
    {
        return name;
    }

    @Override
    public void registerConnected(String connectedModule)
    {
        connected.add(connectedModule);
    }


    public boolean isHasReceivedPulse()
    {
        return hasReceivedPulse;
    }
}
