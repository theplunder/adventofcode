package nl.mrtijmen.aoc2023.pulse;

import java.util.*;

public class PulseEventQueue
{

    private Map<String, Module> router = new HashMap<>();

    private final Queue<PulseMessage> queue = new ArrayDeque<>();

    private long lowCounter = 0;
    private long highCounter = 0;

    public PulseEventQueue(Collection<Module> modules)
    {
        modules.forEach(this::register);
    }

    public void register(Module module)
    {
        router.put(module.name(), module);
    }


    private void processEvent()
    {
        int counter = 0;
        while (!queue.isEmpty())
        {
            if (counter++ > 10000000)
            {
                break; //avoid infi loops
            }

            PulseMessage pulseToProcess = queue.poll();
            var newPulses = process(pulseToProcess);
            for (PulseMessage pulse : newPulses)
            {
                if (pulse.getTarget().equals("destroy"))
                {
                    continue;
                }
                countMessage(pulse);
       //            System.out.println(pulse.getPreviousTarget() + " -" + pulse.getPulse() + "-> " + pulse.getTarget());
                queue.offer(pulse);
            }
        }
    }


    public void pushButton()
    {
        PulseMessage pulse = new PulseMessage(PulseType.LOW, Broadcaster.NAME);
        PulseMessage.buttonCount++;
        queue.add(pulse);
        countMessage(pulse);
//        System.out.println("---------PUSH---------");
//        System.out.println("button -" + pulse.getPulse() + "-> " + pulse.getTarget());
        processEvent();
    }


    private void countMessage(PulseMessage message)
    {

        if (message.getPulse() == PulseType.HIGH)
        {
            highCounter++;
        } else
        {
            lowCounter++;
        }


    }

    private List<PulseMessage> process(PulseMessage message)
    {
        String target = message.getTarget();

        if (!router.containsKey(target))
        {
            System.out.println("target not found!" + message);
            //destroy instead?
            return List.of(message.route(message.getPulse(), "destroy"));

        }


        return router.get(target).process(message);
    }

    public long getLowCounter()
    {
        return lowCounter;
    }

    public long getHighCounter()
    {
        return highCounter;
    }

    public Map<String, Module> getRouter()
    {
        return router;
    }

}
