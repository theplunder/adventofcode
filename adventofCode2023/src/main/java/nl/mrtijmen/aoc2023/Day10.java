package nl.mrtijmen.aoc2023;

import nl.mrtijmen.aoc2023.pipes.Pipe;
import nl.mrtijmen.aoc2023.pipes.PipeType;
import nl.mrtijmen.aoccommons.util.ArrayUtil;
import nl.mrtijmen.aoccommons.util.FileNameUtil;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;
import nl.mrtijmen.aoccommons.util.ParseUtil;
import nl.mrtijmen.aoccommons.util.grid.Coordinate;
import nl.mrtijmen.aoccommons.util.grid.Grid;
import org.checkerframework.checker.units.qual.C;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static nl.mrtijmen.aoc2023.pipes.PipeType.*;

public class Day10 {

    public static void main(String[] args) throws Exception {
//        List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName(1));
        //    List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName(2));
        // List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName(3));


        List<String> data = FileReaderUtil.getStringList(FileNameUtil.dataFileName());

        var grid1 = data.stream().map(l -> ParseUtil.lineToObject(l, "", PipeType::fromCode)).toList();
        Grid<Pipe> grid = new Grid<>(ArrayUtil.poitionAwareConvert(grid1, (type, coordinate) -> new Pipe(coordinate, type)));

        System.out.println(grid);

        Coordinate start = findStart(grid);
        PipeType startType = determineStartType(grid, start);
        grid.setValue(start, new Pipe(start, startType));

        Coordinate initialDirection = switch (startType) {
            case BEND_NE, HORIZONTAL, BEND_SE -> start.right();
            case BEND_NW, BEND_SW -> start.left();
            case VERTICAL -> start.down();
            default -> null;//can't happen
        };


        Coordinate next = initialDirection;
        Coordinate current = start;
        List<Coordinate> path = new ArrayList<>();
        do {
            System.out.println(current);
            Coordinate nextCoordinate = grid.getValue(next).traverse(current);
            current = next;
            next = nextCoordinate;
            path.add(current);
        } while (!current.equals(start));

        int stepCounter = path.size();
        System.out.println("Steps: " + stepCounter);
        System.out.println("Result part 1 --> " + (stepCounter / 2));

        //removeJunk
        cleanGrid(grid, path);
        System.out.println(grid);

        //THE PLAN!
        // FOLLOW lines left-right of top-bottom
        // every time you cross a line, you are either inside or outside of the loop!
        // difficulty is in determining wheter you cross or follow along

        // lets go!

        int result2 = countSurface(grid);
        System.out.println("Result part 2 --> " + result2);

    }

    public static void cleanGrid(Grid<Pipe> grid, List<Coordinate> path) {
        grid.visitAndUpdateAllCoordinates((c, p) -> {
                    if (!path.contains(c)) {
                        return new Pipe(c, NO_PIPE);
                    }
                    return p;
                }
        );
    }


    private static Coordinate findStart(Grid<Pipe> grid) {
        Coordinate[] startPoint = new Coordinate[1];

        grid.visitAllCoordinates((c, p) -> {
            if (p.type().equals(PipeType.START)) startPoint[0] = c;
        });

        return startPoint[0];
    }


    private static PipeType determineStartType(Grid<Pipe> grid, Coordinate start) {

        List<PipeType> types = new ArrayList<>(List.of(PipeType.values()));
        types.remove(PipeType.START);
        types.remove(PipeType.NO_PIPE);


        Coordinate down = start.up();
        System.out.println("checking " + down);
        if (!grid.coordinateWithinRange(down)
                || !isOneOf(grid.getValue(down).type(), VERTICAL, BEND_NE, BEND_NW)) {
            types.remove(HORIZONTAL);
            types.remove(BEND_SE);
            types.remove(BEND_SW);
        }

        Coordinate up = start.down();
        System.out.println("checking " + up);
        if (!grid.coordinateWithinRange(up)
                || !isOneOf(grid.getValue(up).type(), VERTICAL, BEND_SE, BEND_SW)) {
            types.remove(HORIZONTAL);
            types.remove(BEND_NE);
            types.remove(BEND_NW);
        }

        Coordinate left = start.left();
        System.out.println("checking " + left);
        if (!grid.coordinateWithinRange(left)
                || !isOneOf(grid.getValue(left).type(), HORIZONTAL, BEND_NE, BEND_SE)) {
            types.remove(VERTICAL);
            types.remove(BEND_NW);
            types.remove(BEND_SW);
        }


        Coordinate right = start.right();
        System.out.println("checking " + right);
        if (!grid.coordinateWithinRange(right)
                || !isOneOf(grid.getValue(right).type(), HORIZONTAL, BEND_NW, BEND_SW)) {
            types.remove(VERTICAL);
            types.remove(BEND_NE);
            types.remove(BEND_SE);
        }
        System.out.println(types);
        return types.get(0);
    }


    public static <T> boolean isOneOf(T value, T... candiates) {
        for (T candidate : candiates) {
            if (value.equals(candidate)) {
                return true;
            }
        }
        return false;
    }

    public static int countSurface(Grid<Pipe> cleanedGrid) {

        boolean inside;
        List<Coordinate> insideCoordinates = new ArrayList<>();


        //We check columns
        for (int x = 0; x < cleanedGrid.xSize(); x++) {
            inside = false;
            PipeType lastBend = null;
            for (int y = 0; y < cleanedGrid.ySize(); y++) {


                Pipe pipe = cleanedGrid.getValue(x, y);
                PipeType type = pipe.type();
                if (VERTICAL.equals(type)) {
                    continue;
                }
                if (isOneOf(type, BEND_SE, BEND_SW)) {
                    //Always meet this one before a North Bend
                    lastBend = type;
                }
                if (isOneOf(type, BEND_NE, BEND_NW)) {

                    if ((BEND_SE.equals(lastBend) && BEND_NW.equals(type)) ||
                            (BEND_SW.equals(lastBend) && BEND_NE.equals(type))) {
                        inside = !inside;
                    }
                    lastBend = null;
                }
                if (NO_PIPE.equals(type) && inside) {
                    insideCoordinates.add(pipe.position());
                }
                if (HORIZONTAL.equals(type)) {
                    inside = !inside;
                }
            }
        }

        return insideCoordinates.size();
    }


}