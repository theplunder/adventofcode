package nl.mrtijmen.aoc2023.camelcard;

import java.util.List;

public record Hand(List<Card> cards, int bid) implements Comparable<Hand>
{

    @Override
    public int compareTo(Hand other)
    {
        return 0;
    }

    @Override
    public String toString()
    {
        return "Hand{" +
                "cards=" + cards +
                '}';
    }
}
