package nl.mrtijmen.aoc2023.camelcard;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PokerSetComparator implements Comparator<Hand>
{
//
//  6  Five of a kind, where all five cards have the same label: AAAAA
//  5  Four of a kind, where four cards have the same label and one card has a different label: AA8AA
//  4  Full house, where three cards have the same label, and the remaining two cards share a different label: 23332
//  3  Three of a kind, where three cards have the same label, and the remaining two cards are each different from any other card in the hand: TTT98
//  2  Two pair, where two cards share one label, two other cards share a second label, and the remaining card has a third label: 23432
//  1  One pair, where two cards share one label, and the other three cards have a different label from the pair and each other: A23A4
//  0  High card, where all cards' labels are distinct: 23456


    private Map<Card, Integer> countCard(List<Card> cards)
    {
        Map<Card, Integer> count = new HashMap<>();
        for (Card card : cards)
        {
            count.put(card, count.computeIfAbsent(card, n -> 0) + 1);
        }
        return count;
    }

    private Integer setValue(Map<Card, Integer> cardCount)
    {
        int jokerCount = cardCount.getOrDefault(Card.JOKER, 0);
        cardCount.remove(Card.JOKER);

        if (cardCount.size() <= 1) return 6; //five of a kind
        if (cardCount.size() == 2 && cardCount.containsValue(4 - jokerCount)) return 5; // 4 of a kind
        if (cardCount.size() == 2
                && ((cardCount.containsValue(2 - jokerCount) && cardCount.containsValue(3))
                || (cardCount.containsValue(2) && cardCount.containsValue(3 - jokerCount)))) return 4; // full house
        if (cardCount.size() == 3 && cardCount.containsValue(3 - jokerCount)) return 3; // 3 of a kind
        if (cardCount.size() == 3 && cardCount.containsValue(2 - jokerCount))
            return 2; // 2  //jokerCount irrelevant? always 3 of a kind!
        if (cardCount.size() == 4 || jokerCount != 0) return 1; // 1 pair
        return 0;
    }

    private Integer handValue(Hand hand)
    {
        var cards = hand.cards();
        var countCards = countCard(cards);
        int value = setValue(countCards);

        if (cards.contains(Card.JOKER)){
            System.out.println(cards + " -> " + value) ;
        }

        return value;
    }


    @Override
    public int compare(Hand o1, Hand o2)
    {
        return handValue(o1).compareTo(handValue(o2));
    }

}
