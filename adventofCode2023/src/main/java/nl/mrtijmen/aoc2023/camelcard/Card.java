package nl.mrtijmen.aoc2023.camelcard;

public enum Card
{

    ACE('A', 14),
    KING('K', 13),
    QUEEN('Q', 12),
    JACK('J', 11),
    TEN('T', 10),
    NINE('9', 9),
    EIGHT('8', 8),
    SEVEN('7', 7),
    SIX('6', 6),
    FIVE('5', 5),
    FOUR('4', 4),
    THREE('3', 3),
    TWO('2', 2),
    JOKER('X', 0);

    private Character code;
    private int value;

    Card(Character code, int value)
    {
        this.code = code;
        this.value = value;
    }

    public int value()
    {
        return value;
    }

    public static Card fromCode(String code)
    {
        for (Card card : Card.values())
        {
            if (card.code == code.charAt(0))
            {
                return card;
            }
        }
        throw new IllegalArgumentException("no card with code " + code + " found.");

    }
}
