package nl.mrtijmen.aoc2023;

import nl.mrtijmen.aoccommons.util.ArrayUtil;
import nl.mrtijmen.aoccommons.util.FileNameUtil;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;
import nl.mrtijmen.aoccommons.util.ParseUtil;
import nl.mrtijmen.aoccommons.util.grid.Grid;

import java.util.ArrayList;
import java.util.List;

public class Day13
{

    public static void main(String[] args) throws Exception
    {
        //List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName());

        List<String> data = FileReaderUtil.getStringList(FileNameUtil.dataFileName());

        var batches = batchInput(data);
        System.out.println(batches);


        var images = batches.stream().map(l -> ParseUtil.parseStringMatrix(l, "")).toList();

        //  findHorizontalReflectionPosition(images.get(2));
        //countValue(images.get(2));

        int result = images.stream().mapToInt(Day13::countValue).peek(System.out::println).sum();

        System.out.println("Result part 1 -->" + result);

        int result2 = images.stream().mapToInt(Day13::countSmudgedValue).peek(System.out::println).sum();

        System.out.println("Result part 2 -->" + result2);

    }

    private static int countValue(List<List<String>> input)
    {
        //  System.out.println(new Grid<>(input));
        int vert = simpleFindVerticalReflectionPosition(input);
        int horiz = simpleFindHorizontalReflectionPosition(input);
//        System.out.println("found vert: " + vert + " horiz: " + horiz);


        if (vert != 0 && horiz != 0)
        {
            System.out.println("DOUBLE REFLECTION");
            System.out.println("found vert: " + vert + " horiz: " + horiz);
            System.out.println(new Grid<>(input));
        }
        return vert + horiz * 100;
    }

    private static int countSmudgedValue(List<List<String>> input)
    {
        //  System.out.println(new Grid<>(input));
        int vert = findVerticalReflectionPosition(input);
        int horiz = findHorizontalReflectionPosition(input);
//        System.out.println("found vert: " + vert + " horiz: " + horiz);


        return vert + horiz * 100;
    }


    public static int findVerticalReflectionPosition(List<List<String>> input)
    {
        var rotated = ArrayUtil.rotate(input);
        return findHorizontalReflectionPosition(rotated);
    }

    public static int simpleFindVerticalReflectionPosition(List<List<String>> input)
    {
        var rotated = ArrayUtil.rotate(input);
        return simpleFindHorizontalReflectionPosition(rotated);
    }

    public static int findHorizontalReflectionPosition(List<List<String>> input)
    {
        for (int i = 1; i < input.size(); i++)
        {
            int maxRefLength = Math.min(i, input.size() - i);

            boolean reflectionCandidate = false;

//            System.out.println("----" + i + "----");

            int diffCount = 0;
            for (int j = 1; j <= maxRefLength; j++)
            {
                //check skip one or not!
                var above = input.get(i - j);
                var below = input.get(i + j - 1);
                diffCount += countDifferences(above, below);
                if (diffCount <= 1)
                {
                    reflectionCandidate = true;
                } else
                {
                    //System.out.println("No Reflection");
                    reflectionCandidate = false;
                    break;
                }

            }
            if (reflectionCandidate && diffCount == 1)
            {
                System.out.println("found smudged reflection at " + i);
                return i;
            }

        }
        return 0;
    }

    public static int countDifferences(List<String> a, List<String> b)
    {
        int result = 0;
        for (int i = 0; i < a.size(); i++)
        {
            if (!a.get(i).equals(b.get(i)))
            {
                result++;
            }
        }
        return result;
    }

    public static int simpleFindHorizontalReflectionPosition(List<List<String>> input)
    {
        //     System.out.println(new Grid<>(input));

        for (int i = 1; i < input.size(); i++)
        {
            int maxRefLength = Math.min(i, input.size() - i);

            boolean foundReflection = false;

//            System.out.println("----" + i + "----");
            for (int j = 1; j <= maxRefLength; j++)
            {
                //check skip one or not!
                var above = input.get(i - j);
                var below = input.get(i + j - 1);
//                System.out.println(above);
//                System.out.println(below);
                if (above.equals(below))
                {
                    foundReflection = true;
                } else
                {
                    //System.out.println("No Reflection");
                    foundReflection = false;
                    break;
                }

            }
            if (foundReflection)
            {
                System.out.println("found reflection at " + i);
                return i;
            }

        }
        return 0;

    }


    private static List<List<String>> batchInput(List<String> data)
    {
        List<List<String>> batched = new ArrayList<>();

        int low = 0;
        for (int i = 0; i < data.size(); i++)
        {
            if (!data.get(i).isBlank())
            {
                continue;
            }
            batched.add(data.subList(low, i));
            low = i + 1;

        }

        batched.add(data.subList(low, data.size()));

        return batched;


    }

}