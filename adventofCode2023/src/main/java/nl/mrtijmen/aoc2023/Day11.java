package nl.mrtijmen.aoc2023;

import nl.mrtijmen.aoc2023.sequence.Sequence;
import nl.mrtijmen.aoccommons.util.ArrayUtil;
import nl.mrtijmen.aoccommons.util.FileNameUtil;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;
import nl.mrtijmen.aoccommons.util.ParseUtil;
import nl.mrtijmen.aoccommons.util.grid.Coordinate;
import nl.mrtijmen.aoccommons.util.grid.Grid;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Day11 {

    private static final String GALAXY = "#";
    private static final String EMPTY = ".";

    private static final long EXPANSION_FACTOR_1 = 2;
    private static final long EXPANSION_FACTOR_2 = 1_000_000;

    private static final long EXPANSION_FACTOR_3 = 1_00;

    public static void main(String[] args) throws Exception {
        // List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName());

        List<String> data = FileReaderUtil.getStringList(FileNameUtil.dataFileName());

        List<List<String>> galaxy = data.stream().map(line -> ParseUtil.lineToStringList(line, "")).toList();
        List<List<Integer>> expansionLines = listPositionsOfExpansion(galaxy);

        System.out.println(expansionLines);
        //galaxy = applyCosmologicalExpansion(galaxy);
        Grid<String> cosmos = new Grid<>(galaxy);
        System.out.println(cosmos);

        List<Coordinate> galaxies = cosmos.find(s -> s.equals(GALAXY));

        part1(galaxies, expansionLines);


        part2(galaxies, expansionLines);


    }

    private static void part2(List<Coordinate> galaxies, List<List<Integer>> expansionLines) {
        long sum = 0;
        for (int i = 0; i < galaxies.size() - 1; i++) {
            Coordinate one = galaxies.get(i);

            for (int j = i + 1; j < galaxies.size(); j++) {
                sum += distanceBetween(one, galaxies.get(j), expansionLines, EXPANSION_FACTOR_2);
            }
        }
        System.out.println("Result Part 1 --> " + sum);
    }

    private static void part1(List<Coordinate> galaxies, List<List<Integer>> expansionLines) {
        long sum = 0;
        for (int i = 0; i < galaxies.size() - 1; i++) {
            Coordinate one = galaxies.get(i);

            for (int j = i + 1; j < galaxies.size(); j++) {
                sum += distanceBetween(one, galaxies.get(j), expansionLines, EXPANSION_FACTOR_1);
            }
        }
        System.out.println("Result Part 1 --> " + sum);
    }

    private static long distanceBetween(Coordinate one, Coordinate two, List<List<Integer>> expansionLines, long expansionFactor) {
        long intermediate = (Math.abs(one.x() - two.x()) + Math.abs(one.y() - two.y()));

        var xpositions = expansionLines.get(0);
        var ypositions = expansionLines.get(1);

        long upperX = Math.max(one.x(), two.x());
        long lowerX = Math.min(one.x(), two.x());
        long xCrossings = xpositions.stream().filter(x -> x > lowerX && x < upperX).count();

        long upperY = Math.max(one.y(), two.y());
        long lowerY = Math.min(one.y(), two.y());
        long yCrossings = ypositions.stream().filter(y -> y > lowerY && y < upperY).count();


        return intermediate + (expansionFactor - 1) * (xCrossings + yCrossings);

    }


    public static List<List<String>> applyCosmologicalExpansion(List<List<String>> galaxy) {
        galaxy = expandEmptyLines(galaxy);
        galaxy = ArrayUtil.rotate(galaxy);
        galaxy = expandEmptyLines(galaxy);
        galaxy = ArrayUtil.rotate(galaxy);
        galaxy = ArrayUtil.rotate(galaxy);
        galaxy = ArrayUtil.rotate(galaxy);
        return galaxy;
    }

    public static List<List<Integer>> listPositionsOfExpansion(List<List<String>> galaxy) {
        List<Integer> horizontalLines = findLinesOfExpansion(galaxy);
        galaxy = ArrayUtil.rotate(galaxy);
        List<Integer> verticalLines = findLinesOfExpansion(galaxy);
        return List.of(verticalLines, horizontalLines);
    }

    public static List<Integer> findLinesOfExpansion(List<List<String>> galaxy) {

        List<Integer> linesOfExpansion = new ArrayList<>();
        for (int i = 0; i < galaxy.size(); i++) {
            var line = galaxy.get(i);
            if (!line.contains(GALAXY)) {
                linesOfExpansion.add(i);
            }
        }
        return linesOfExpansion;
    }


    private static List<List<String>> expandEmptyLines(List<List<String>> galaxy) {
        List<List<String>> rebuild = new ArrayList<>();
        List<String> emptyLine = new ArrayList<>(Collections.nCopies(galaxy.get(0).size(), EMPTY));

        for (List<String> line : galaxy) {
            if (!line.contains(GALAXY)) {
                rebuild.add(emptyLine);
            }
            rebuild.add(line);
        }
        return rebuild;
    }






}