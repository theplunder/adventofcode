package nl.mrtijmen.aoc2023;

import nl.mrtijmen.aoc2023.brick.Brick;
import nl.mrtijmen.aoc2023.brick.BrickPile;
import nl.mrtijmen.aoccommons.util.FileNameUtil;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;
import nl.mrtijmen.aoccommons.util.ParseUtil;
import nl.mrtijmen.aoccommons.util.grid.Coordinate3D;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class Day22
{

    //NOTE Z=1 is ground!
    public static void main(String[] args) throws Exception
    {
       // List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName());
        List<String> data = FileReaderUtil.getStringList(FileNameUtil.dataFileName());


        List<Brick> brickList = data.stream()
                                    .map(l -> ParseUtil.lineToStringList(l, "~")
                                                       .stream()
                                                       .map(l2 -> ParseUtil.lineToNumberList(l2, ","))
                                                       .map(c -> new Coordinate3D(c.get(0), c.get(1), c.get(2)))
                                                       .toList())
                                    .map(lc -> new Brick(lc.get(0), lc.get(1), 0))
                                    .toList();

        AtomicInteger i = new AtomicInteger(0);
        brickList = brickList.stream()
                             .sorted(brickBottomToTopComparator())
                             //only now add id, to it increases from bottom to top
                             .map(b -> new Brick(b.start(), b.end(), i.getAndIncrement()))
                             //   .peek(System.out::println)
                             .toList();

        BrickPile pile = new BrickPile(10, 10);
        brickList.forEach(pile::addBrick);

        var removables = pile.listRemovableBricks();

        System.out.println(removables);

        int result1 = removables.size();
        System.out.println("Result Part 1 --> " + result1);

        var supportingMap = pile.buildSupportingMap();
        validateAssumption(supportingMap);

        var supportedByMap = buildSupportedByMap(supportingMap);

        long result2 = supportingMap.keySet().stream().mapToLong(c -> {
            int fallCount = findFallCount(c, supportingMap, supportedByMap);
            System.out.println("Fallcount: " + fallCount);
            return fallCount;
        }).sum();


        System.out.println("Result Part 2 --> " + result2);
    }


    private static int findFallCount(Integer target, Map<Integer, Set<Integer>> supportingPile, Map<Integer, Set<Integer>> supportedBy)
    {
        Set<Integer> fallingBricks = new HashSet<>();

        //lowestID first?
        Set<Integer> newFallingBricks = new HashSet<>();
        newFallingBricks.add(target);
        while (true)
        {
            newFallingBricks.remove(target);
            Set<Integer> supportedByTarget = supportingPile.get(target);
            int workingTarget = target; //for use in lambda
            for (Integer supportedBrickId : supportedByTarget)
            {
                var candidateSupports = supportedBy.get(supportedBrickId);
                long supportsCount = candidateSupports.stream()
                                                      .filter(id -> !Objects.equals(id, workingTarget))
                                                      .filter(id -> !fallingBricks.contains(id))
                                                      .filter(id -> !newFallingBricks.contains(id))
                                                      .count();
                if (supportsCount == 0)
                {
                    fallingBricks.add(supportedBrickId);
                    newFallingBricks.add(supportedBrickId);
                }
            }

            var newTarget = newFallingBricks.stream().sorted().findFirst();
            if (newTarget.isEmpty())
            {
                break;
            }
            target = newTarget.get();
        }


        return fallingBricks.size();
    }


    private static Map<Integer, Set<Integer>> buildSupportedByMap(Map<Integer, Set<Integer>> supportingPile)
    {

        Map<Integer, Set<Integer>> supportedBy = new HashMap<>();
        for (Map.Entry<Integer, Set<Integer>> entry : supportingPile.entrySet())
        {
            for (Integer id : entry.getValue())
            {
                supportedBy.computeIfAbsent(id, (i) -> new HashSet<>());
                supportedBy.get(id).add(entry.getKey());
            }
        }

        return supportedBy;
    }

    private static void validateAssumption(Map<Integer, Set<Integer>> supportingPile)
    {

        Set<Integer> ids = supportingPile.keySet();
        var supporedBricks = supportingPile.values();

        int counter = 0;

        for (Integer brickId : ids)
        {
            Set<Integer> isSupportedBy = supportingPile.get(brickId);
            if (isSupportedBy.isEmpty())
            {
                counter++;
                continue;
            }

            boolean canBeRemoved = true;
            //all should have more than 1 supporter
            for (Integer subject : isSupportedBy)
            {

                long supportsCount = supporedBricks.stream().filter(s -> s.contains(subject)).count();
                if (supportsCount == 1) canBeRemoved = false;
            }

            if (canBeRemoved) counter++;


        }

        System.out.println(counter);

    }


    public static Comparator<Brick> brickBottomToTopComparator()
    {
        return (b1, b2) -> {
            long minZ1 = Math.min(b1.start().z(), b1.end().z());
            long minZ2 = Math.min(b2.start().z(), b2.end().z());
            return Long.compare(minZ1, minZ2);
        };


    }

}