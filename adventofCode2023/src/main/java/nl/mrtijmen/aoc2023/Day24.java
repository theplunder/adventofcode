package nl.mrtijmen.aoc2023;

import nl.mrtijmen.aoc2023.hail.HailStone;
import nl.mrtijmen.aoccommons.util.FileNameUtil;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;
import nl.mrtijmen.aoccommons.util.ParseUtil;
import nl.mrtijmen.aoccommons.util.grid.Coordinate3D;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class Day24
{
    public static void main(String[] args) throws Exception
    {
//        long limit_low = 7;
//        long limit_high = 27;
//        List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName());

        // //hardcode testBox for now, x and y are same.
        long limit_low = 200_000_000_000_000L;
        long limit_high = 400_000_000_000_000L;
        List<String> data = FileReaderUtil.getStringList(FileNameUtil.dataFileName());

        AtomicInteger id = new AtomicInteger(0);
        var hailstones = data.stream().map(l -> ParseUtil.lineToStringList(l, "@")).map(l -> {
            var pos = ParseUtil.lineToLongList(l.get(0), ",");
            Coordinate3D cPos = new Coordinate3D(pos.get(0), pos.get(1), pos.get(2));
            var dir = ParseUtil.lineToLongList(l.get(1), ",");
            Coordinate3D cDir = new Coordinate3D(dir.get(0), dir.get(1), dir.get(2));
            return new HailStone(cPos, cDir, id.incrementAndGet());
        }).toList();


        int counter = 0;
        for (int i = 0; i < hailstones.size(); i++)
        {

            for (int j = i + 1; j < hailstones.size(); j++)
            {
                var stoneA = hailstones.get(i);
                var stoneB = hailstones.get(j);
                boolean intersects = pathsIntersectWithingBox(stoneA, stoneB, limit_low, limit_high);

                if (intersects) counter++;
            }
        }

        int result1 = counter;
        System.out.println("Result Part 1 --> " + result1);

        long x0_min = Long.MAX_VALUE;
        long x0_max = Long.MIN_VALUE;

        long y0_min = Long.MAX_VALUE;
        long y0_max = Long.MIN_VALUE;

        long z0_min = Long.MAX_VALUE;
        long z0_max = Long.MIN_VALUE;

        long vx_min = Long.MAX_VALUE;
        long vx_max = Long.MIN_VALUE;
        long vy_min = Long.MAX_VALUE;
        long vy_max = Long.MIN_VALUE;
        long vz_min = Long.MAX_VALUE;
        long vz_max = Long.MIN_VALUE;


        for (HailStone stone : hailstones)
        {
            x0_min = Math.min(x0_min, stone.initialPosition().x());
            x0_max = Math.max(x0_max, stone.initialPosition().x());

            y0_min = Math.min(y0_min, stone.initialPosition().y());
            y0_max = Math.max(y0_max, stone.initialPosition().y());

            z0_min = Math.min(z0_min, stone.initialPosition().z());
            z0_max = Math.max(z0_max, stone.initialPosition().z());

            vx_min = Math.min(vx_min, stone.dirVector().x());
            vx_max = Math.max(vx_max, stone.dirVector().x());

            vy_min = Math.min(vy_min, stone.dirVector().y());
            vy_max = Math.max(vy_max, stone.dirVector().y());

            vz_min = Math.min(vz_min, stone.dirVector().z());
            vz_max = Math.max(vz_max, stone.dirVector().z());
        }

        System.out.println("vx min max: " + vx_min + " " + vx_max);
        System.out.println("vy min max: " + vy_min + " " + vy_max);
        System.out.println("vz min max: " + vz_min + " " + vz_max);

        System.out.println("x0 range: " + (x0_max - x0_min));
        System.out.println("y0 range: " + (y0_max - y0_min));
        System.out.println("z0 range: " + (z0_max - z0_min));

        // x0_min matches vx_max and viceversa


        Coordinate3D refPoint = new Coordinate3D(x0_max, y0_min, z0_min);


        //Find a reference point for t0
        //caclulate the interception velcotiy range for any stone?
        //intercept!
        //fine a next stone, is it still interceptable?
        //if stones fly past our start points in the opposite direction, we can no longer intercept!

        List<Long> optionsX = null;
        List<Long> optionsY = null;
        List<Long> optionsZ = null;

        for (int i = 0; i < hailstones.size(); i++)
        {
            for (int j = i + 1; j < hailstones.size(); j++)
            {
                var stoneA = hailstones.get(i);
                var stoneB = hailstones.get(j);

                if (stoneA.dirVector().x() == stoneB.dirVector().x())
                {
                    List<Long> interceptVel = findInterceptEqualSpeed(
                            stoneA.initialPosition().x(), stoneB.initialPosition().x()
                            , stoneA.dirVector().x());
                    if (optionsX == null)
                    {
                        optionsX = interceptVel;
                    } else
                    {
                        optionsX = optionsX.stream().filter(interceptVel::contains).toList();
                    }
                }
                if (stoneA.dirVector().y() == stoneB.dirVector().y())
                {
                    List<Long> interceptVel = findInterceptEqualSpeed(
                            stoneA.initialPosition().y(), stoneB.initialPosition().y()
                            , stoneA.dirVector().y());
                    if (optionsY == null)
                    {
                        optionsY = interceptVel;
                    } else
                    {
                        optionsY = optionsY.stream().filter(interceptVel::contains).toList();
                    }
                }
                if (stoneA.dirVector().z() == stoneB.dirVector().z())
                {
                    List<Long> interceptVel = findInterceptEqualSpeed(
                            stoneA.initialPosition().z(), stoneB.initialPosition().z()
                            , stoneA.dirVector().z());
                    if (optionsZ == null)
                    {
                        optionsZ = interceptVel;
                    } else
                    {
                        optionsZ = optionsZ.stream().filter(interceptVel::contains).toList();
                    }
                }


            }
        }

        System.out.println("options for vX are:" + optionsX);
        System.out.println("options for vY are:" + optionsY);
        System.out.println("options for vZ are:" + optionsZ);


        //if I now correct the velocities they all intersect at the same point!

        long vxs = optionsX.get(0);
        long vys = optionsY.get(0);
        long vzs = optionsZ.get(0);

        var posA = hailstones.get(1).initialPosition();
        var dirA = hailstones.get(1).dirVector();
        var posB = hailstones.get(2).initialPosition();
        var dirB = hailstones.get(2).dirVector();

        double x0_delta = posA.x() - posB.x();
        double vel_y_rat_A = (dirA.x() - vxs) / ((double) dirA.y() - vys);
        double vel_y_rat_B = (dirB.x() - vxs) / ((double) dirB.y() - vys);

        double y_intersect = solveMotionEquation(x0_delta, vel_y_rat_A, vel_y_rat_B, posA.y(), posB.y());
        double x_intersect = posA.x() + (dirA.x() - vxs) * ((y_intersect - posA.y()) / (dirA.y() - vys));

        double z_intersect = posA.z() + (dirA.z() - vzs) * ((y_intersect - posA.y()) / (dirA.y() - vys));


        System.out.println(x_intersect + " , " + y_intersect + " , " + z_intersect);

        long result2 = Math.round(x_intersect + y_intersect + z_intersect);
        //from top or bottom?
//        for (long vx = vx_min; Math.abs(vx) < x0_min; vx--) {
//            //((stoneA.initialPosition().x() - x0) % (vx - stoneA.dirVector().x()) == 0) <-- only these
//            long nmax = (long) Math.abs(Math.ceil((stoneA.initialPosition().x()) / (vx - stoneA.dirVector().x())));
//
//        //    System.out.println("nmax: " + nmax);
//
//            for (long n = 1; n <= nmax; n++) {
//
//
//                long x0 = stoneA.initialPosition().x() - (vx - stoneA.dirVector().x()) * n;
//           //     System.out.println("trying " + x0 + " with vx " + vx);
//
//                if ((vx - stoneA.dirVector().x()) == 0) {
//                    System.out.println("same velocity");
//
//                } else if ((stoneA.initialPosition().x() - x0) % (vx - stoneA.dirVector().x()) == 0) {
//                    long t = (stoneA.initialPosition().x() - x0) / (vx - stoneA.dirVector().x());
//
//                    //is it also a possible intercept for other stones?
//                    long finalVx = vx;
//                    long finalX = x0;
//                    long matching = hailstones.stream().filter(stone ->
//                            ((finalVx - stone.dirVector().x()) != 0 && (stone.initialPosition().x() - finalX) % (finalVx - stone.dirVector().x()) == 0)
//                    ).count();
//
//                    if (matching == hailstones.size()) {
//                        System.out.println("FOUND A POSSIBLE SOLUTION!");
//                        System.out.println("possible intercept: " + x0 + ", " + vx + " at time " + t);
//
////                        break;
//                    }
//
//
//                }
//
//           }

        //        }
//
//
        System.out.println("Result Part 2 --> " + result2);
    }

    private static List<Long> findInterceptEqualSpeed(long posA, long posB, long velA)
    {
        long diff = Math.abs(posA - posB);

        List<Long> options = new ArrayList<>();

        //i = velocity
        for (long i = -3000; i < 3001; i++)
        {
            if (i == 0 || i == 1 || i == -1 || (velA - i) == 0)
            {
                continue;
            }

            if ((diff % (velA - i)) % i == 0)
            {
                //   long time = (diff + velA * i) / i;
                options.add(i);
            }
        }
        return options;
    }
//  THIS BELOW DOES NOT WORK BECAUSE THEY TRAVEL BETWEEN COLLISIONS< THEY DO NOT LINE UP IN A SNAPSHOT!

//
//        //The order from some reference point should be the same in all cooridinates for it to hit
//        for (long t = 0; t < 10000000; t++) {
//
//            if (t % 1000 == 0) System.out.println(t);
//
//            List<Pair<Integer, List<Long>>> list = new ArrayList<>();
//
//            for (HailStone stone : hailstones) {
//                List<Long> positionAt = positionAtTimeRelativeTo(stone, t, refPoint);
//                list.add(Pair.from(stone.identifier(), positionAt));
//            }
//
//            var xorder = list.stream().sorted(Comparator.comparingLong(p -> p.second().get(0))).toList();
//            var yorder = list.stream().sorted(Comparator.comparingLong(p -> p.second().get(1))).toList();
//            var zorder = list.stream().sorted(Comparator.comparingLong(p -> p.second().get(2))).toList();
//
//            if (xorder.equals(yorder)) {
//                System.out.println("X equals Y: " + t);
//                if ( yorder.equals(zorder)) {
//                    System.out.println("yaaaay at time: " + t);
//                    return;
//                }
//            }
//        }


    private static void stash(long vx_min, long vx_max, long x0_max, long x0_min, List<HailStone> hailstones)
    {
        whatever:
        for (long vs = vx_min * 2; vs < (vx_max * 2); vs++)
        {
            if ((vs > vx_min && vs < vx_max))
            {
                continue;
            }
            long xs = vs < 0 ? x0_max : x0_min; //first guess

            //do correction, try again
            boolean done = false;
            outter:
            while (!done)
            {
                done = true;
                for (HailStone stone : hailstones)
                {
                    long xs_new = findXs(stone, vs, xs);
                    if (xs_new != xs)
                    {
                        xs = xs_new;
                        done = false;

                        if (xs == 24)
                        {
                            System.out.println("");
                        }

                        if ((xs > 1.5 * x0_max || xs < 0.75 * x0_min))
                        {
                            break outter;
                        }
                        break;
                    }
                }
                if (done)
                {
                    System.out.println("-----> xs " + xs + " vs " + vs);
                    break whatever;
                }

            }
        }
    }

    private static Long findXs(HailStone stone, long vs, long xs)
    {
        long x0 = stone.initialPosition().x();
        long va = stone.dirVector().x();

        if (vs == va || xs == x0)
        {
            if (xs == x0 && vs == va)
            {
                return xs;
            }
            if (vs == va)
            {
                return Long.MAX_VALUE; //impossible
            }
            xs = xs - va;
        }

        long x_corr = (Math.abs(xs - x0) % Math.abs(va - vs));

        long modifier;
        if (vs > 0)
        {
            modifier = x_corr != 0 ? -x_corr : 0;
        } else
        {
            modifier = x_corr != 0 ? Math.abs(va - vs) - x_corr : 0;
        }

        //    System.out.println(x_corr + " modifier: " + modifier);

        long t_collision = ((xs + modifier) - x0) / (va - vs);

        //   System.out.println("actial x0: " + (xs + modifier) + " at vs " + vs + " at time " + t_collision);
        return xs + modifier;
    }

    private static void findInterceptionParameters(HailStone stoneB, Coordinate3D refPoint)
    {
        for (int t = 0; t < 10; t++)
        {
            if (t == 0)
            {
                continue;
            }

            long diffFromMultiple = (stoneB.initialPosition().x() - refPoint.x() + t) % t;
            long correctionToRefpoint = -diffFromMultiple;
            long x0 = refPoint.x() + t - correctionToRefpoint;


            var interceptVelocity = interceptionVelocityX(stoneB, x0, t);
            System.out.println("at timt " + t + " stoneB " + interceptVelocity + " with start pos: " + x0);

        }
    }


    public static boolean interSectsTestBox(HailStone stone, long limit_low, long limit_high)
    {
        var stonePos = stone.initialPosition();
        var stoneDir = stone.dirVector();
        // t_min = (X_low - x0) / v_x
        double t_x_enter = (limit_low - stonePos.x()) / (double) stoneDir.x();
        double t_x_exit = (limit_high - stonePos.x()) / (double) stoneDir.x();

        double t_y_enter = (limit_low - stonePos.y()) / (double) stoneDir.y();
        double t_y_exit = (limit_high - stonePos.y()) / (double) stoneDir.y();

        System.out.println(t_x_enter + " " + t_x_exit);

        //times should be positive, intersecting in the past not allowed
        if (t_x_exit < 0 || t_y_exit < 0) return false;

        //intersecting times should be in the future!
        return (between(t_x_enter, t_y_enter, t_y_exit) || between(t_y_enter, t_x_enter, t_x_exit));

    }


    public static boolean pathsIntersectWithingBox(HailStone stoneA, HailStone stoneB, long limit_low,
                                                   long limit_high)
    {
        var posA = stoneA.initialPosition();
        var dirA = stoneA.dirVector();
        var posB = stoneB.initialPosition();
        var dirB = stoneB.dirVector();
        System.out.println("solving: " + stoneA + "  " + stoneB);

        double x0_delta = posA.x() - posB.x();
        double vel_y_rat_A = dirA.x() / ((double) dirA.y());
        double vel_y_rat_B = dirB.x() / ((double) dirB.y());

        double y_intersect = solveMotionEquation(x0_delta, vel_y_rat_A, vel_y_rat_B, posA.y(), posB.y());

        double x_intersect = posA.x() + dirA.x() * ((y_intersect - posA.y()) / dirA.y());

        System.out.println("intersect at x: " + x_intersect + " y: " + y_intersect);
        if (!(x_intersect > limit_low && x_intersect < limit_high && y_intersect > limit_low && y_intersect < limit_high))
        {
            System.out.println("Not in the box!");
            return false;
        }
        //but at what time?

        var time_A = timeAtPositionX(x_intersect, stoneA);
        var time_B = timeAtPositionX(x_intersect, stoneB);
        System.out.println("at times: " + time_A + " " + time_B);
        boolean inTheFuture = (time_A > 0 && time_B > 0);
        if (!inTheFuture)
        {
            System.out.println("In the Past!");
        }
        return inTheFuture;
    }

    public static double solveMotionEquation(double x0_delta, double vel_y_rat_A, double vel_y_rat_B, double y_0A,
                                             double y_0B)
    {

        return (x0_delta - (vel_y_rat_A * y_0A) + (vel_y_rat_B * y_0B)) / (vel_y_rat_B - vel_y_rat_A);
    }

    public static double timeAtPositionX(double position_x, HailStone stone)
    {

        return (position_x - stone.initialPosition().x()) / stone.dirVector().x();

    }


    private static boolean isParallel_XY(Coordinate3D v1, Coordinate3D v2)
    {
        //normalized vectors are same
        double normFactorV1 = v1.x() + v1.y();
        double normFactorV2 = v2.x() + v2.y();

        return (doubleEquals(v1.x() / normFactorV1, v2.x() / normFactorV2, 0.0001)
                && doubleEquals(v1.y() / normFactorV1, v2.y() / normFactorV2, 0.0001));

    }

    public static boolean doubleEquals(double a, double b, double precision)
    {
        return (Math.abs(a - b) < precision);
    }

    public static List<Double> positionAtTime(HailStone stone, double time)
    {
        double x = stone.initialPosition().x() + stone.dirVector().x() * time;
        double y = stone.initialPosition().y() + stone.dirVector().y() * time;
        double z = stone.initialPosition().z() + stone.dirVector().z() * time;

        return List.of(x, y, z);
    }


    public static List<Long> positionAtTimeRelativeTo(HailStone stone, long time, Coordinate3D reference)
    {
        long x = stone.initialPosition().x() + stone.dirVector().x() * time - reference.x();
        long y = stone.initialPosition().y() + stone.dirVector().y() * time - reference.y();
        long z = stone.initialPosition().z() + stone.dirVector().z() * time - reference.z();

        return List.of(x, y, z);
    }

    private static boolean between(double a, double b, double c)
    {
        return a > b && a < c;
    }

    public static double interceptionVelocityX(HailStone stoneA, long x0, double time)
    {
        //special case??:
        if (stoneA.initialPosition().x() == x0)
        {
            return stoneA.dirVector().x();
        }

        return (stoneA.initialPosition().x() - x0) / time + stoneA.dirVector().x();
    }


    public static double interceptionVelocityY(HailStone stoneA, long y_0, double time)
    {
        //special case:
        if (time == 0 && stoneA.initialPosition().y() == y_0)
        {
            return 0;
        }

        return (stoneA.initialPosition().y() - y_0) / time + stoneA.dirVector().y();
    }

    public static double interceptionVelocityZ(HailStone stoneA, Coordinate3D startIntercept, double time)
    {
        //special case:
        if (time == 0 && stoneA.initialPosition().z() == startIntercept.z())
        {
            return 0;
        }

        return (stoneA.initialPosition().z() - startIntercept.x()) / time + stoneA.dirVector().z();
    }


}
