package nl.mrtijmen.aoc2023.snowforest;

import nl.mrtijmen.aoccommons.util.grid.Coordinate;
import nl.mrtijmen.aoccommons.util.grid.Direction;

public enum ForestTileType
{
    PATH("."),
    FOREST("#"),
    SLOPE_UP("^"),
    SLOPE_DOWN("v"),
    SLOPE_LEFT("<"),
    SLOPE_RIGHT(">");


    String token;

    ForestTileType(String token)
    {
        this.token = token;
    }

    public static ForestTileType fromToken(String token)
    {
        return switch (token)
        {
            case "." -> PATH;
            case "#" -> FOREST;
            case "^" -> SLOPE_UP;
            case ">" -> SLOPE_RIGHT;
            case "v" -> SLOPE_DOWN;
            case "<" -> SLOPE_LEFT;
            default -> throw new IllegalArgumentException();
        };
    }

    @Override
    public String toString()
    {
        return token;
    }


    public boolean isSlope()
    {
        return this == SLOPE_DOWN || this == SLOPE_UP || this == SLOPE_LEFT || this == SLOPE_RIGHT;
    }

    public Coordinate slopeTarget(Coordinate slopePostion)
    {
        return switch (this)
        {
            case SLOPE_UP -> Direction.UP.travel(slopePostion);
            case SLOPE_DOWN -> Direction.DOWN.travel(slopePostion);
            case SLOPE_LEFT -> Direction.LEFT.travel(slopePostion);
            case SLOPE_RIGHT -> Direction.RIGHT.travel(slopePostion);
            case PATH, FOREST -> null; //always ask, isSlope? first!
        };
    }
}
