package nl.mrtijmen.aoc2023;

import nl.mrtijmen.aoc2023.sequence.Sequence;
import nl.mrtijmen.aoccommons.util.FileNameUtil;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;
import nl.mrtijmen.aoccommons.util.ParseUtil;

import java.util.List;

public class Day09
{

    public static void main(String[] args) throws Exception
    {
       // List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName());

        List<String> data = FileReaderUtil.getStringList(FileNameUtil.dataFileName());

        List<Sequence> sequences = data.stream()
                                       .map(line -> ParseUtil.lineToLongList(line, " "))
                                       .map(Sequence::new)
                                       .toList();

        long result1 = sequences.stream().mapToLong(Sequence::calculateNext).peek(System.out::println).sum();
        System.out.println("result part 1 -> " + result1);


        long result2 = sequences.stream().mapToLong(Sequence::calculatePrevious).peek(System.out::println).sum();
        System.out.println("result part2 -> " + result2);

    }

}