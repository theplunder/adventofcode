package nl.mrtijmen.aoc2023;

import nl.mrtijmen.aoc2023.camelcard.Card;
import nl.mrtijmen.aoc2023.camelcard.Hand;
import nl.mrtijmen.aoc2023.camelcard.HighCardComparator;
import nl.mrtijmen.aoc2023.camelcard.PokerSetComparator;
import nl.mrtijmen.aoc2023.map.Direction;
import nl.mrtijmen.aoc2023.map.Node;
import nl.mrtijmen.aoc2023.map.NodeMap;
import nl.mrtijmen.aoccommons.util.FileNameUtil;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;
import nl.mrtijmen.aoccommons.util.ParseUtil;

import java.util.List;

public class Day08 {

    public static void main(String[] args) throws Exception {
        //List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName(1));
        //   List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName(2));


        //NOT COMPATIBLE WITH PART 1
        //List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName(3));

        List<String> data = FileReaderUtil.getStringList(FileNameUtil.dataFileName());

        List<Direction> instructions = ParseUtil.lineToObject(data.get(0), "", Direction::fromCode);

        NodeMap nodeMap = new NodeMap();

        data.subList(2, data.size()).stream()
                .map(s -> new Node(s.substring(0, 3), s.substring(7, 10), s.substring(12, 15)))
                .forEach(nodeMap::addNode);


        long result2 = nodeMap.testSingle(instructions);
        System.out.println("Result part 2 -->  " + result2);
    }

}