package nl.mrtijmen.aoc2023;

import nl.mrtijmen.aoccommons.util.FileNameUtil;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;
import nl.mrtijmen.aoccommons.util.ParseUtil;

import java.util.*;

public class Day25
{

    private static Set<Set<String>> wires = null;
    private static Map<String, Set<String>> connections;

    public static void main(String[] args) throws Exception
    {
        //List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName());
         List<String> data = FileReaderUtil.getStringList(FileNameUtil.dataFileName());


        wires = new HashSet<>();
        connections = new HashMap<>();

        for (String line : data)
        {
            var line1 = ParseUtil.lineToStringList(line, ":");
            var line2 = ParseUtil.lineToStringList(line1.get(1), " ");
            var key = line1.get(0);
            connections.computeIfAbsent(key, (k) -> new HashSet<>());
            connections.get(key).addAll(line2);

            for (String component : line2)
            {
                connections.computeIfAbsent(component, (k) -> new HashSet<>());
                wires.add(Set.of(key, component));
                connections.get(component).add(key);
            }
        }


        //for every node you can find at least 4 distinct paths to any of its neightbours without re-using wires.
        //EXCEPT the ones that can be cut!

        connections.entrySet().stream().forEach(System.out::println);
        System.out.println("there are " + connections.size() + "components");
        System.out.println("there are " + wires.size() + "wires");

        List<Set<String>> wiresToCut = new ArrayList<>();

        for (Set<String> wire : wires)
        {
            var wireL = wire.stream().toList();
            boolean has4Paths = find4Paths(wireL.get(0), wireL.get(1));
            if (!has4Paths)
            {
                wiresToCut.add(wire);
            }

        }

        System.out.println(wiresToCut);
        String aNode = wiresToCut.get(0).stream().findFirst().get();
        System.out.println("counting from " + aNode);
        int count = countConnectedNodes(aNode, wiresToCut);
        System.out.println(count);

        int result1 = (connections.size() - count) * count;
        System.out.println("Result Part 1 --> " + result1);

        int result2 = 0;
        System.out.println("Result Part 2 --> " + result2);
    }

    private static boolean find4Paths(String start, String target)
    {
        var path1 = findPathTo(start, target, Set.of());

        //   System.out.println(path1);

        Set<Set<String>> excludeSet = new HashSet<>();
        for (int i = 0; i < path1.size() - 1; i++)
        {
            excludeSet.add(Set.of(path1.get(i), path1.get(i + 1)));
        }

        var path2 = findPathTo(start, target, excludeSet);
        //   System.out.println(path2);

        if (path2.isEmpty())
        {
            return false;
        }

        for (int i = 0; i < path2.size() - 1; i++)
        {
            excludeSet.add(Set.of(path2.get(i), path2.get(i + 1)));
        }

        var path3 = findPathTo(start, target, excludeSet);
        //System.out.println(path3);

        if (path3.isEmpty())
        {
            return false;
        }


        for (int i = 0; i < path3.size() - 1; i++)
        {
            excludeSet.add(Set.of(path3.get(i), path3.get(i + 1)));
        }

        var path4 = findPathTo(start, target, excludeSet);
        //   System.out.println(path4);

        return !path4.isEmpty();
    }


    public static List<String> findPathTo(String start, String target, Set<Set<String>> excludedWires)
    {
        String current = start;
        Set<String> visited = new HashSet<>();
        visited.add(current);
        Queue<List<String>> queue = new LinkedList<>();

        List<String> currentPath = new ArrayList<>();
        currentPath.add(current);
        queue.add(currentPath);

        while (!queue.isEmpty())
        {
            currentPath = queue.poll();
            if (currentPath.getLast().equals(target))
            {
                return currentPath;
            }
            var currentPathF = currentPath;
            String currrentF = currentPath.getLast();
            var pathToNeighbours = connections.get(currrentF).stream()
                                              .filter(c -> !c.equals(currrentF))
                                              .filter(c -> !visited.contains(c))
                                              .filter(c -> !excludedWires.contains(Set.of(currrentF, c)))
                                              .map(c -> {
                                                  var list = new ArrayList<>(currentPathF);
                                                  list.add(c);
                                                  visited.add(c);
                                                  return list;
                                              })
                                              .toList();
            queue.addAll(pathToNeighbours);
        }
        return List.of();
    }

    public static int countConnectedNodes(String start, List<Set<String>> excludedWires)
    {

        String current = start;
        Set<String> visited = new HashSet<>();
        visited.add(current);
        Queue<String> queue = new LinkedList<>();

        queue.add(current);

        while (!queue.isEmpty())
        {
            current = queue.poll();
            String currrentF = current;
            var neighbours = connections.get(currrentF).stream()
                                        .filter(c -> !visited.contains(c))
                                        .filter(c -> !excludedWires.contains(Set.of(currrentF, c)))
                                        .toList();
            visited.addAll(neighbours);
            queue.addAll(neighbours);
        }
        return visited.size();

    }


}