package nl.mrtijmen.aoc2023;

import nl.mrtijmen.aoc2023.scratchcard.ScratchCard;
import nl.mrtijmen.aoccommons.util.ArrayUtil;
import nl.mrtijmen.aoccommons.util.FileNameUtil;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;
import nl.mrtijmen.aoccommons.util.ParseUtil;
import nl.mrtijmen.aoccommons.util.grid.Coordinate;
import nl.mrtijmen.aoccommons.util.grid.Grid;

import java.util.*;

public class Day04 {

    public static void main(String[] args) throws Exception {
        //List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName());
        List<String> data = FileReaderUtil.getStringList(FileNameUtil.dataFileName());

        List<ScratchCard> cards =
                data.stream()
                        .map(s -> s.substring(s.indexOf(':') + 1))
                        .map(s -> s.split("\\|"))
                        .map(sArr -> new ScratchCard(toIntList(sArr[0]), toIntList(sArr[1])))
                        .toList();

        double result = part1Score(cards);

        System.out.println("result part 1 --> " + result);

        long result2 = part2Score(cards);
        System.out.println("result part 2 --> " + result2);

    }

    private static double part1Score(List<ScratchCard> cards) {
        return cards.stream().map(ScratchCard::myWinningNumbers)
                .filter(list -> !list.isEmpty())
                .mapToDouble(List::size)
                .map(n -> Math.pow(2, n - 1))
                .sum();
    }


    private static long part2Score(List<ScratchCard> cards) {

        int[] cardCounts = new int[cards.size()];
        Arrays.fill(cardCounts, 1);

        for (int i = 0; i < cards.size(); i++) {
            ScratchCard card = cards.get(i);
            int winCount = card.myWinningNumbers().size();
            int cardCount = cardCounts[i];
            for( int j = i+1; j < i+1+winCount; j++){
                cardCounts[j] = cardCounts[j] + cardCount;
            }
        }
        return Arrays.stream(cardCounts).asLongStream().sum();

    }

    public static List<Integer> toIntList(String numbers) {
        return Arrays.stream(numbers.strip().split(" "))
                .toList().stream()
                .filter(s -> !s.isBlank())
                .map(Integer::parseInt)
                .toList();
    }


}