package nl.mrtijmen.aoc2023;

import nl.mrtijmen.aoc2023.colourcubegame.CubeColour;
import nl.mrtijmen.aoc2023.colourcubegame.Game;
import nl.mrtijmen.aoc2023.colourcubegame.Hand;
import nl.mrtijmen.aoccommons.util.FileNameUtil;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;

import java.util.Arrays;
import java.util.EnumMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static nl.mrtijmen.aoc2023.colourcubegame.CubeColour.*;

public class Day02
{
    private static final Pattern redPattern = Pattern.compile("((\\d+) red)");
    private static final Pattern greenPattern = Pattern.compile("((\\d+) green)");
    private static final Pattern bluePattern = Pattern.compile("((\\d+) blue)");


    public static void main(String[] args) throws Exception
    {
        //List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName());
        List<String> data = FileReaderUtil.getStringList(FileNameUtil.dataFileName());

        List<Game> games = data.stream().map(Day02::parseGame).toList();

        int resultP1 = part1(games);
        System.out.println("Result Part 1 --> " + resultP1);

        int resultP2 = part2(games);
        System.out.println("Result Part 2 --> " + resultP2);
    }

    private static int part1(List<Game> games)
    {
        final EnumMap<CubeColour, Integer> requirements = new EnumMap<>(CubeColour.class);
        requirements.put(RED, 12);
        requirements.put(GREEN, 13);
        requirements.put(BLUE, 14);

        return games.stream()
                    .filter(game -> matchesRequirements(game.minRequiredCubes(), requirements))
                    .mapToInt(Game::getId)
                    .sum();
    }

    private static int part2(List<Game> games)
    {
        return games.stream()
                    .map(Game::minRequiredCubes)
                    .mapToInt(map -> map.get(RED) * map.get(GREEN) * map.get(BLUE))
                    .sum();
    }


    public static boolean matchesRequirements(EnumMap<CubeColour, Integer> actual, EnumMap<CubeColour, Integer> requirements)
    {
        for (CubeColour colour : CubeColour.values())
        {
            if (actual.get(colour) > requirements.get(colour))
            {
                return false;
            }
        }
        return true;
    }

    private static Game parseGame(String gameString)
    {
        String[] split = gameString.split(":");
        int gameId = Integer.parseInt(split[0].substring("Game ".length()).strip());

        Game game = new Game(gameId);

        parseHands(split[1]).forEach(game::addHand);
        return game;
    }

    private static List<Hand> parseHands(String gameString)
    {
        System.out.println(gameString);
        String[] handStrings = gameString.split(";");
        var hands = Arrays.stream(handStrings).map(String::strip).map(Day02::parseHand).toList();
        System.out.println(hands);
        return hands;
    }

    private static Hand parseHand(String handString)
    {


        int red = findNumber(handString, redPattern);
        int green = findNumber(handString, greenPattern);
        int blue = findNumber(handString, bluePattern);

        return new Hand(red, green, blue);
    }

    private static int findNumber(String handString, Pattern pattern)
    {
        Matcher m = pattern.matcher(handString);
        if (m.find())
        {
            if (m.group(2) != null)
            {
                return Integer.parseInt(m.group(2));
            }
        }
        return 0;
    }

}