package nl.mrtijmen.aoc2023.lens;

import java.util.Optional;

public record InitializationStep(String boxHash, String operation, Optional<Integer> focalLength)
{

    // The relevant Regex (\w+)([\D\W\S]{1})(\d)?

    //    @Override
    @Override
    public boolean equals(Object other)
    {
        if (this == other)
        {
            return true;
        }
        if (other == null)
        {
            return false;
        }
        if (!(other instanceof InitializationStep otherIS))
        {
            return false;
        }
        return otherIS.boxHash.equals(this.boxHash);
    }

}
