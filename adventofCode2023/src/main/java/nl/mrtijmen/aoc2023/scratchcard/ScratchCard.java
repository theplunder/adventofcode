package nl.mrtijmen.aoc2023.scratchcard;

import java.util.List;

public record ScratchCard(List<Integer> winningNumbers, List<Integer> myNumbers) {

    public List<Integer> myWinningNumbers() {

        return myNumbers.stream().filter(winningNumbers::contains).toList();


    }
}
