package nl.mrtijmen.aoc2023.map;

public enum Direction {

    LEFT("L"),
    RIGHT("R");

    String code;

    Direction(String code) {
        this.code = code;
    }

    public static Direction fromCode(String code) {
        return switch (code) {
            case "L" -> LEFT;
            case "R" -> RIGHT;
            default -> throw new IllegalArgumentException();
        };
    }


}
