package nl.mrtijmen.aoc2023.map;

import nl.mrtijmen.aoccommons.util.MathUtil;

import java.util.*;

public class NodeMap {


    private Map<String, Node> nodes = new HashMap<>();

    private List<Node> ghostStartNode = new ArrayList<>();
    private static final String START_ID = "AAA";
    private static final String END_ID = "ZZZ";

    public void addNode(Node node) {
        nodes.put(node.id(), node);
        if (node.isGhostStart()) {
            ghostStartNode.add(node);
        }
    }

    public List<Node> route(List<Direction> instructions) {
        List<Node> route = new ArrayList<>();
        Node currentNode = nodes.get(START_ID);
        route.add(currentNode);
        int instrIndex = 0;
        int instrCount = instructions.size();
        for (; !currentNode.id().equals(END_ID); instrIndex++) {
            Direction direction = instructions.get(instrIndex % instrCount);
            String nextNodeId = currentNode.getNodeId(direction);
            currentNode = nodes.get(nextNodeId);
            route.add(currentNode);
        }

        return route;
    }


    //THIS TAKES WAAAAAAAAAAAAAAAAAAAAAY TO LONG!
    public long routeForGhosts(List<Direction> instructions) {
        long stepCount = 0;
        List<Node> currentNodes = ghostStartNode;
        int instrIndex = 0;
        int instrCount = instructions.size();
        for (; !allGhostEndNodes(currentNodes); instrIndex++) {
            Direction direction = instructions.get(instrIndex % instrCount);
            List<Node> nextStep = new ArrayList<>();
            for (Node currentNode : currentNodes) {
                String nextNodeId = currentNode.getNodeId(direction);
                currentNode = nodes.get(nextNodeId);
                nextStep.add(currentNode);
            }
            currentNodes = nextStep;
            stepCount++;
        }

        return stepCount;
    }


    public Pattern findLoopPattern(List<Direction> instructions, Node start) {
        Node currentNode = start;
        int instrIndex = 0;
        int instrCount = instructions.size();
        List<Instruction> instructionList = new ArrayList<>();
        int ghostNodeStep = 0;
        for (; ; instrIndex++) {
            Direction direction = instructions.get(instrIndex % instrCount);
            String nextNodeId = currentNode.getNodeId(direction);
            Node nextNode = nodes.get(nextNodeId);

            var instr = new Instruction(instrIndex % instrCount, currentNode, nextNode);

            if (instructionList.contains(instr)) {
                int previous = instructionList.indexOf(instr);

                System.out.println("LOOP AT " + instr);
                System.out.println("AT STEP " + instrIndex);
                System.out.println("FIRST AT " + previous);
                System.out.println("LOOP LENGTH: " + (instrIndex - previous));
                Pattern pattern = new Pattern(ghostNodeStep, (instrIndex - previous));
                System.out.println(pattern);
                return pattern;
            }

            instructionList.add(instr);
            currentNode = nextNode;

            if (currentNode.isGhostEnd()) {
                System.out.println("ghostEnd at step " + (instrIndex + 1) + " " + currentNode);
                ghostNodeStep = (instrIndex + 1);
            }
        }
    }

    public long testSingle(List<Direction> instructions) {
        List<Pattern> patterns = new ArrayList<>();

        for (Node node : ghostStartNode) {
            Pattern pattern = findLoopPattern(instructions, node);
            patterns.add(pattern);
        }

        patterns = patterns.stream().sorted(Comparator.comparingInt(Pattern::looplength)).toList();

        long[] numbers = patterns.stream().mapToLong(Pattern::looplength).toArray();

        return MathUtil.leastCommonMultiple(numbers);

    }

    private boolean allGhostEndNodes(List<Node> nodes) {
        //   System.out.println(nodes);
        for (Node node : nodes) {
            if (!node.isGhostEnd()) {
                return false;
            }
        }
        return true;
    }


    record Instruction(int index, Node origin, Node target) {
    }

    record Pattern(int stepsToEnd, int looplength) {
    }

}
