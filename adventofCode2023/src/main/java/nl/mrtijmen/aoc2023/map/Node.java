package nl.mrtijmen.aoc2023.map;

public record Node(String id, String leftId, String rightId) {

    public Node {

        if (id.equals(leftId) && id.equals(rightId)){
            System.out.println("!!!!!!! INFINITE LOOP NODE");
        }
    }

    public String getNodeId(Direction dir) {
        return switch (dir) {
            case LEFT -> leftId;
            case RIGHT -> rightId;
        };
    }

    public boolean isGhostStart() {
        return id.endsWith("A");
    }

    public boolean isGhostEnd() {
        return id.endsWith("Z");
    }

}
