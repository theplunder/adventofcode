package nl.mrtijmen.aoc2023;

import nl.mrtijmen.aoc2023.camelcard.Card;
import nl.mrtijmen.aoc2023.camelcard.Hand;
import nl.mrtijmen.aoc2023.camelcard.HighCardComparator;
import nl.mrtijmen.aoc2023.camelcard.PokerSetComparator;
import nl.mrtijmen.aoccommons.util.FileNameUtil;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;
import nl.mrtijmen.aoccommons.util.ParseUtil;

import java.util.List;

public class Day07
{
    private static final PokerSetComparator pokerComparator = new PokerSetComparator();
    private static final HighCardComparator highCardComparator = new HighCardComparator();

    public static void main(String[] args) throws Exception
    {
        // List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName());
        List<String> data = FileReaderUtil.getStringList(FileNameUtil.dataFileName());

        part1(data);
        part2(data);
    }

    private static void part1(List<String> data)
    {
        List<Hand> hands = data.stream()
                               .map(s -> s.split(" "))
                               .map(s -> new Hand(ParseUtil.lineToObject(s[0], "", Card::fromCode), Integer.parseInt(s[1])))
                               .toList();

        long result = calculateWinnings(hands);
        System.out.println("Result part 1 --> " + result);
    }


    private static void part2(List<String> data)
    {
        List<Hand> hands = data.stream()
                               .map(s -> s.split(" "))
                               .peek(s -> s[0] = s[0].replace("J", "X"))
                               .map(s -> new Hand(ParseUtil.lineToObject(s[0], "", Card::fromCode), Integer.parseInt(s[1])))
                               .toList();

        long result = calculateWinnings(hands);

        System.out.println("Result part 2 --> " + result);
    }

    private static long calculateWinnings(List<Hand> hands)
    {
        var sortedHands = hands.stream().sorted(pokerComparator.thenComparing(highCardComparator)).toList();

        long result = 0;
        for (int i = 0; i < sortedHands.size(); i++)
        {
            result += (long) (i + 1) * sortedHands.get(i).bid();
        }
        return result;
    }

}