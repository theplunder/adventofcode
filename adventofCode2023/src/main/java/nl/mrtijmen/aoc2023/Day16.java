package nl.mrtijmen.aoc2023;

import nl.mrtijmen.aoc2023.lightray.Component;
import nl.mrtijmen.aoc2023.lightray.ComponentType;
import nl.mrtijmen.aoccommons.util.ArrayUtil;
import nl.mrtijmen.aoccommons.util.FileNameUtil;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;
import nl.mrtijmen.aoccommons.util.ParseUtil;
import nl.mrtijmen.aoccommons.util.grid.Coordinate;
import nl.mrtijmen.aoccommons.util.grid.Grid;
import nl.mrtijmen.aoccommons.util.object.Pair;

import java.util.ArrayList;
import java.util.List;

public class Day16
{
    private static Grid<Component> componentGrid;
    private static Grid<VitiorLog> vitiorLogGrid;

    private static final Coordinate START = new Coordinate(-1, 0);

    public static void main(String[] args) throws Exception
    {
        Coordinate.UP_IS_NEGATIVE_Y = true;
//        List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName());

        List<String> data = FileReaderUtil.getStringList(FileNameUtil.dataFileName());

        var tokensGrid = data.stream().map(l -> ParseUtil.lineToStringList(l, "")).toList();
        componentGrid = new Grid<>(ArrayUtil.poitionAwareConvert(tokensGrid, (token, coordinate) -> new Component(coordinate, ComponentType.intialize(token))));

        vitiorLogGrid = new Grid<>(componentGrid.xSize(), componentGrid.ySize(), VitiorLog.class);
        vitiorLogGrid.visitAndUpdateAllCoordinates(((coordinate, vitiorLog) -> new VitiorLog()));

        System.out.println(componentGrid);

        travel(START, new Coordinate(0, 0));


        int energizeCount = vitiorLogGrid.find(vl -> vl.visitCount() != 0).size();

        System.out.println("Result part 1 --> " + energizeCount);
        //System.out.println(startingRays);

        var energizedCount = travelBorders();
        System.out.println(energizedCount);

        var result2 = energizedCount.stream().mapToInt(i -> i).max().getAsInt();
        System.out.println("Result part 2 --> " + result2);

    }

    private static List<Integer> travelBorders()
    {
        var startingRays = generateStartingRays();

        List<Integer> energizedCount = new ArrayList<>();
        for (Pair<Coordinate, Coordinate> startingRay : startingRays)
        {
            //reset our logging Grid
            vitiorLogGrid.visitAndUpdateAllCoordinates(((coordinate, vitiorLog) -> new VitiorLog()));

            travel(startingRay.first(), startingRay.second());
            int energizeCount = vitiorLogGrid.find(vl -> vl.visitCount() != 0).size();
            energizedCount.add(energizeCount);
        }
        return energizedCount;
    }

    public static List<Pair<Coordinate, Coordinate>> generateStartingRays()
    {
        List<Pair<Coordinate, Coordinate>> result = new ArrayList<>();
        for (int i = 0; i < componentGrid.xSize(); i++)
        {
            var up = new Coordinate(i, -1);
            result.add(Pair.from(up, up.down()));

            var down = new Coordinate(i, componentGrid.ySize());
            result.add(Pair.from(down, down.up()));
        }
        for (int i = 0; i < componentGrid.ySize(); i++)
        {
            var left = new Coordinate(-1, i);
            result.add(Pair.from(left, left.right()));

            var right = new Coordinate(componentGrid.xSize(), i);
            result.add(Pair.from(right, right.left()));
        }
        return result;

    }

    public static void travel(Coordinate from, Coordinate to)
    {
        while (componentGrid.coordinateWithinRange(to) && !vitiorLogGrid.getValue(to).hasBeenVisitedBefore(from))
        {
            vitiorLogGrid.getValue(to).addVisitFrom(from);

//            System.out.println(vitiorLogGrid);
//            System.out.println();

            var toComponent = componentGrid.getValue(to);
            Coordinate next;

            if (toComponent.willItSplit(from))
            {
                var splitResult = toComponent.split();
                travel(to, splitResult.get(0));
                next = splitResult.get(1);
                //but should we split?
            } else
            {
                next = toComponent.moveThrough(from);
            }

            from = to;
            to = next;
        }


    }

    public static class VitiorLog
    {
        List<Coordinate> visitsFrom = new ArrayList<>();

        public void addVisitFrom(Coordinate coordinate)
        {
            visitsFrom.add(coordinate);
        }

        public boolean hasBeenVisitedBefore(Coordinate coordinate)
        {
            return visitsFrom.contains(coordinate);
        }

        public int visitCount()
        {
            return visitsFrom.size();

        }

        @Override
        public String toString()
        {
            return String.valueOf(visitsFrom.size());
        }
    }


}