package nl.mrtijmen.aoc2023.lightray;

import java.util.Arrays;

public enum ComponentType
{
    EMPTY_SPACE("."),
    MIRROR_FW("/"),
    MIRROR_BW("\\"),
    SPLITTER_VERTICAL("|"),
    SPLITTER_HORIZONTAL("-");
    private String token;

    ComponentType(String token)
    {
        this.token = token;
    }

    public static ComponentType intialize(String fromToken)
    {
        return Arrays.stream(ComponentType.values()).filter(t -> t.token.equals(fromToken)).findFirst().orElseThrow();
    }

    public boolean isSplitter()
    {
        return (this == SPLITTER_HORIZONTAL || this == SPLITTER_VERTICAL);
    }

    public String getToken()
    {
        return token;
    }
}
