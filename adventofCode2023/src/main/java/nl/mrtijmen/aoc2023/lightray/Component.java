package nl.mrtijmen.aoc2023.lightray;

import nl.mrtijmen.aoccommons.util.grid.Coordinate;

import java.util.List;

public record Component(Coordinate location, ComponentType type)
{

    //note probably you need to set UP_IS_NEGATIVE_Y
    public Coordinate moveThrough(Coordinate origin)
    {
        Coordinate up = location.up();
        Coordinate down = location.down();
        Coordinate left = location.left();
        Coordinate right = location.right();

        return switch (type)
        {
            case EMPTY_SPACE -> emptySpaceTravelBehaviour(origin, up, down, left, right);
            case MIRROR_FW -> mirrorFWTravelBehaviour(origin, up, down, left, right);
            case MIRROR_BW -> mirrorBWTravelBehaviour(origin, up, down, left, right);
            case SPLITTER_VERTICAL -> splitterVerticalTravelBehaviour(origin, up, down, left, right);
            case SPLITTER_HORIZONTAL -> splitterHorizontalTravelBehaviour(origin, up, down, left, right);
        };
    }

    private Coordinate emptySpaceTravelBehaviour(Coordinate origin, Coordinate up, Coordinate down, Coordinate left, Coordinate right)
    {
        if (origin.equals(up)) return down;
        if (origin.equals(down)) return up;
        if (origin.equals(left)) return right;
        if (origin.equals(right)) return left;
        throw new IllegalStateException(String.valueOf(this));
    }

    private Coordinate mirrorFWTravelBehaviour(Coordinate origin, Coordinate up, Coordinate down, Coordinate left, Coordinate right)
    {
        //     [/]
        if (origin.equals(up)) return left;
        if (origin.equals(down)) return right;
        if (origin.equals(left)) return up;
        if (origin.equals(right)) return down;
        throw new IllegalStateException(String.valueOf(this));
    }

    private Coordinate mirrorBWTravelBehaviour(Coordinate origin, Coordinate up, Coordinate down, Coordinate left, Coordinate right)
    {
        //     [\]
        if (origin.equals(up)) return right;
        if (origin.equals(right)) return up;
        if (origin.equals(down)) return left;
        if (origin.equals(left)) return down;
        throw new IllegalStateException(String.valueOf(this));
    }

    private Coordinate splitterVerticalTravelBehaviour(Coordinate origin, Coordinate up, Coordinate down, Coordinate left, Coordinate right)
    {
        if (origin.equals(up)) return down;
        if (origin.equals(down)) return up;
        throw new IllegalStateException("With Origin : " + origin + " self: " + this);
    }

    private Coordinate splitterHorizontalTravelBehaviour(Coordinate origin, Coordinate up, Coordinate down, Coordinate left, Coordinate right)
    {
        if (origin.equals(left)) return right;
        if (origin.equals(right)) return left;
        throw new IllegalStateException("With Origin : " + origin + " self: " + this);
    }


    public List<Coordinate> split()
    {
        if (!type.isSplitter())
        {
            throw new IllegalStateException("this is not a splitter " + this);
        }
        if (type == ComponentType.SPLITTER_VERTICAL)
        {
            return List.of(location.up(), location.down());
        }
        //then we are Horizontal
        return List.of(location.left(), location.right());
    }

    public boolean willItSplit(Coordinate from)
    {
        if (!type.isSplitter())
        {
            return false;
        }

        if (type == ComponentType.SPLITTER_HORIZONTAL)
        {
            return location.equals(from.up()) || location.equals(from.down());
        }
        //then we are Horizontal
        return location.equals(from.left()) || location.equals(from.right());
    }

    @Override
    public String toString()
    {
        return type.getToken();

    }

}
