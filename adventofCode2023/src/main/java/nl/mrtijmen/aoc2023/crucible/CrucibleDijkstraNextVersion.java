package nl.mrtijmen.aoc2023.crucible;

import nl.mrtijmen.aoccommons.util.grid.Coordinate;
import nl.mrtijmen.aoccommons.util.grid.Direction;
import nl.mrtijmen.aoccommons.util.grid.Grid;

import java.util.*;

public class CrucibleDijkstraNextVersion
{

    private final Grid<Integer> graph;
    private final Grid<Integer> valueGrid;

    private Map<Coordinate, Integer> unvisitedValues;

    private final Grid<EnumMap<Direction, Integer>> alternativeTravelValues;

    boolean flagDoNotMarkVisited = false;

    public CrucibleDijkstraNextVersion(Grid<Integer> graph)
    {
        this.graph = graph;
        valueGrid = new Grid<>(graph.xSize(), graph.ySize(), Integer.MAX_VALUE);
        alternativeTravelValues = new Grid<>(graph.xSize(), graph.ySize(), (c) -> new EnumMap<>(Direction.class));
        for (Direction dir : Direction.values())
        {
            alternativeTravelValues.getValue(0, 0).put(dir, 0);
        }
        graph.setValue(new Coordinate(0, 0), 0);

    }

    public Grid<Integer> calculateValueGrid(Coordinate targetNode)
    {
        unvisitedValues = new HashMap<>();


        Coordinate currentNode = new Coordinate(0, 0);
        valueGrid.setValue(currentNode, 0);
        int i = 0;
        while (true)
        {
            if (i++ > 1000000) break;
            Coordinate finalCurrentNode = currentNode;
            currentNode.findNeighbours().stream()
                       .filter(graph::coordinateWithinRange)
                       .forEach(neighbour -> processNeighbours(finalCurrentNode, neighbour));

            if (!flagDoNotMarkVisited)
            {
                unvisitedValues.remove(currentNode);
            }
            flagDoNotMarkVisited = false;

            if (unvisitedValues.isEmpty() || currentNode.equals(targetNode))
            {
                break;
            }

            currentNode = findCurrentNode();
            //update current node

        }

        return valueGrid;
    }

    private void processNeighbours(Coordinate currentNode, Coordinate neighbour)
    {

        Direction direction = Direction.findDirection(currentNode, neighbour);
        if (alternativeTravelValues.getValue(neighbour).containsKey(direction)){
            return;
        }

        int valueViaC = cheapestPathWithLimitations(currentNode, direction.reverse()) + graph.getValue(neighbour);
        if (valueViaC > 100000)
        {
            int nextUnvisitedValue = unvisitedValues.entrySet()
                                                    .stream().filter(e -> e.getKey() != currentNode)
                                                    .min(Comparator.comparingInt(Map.Entry::getValue))
                                                    .get().getValue();

            unvisitedValues.put(currentNode, nextUnvisitedValue + 1);
            flagDoNotMarkVisited = true;
            //we could not yet find a path! :o
            return;
        }
        //Direction is direction of entry!
        alternativeTravelValues.getValue(neighbour).put(direction, valueViaC);

        valueGrid.update(neighbour, val ->
                (valueViaC < val) ? valueViaC : val
        );
        unvisitedValues.compute(neighbour, (key, val) -> (val == null || valueViaC < val) ? valueViaC : val);
    }

    public int cheapestPathWithLimitations(Coordinate here, Direction lineDir)
    {
        int stepLimit = 3;
        int pathval = 0;
        int lowestOtherBranch = 100000;
        var current = here;
        for (int i = 0; i < stepLimit; i++)
        {
            int cheapestBranch = 1000000;
            for (Direction flank : Direction.values())
            {
                if (flank == lineDir || flank == lineDir.reverse()) continue;
                var flankPos = flank.travel(current);
                if (alternativeTravelValues.coordinateWithinRange(flankPos) && alternativeTravelValues.getValue(current)
                                                                                                      .get(flank.reverse()) != null)
                {
                    int flankvalue = alternativeTravelValues.getValue(current).get(flank.reverse());
                    cheapestBranch = Math.min(flankvalue, cheapestBranch);
                }
            }

            lowestOtherBranch = Math.min(lowestOtherBranch, cheapestBranch + pathval);
            pathval += graph.getValue(current);

            current = lineDir.travel(current);
            if (!graph.coordinateWithinRange(current))
            {
                break;
            }

        }
        return lowestOtherBranch;

    }


    public int findLowestValue(Coordinate here, Coordinate origin)
    {
        Direction direction = Direction.findDirection(here, origin);
        int value = 100000;
        for (Direction dir : Direction.values())
        {
            if (dir == direction) continue;
            value = Math.min(Optional.ofNullable(alternativeTravelValues.getValue(here).get(dir)).orElse(10000), value);
        }
        return value;
    }

    private Coordinate findCurrentNode()
    {
        return unvisitedValues.entrySet()
                              .stream().min(Comparator.comparingInt(Map.Entry::getValue))
                              .get()
                              .getKey();
    }


    /**
     *
     * https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm
     * Mark all nodes unvisited. Create a set of all the unvisited nodes called the unvisited set.
     * Assign to every node a tentative distance value: set it to zero for our initial node and to infinity for all other nodes. The tentative distance of a node v is the length of the shortest path discovered so far between the node v and the starting node. Since initially no path is known to any other vertex than the source itself (which is a path of length zero), all other tentative distances are initially set to infinity. Set the initial node as current.[15]
     * For the current node, consider all of its unvisited neighbors and calculate their tentative distances through the current node. Compare the newly calculated tentative distance to the current assigned value and assign the smaller one. For example, if the current node A is marked with a distance of 6, and the edge connecting it with a neighbor B has length 2, then the distance to B through A will be 6 + 2 = 8. If B was previously marked with a distance greater than 8 then change it to 8. Otherwise, the current value will be kept.
     * When we are done considering all of the unvisited neighbors of the current node, mark the current node as visited and remove it from the unvisited set. A visited node will never be checked again.
     * If the destination node has been marked visited (when planning a route between two specific nodes) or if the smallest tentative distance among the nodes in the unvisited set is infinity (when planning a complete traversal; occurs when there is no connection between the initial node and remaining unvisited nodes), then stop. The algorithm has finished.
     * Otherwise, select the unvisited node that is marked with the smallest tentative distance, set it as the new current node, and go back to step 3.
     */


}
