package nl.mrtijmen.aoc2023.crucible;

import nl.mrtijmen.aoccommons.util.grid.Coordinate;
import nl.mrtijmen.aoccommons.util.grid.Grid;

import java.util.*;
import java.util.stream.Stream;

public class RoadNodeCrucible

{
    private final Grid<Integer> graph;
    private final Grid<Integer> travelValues;
    private final HashMap<Coordinate, List<Coordinate>> origins;


    //Let's do it completely different!

    List<CrucibleRoadNode> roads;


    public RoadNodeCrucible(Grid<Integer> graph)
    {
        this.graph = graph;
        travelValues = new Grid<>(graph.xSize(), graph.ySize(), Integer.MAX_VALUE);
        origins = new HashMap<>();
    }


    public Grid<Integer> fillGrid(Coordinate target)
    {
        roads = new ArrayList<>();
        var inital = CrucibleRoadNode.startNode();

        travelValues.setValue(new Coordinate(0, 0), 0);
        inital.probeNextSteps(graph, travelValues, origins);
        roads.add(inital);


        for (int i = 1; i < 1000 && travelValues.getValue(target) == Integer.MAX_VALUE; i++)
        {
            final int ii = i;
            System.out.println(ii);
            var newRoadsCollection = new ArrayList<CrucibleRoadNode>();
            for (CrucibleRoadNode road : roads)
            {
                var newRoads = road.move(i, origins);
                newRoadsCollection.addAll(newRoads);
            }

            for (CrucibleRoadNode newRoad : newRoadsCollection)
            {
                if (travelValues.getValue(newRoad.getLast()) == Integer.MAX_VALUE)
                {
                    travelValues.setValue(newRoad.getLast(), i);
                }
            }

            int limit = newRoadsCollection.size();
            for (int j = 0; j < limit; j++)
            {
                ListIterator<CrucibleRoadNode> iter = newRoadsCollection.listIterator();
                var compareWith = newRoadsCollection.get(j);
                //   System.out.println("compareWith: " + compareWith);
                while (iter.hasNext())
                {
                    var other = iter.next();
                    if ((other != compareWith && other.headsEqual(compareWith, 4)))
                    {
                        //   System.out.println("remove: " + other);
                        iter.remove();
                    }
                }
                limit = newRoadsCollection.size();
            }
            //       System.out.println("--------------");

            for (CrucibleRoadNode newRoad : newRoadsCollection)
            {
                newRoad.probeOldCrucibleSteps(graph, travelValues, origins);
                //newRoad.probeNextSteps(graph, travelValues);
            }

            roads = Stream.concat(roads.stream(), newRoadsCollection.stream())
                          .filter(r -> !r.isDead(ii))
                          .toList();


            // only new road            roads.forEach(r -> r.probeNextSteps(graph, travelValues));
        }

        Map<Coordinate, Integer> count = new HashMap<>();
        for (CrucibleRoadNode node : roads)
        {
            count.compute(node.position, (p, v) -> v == null ? 1 : v + 1);
        }


        count.entrySet().forEach(e -> System.out.println(e.getKey() + " : " + e.getValue()));


        return travelValues;
    }



}
