package nl.mrtijmen.aoc2023.crucible;

import nl.mrtijmen.aoccommons.util.grid.Coordinate;
import nl.mrtijmen.aoccommons.util.grid.Direction;
import nl.mrtijmen.aoccommons.util.grid.Grid;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CrucibleRoadNode
{

    private final int myvalue;
    private boolean isDead = false;
    private Map<Coordinate, Integer> nextSteps = new HashMap<>();
    private int maxValue = 0;

    CrucibleRoadNode previous;

    Coordinate position;

    int stepnumber;

    private CrucibleRoadNode()
    {
        position = new Coordinate(0, 0);
        stepnumber = 0;
        previous = null;
        myvalue = 0;

    }

    public static CrucibleRoadNode startNode()
    {
        return new CrucibleRoadNode();
    }

    public CrucibleRoadNode(CrucibleRoadNode previous, Coordinate position, int value)
    {
        this.previous = previous;
        this.position = position;
        this.stepnumber = previous.stepnumber + 1;
        this.myvalue = value;
    }


    public void probeNextSteps(Grid<Integer> graph, Grid<Integer> valueGrid, HashMap<Coordinate, List<Coordinate>> origins)
    {
        var possibleNestSteps = position.findNeighbours().stream()
                                        .filter(c -> previous == null || !c.equals(previous.position))
                                        .filter(graph::coordinateWithinRange)
                                        .toList();

        //TODO mark as dead?
        if (possibleNestSteps.isEmpty())
        {
            isDead = true;
        }


        updatNextSteps(graph, possibleNestSteps, origins);
    }

    private void updatNextSteps(Grid<Integer> graph, List<Coordinate> possibleNestSteps, HashMap<Coordinate, List<Coordinate>> origins)
    {
        for (Coordinate nextStep : possibleNestSteps)
        {
//            if (origins.containsKey(nextStep) && origins.get(nextStep).contains(position)){
//                continue;
//            }
            int nextStepValue = myvalue + graph.getValue(nextStep);
            nextSteps.put(nextStep, nextStepValue);
            maxValue = Math.max(maxValue, nextStepValue);
        }
    }


    public void probeOldCrucibleSteps(Grid<Integer> graph, Grid<Integer> valueGrid, HashMap<Coordinate, List<Coordinate>> origins)
    {
        if (!unknownValueWithinRange(3, valueGrid))
        {
            isDead = true;
            return;
        }

        var possibleNestSteps = position.findNeighbours().stream()
                                        .filter(c -> previous == null || !c.equals(previous.position))
                                        .filter(graph::coordinateWithinRange)
                                        .filter(c -> !pathTailXinARow(3, c))
                                        .toList();

        //TODO mark as dead?
        if (possibleNestSteps.isEmpty())
        {
            isDead = true;
        }

        updatNextSteps(graph, possibleNestSteps, origins);
    }

    public void probeUltraCucibleSteps(Grid<Integer> graph, Grid<Integer> valueGrid, HashMap<Coordinate, List<Coordinate>> origins)
    {
        //add rules here! to determine new endpoints
        //for now basic pathfinding

        //TODO special case for first step!

        var possibleNestSteps = position.findNeighbours().stream()
                                        .filter(c -> previous == null || !c.equals(previous.position))
                                        .filter(graph::coordinateWithinRange)
                                        .filter(c -> !pathTailXinARow(3, c))
                                        .filter(c -> valueGrid.getValue(c) == Integer.MAX_VALUE) //default
                                        .toList();

        //TODO mark as dead?
        if (possibleNestSteps.isEmpty())
        {
            isDead = true;
        }

        updatNextSteps(graph, possibleNestSteps, origins);
    }

    private boolean pathTailXinARow(int x, Coordinate option)
    {
        if (x > stepnumber)
        {
            return false;
        }
        Direction direction = Direction.findDirection(position, option);
        var previousTest = position;
        CrucibleRoadNode beforeNode = this;
        for (int i = 0; i < x; i++)
        {
            beforeNode = beforeNode.previous;
            if (Direction.findDirection(beforeNode.position, previousTest) != direction)
            {
                return false;
            }
            previousTest = beforeNode.position;
        }
        return true;
    }


    public List<CrucibleRoadNode> move(int value, HashMap<Coordinate, List<Coordinate>> origins)
    {
        if (!nextSteps.containsValue(value))
        {
            //like this?
            return List.of();
        }

        var nextCoordinates = nextSteps.entrySet()
                                       .stream()
                                       .filter(e -> e.getValue() == value)
                                       .map(Map.Entry::getKey)
                                       .toList();

        List<CrucibleRoadNode> newRoads = new ArrayList<>();

        for (Coordinate next : nextCoordinates)
        {
            origins.computeIfAbsent(next, (c) -> new ArrayList<>());
            origins.get(next).add(position);
            nextSteps.remove(next);
            newRoads.add(new CrucibleRoadNode(this, next, value));
        }

        return newRoads;
    }

    public Coordinate getLast()
    {
        return position;
    }

    private boolean unknownValueWithinRange(int range, Grid<Integer> values)
    {
        for (int i = -1 * range; i < range; i++)
        {
            for (int j = -1 * range; j < range; j++)
            {
                Coordinate test = new Coordinate(position.x() + i, position.y() + j);
                if (values.coordinateWithinRange(test) && values.getValue(test) == Integer.MAX_VALUE)
                {
                    return true;
                }
            }
        }
        return false;

    }


    public boolean isDead(int threshold)
    {
        return nextSteps.isEmpty() || maxValue < threshold;
    }

    public boolean headsEqual(CrucibleRoadNode other, int depth)
    {
        CrucibleRoadNode these = this;
        Coordinate ours = these.position;
        Coordinate theirs = other.position;
        if (!ours.equals(theirs))
        {
            return false;
        }

        for (int i = 0; (i < depth && i <= stepnumber); i++)
        {
            these = these.previous;
            other = other.previous;
            if (!these.position.equals(other.position))
            {
                return false;
            }

        }
        return true;
    }


}
