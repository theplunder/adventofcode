package nl.mrtijmen.aoc2023.crucible;

import nl.mrtijmen.aoccommons.util.grid.Coordinate;
import nl.mrtijmen.aoccommons.util.grid.Direction;
import nl.mrtijmen.aoccommons.util.grid.Grid;
import nl.mrtijmen.aoccommons.util.object.Pair;

import java.util.*;
import java.util.stream.Collectors;

public class UltraCrucibleDijkstra {

    private final Grid<Integer> graph;
    private final Grid<Integer> valueGrid;

    private Set<NextEntry> unvisitedValues2;

    Set<NextEntry> visited = new HashSet<>();

    private record NextEntry(Coordinate coordinate, LinePos orientation, int value) {
        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            NextEntry nextEntry = (NextEntry) o;
            return Objects.equals(coordinate, nextEntry.coordinate) && orientation == nextEntry.orientation;
        }

        @Override
        public int hashCode() {
            return Objects.hash(coordinate, orientation);
        }
    }

    enum LinePos {
        HORIZ, VERT;

        private static LinePos fromDirection(Direction dir) {
            return switch (dir) {
                case UP, DOWN -> VERT;
                case LEFT, RIGHT -> HORIZ;
            };
        }

        public LinePos flip() {
            return this == HORIZ ? VERT : HORIZ;
        }
    }

    public UltraCrucibleDijkstra(Grid<Integer> graph) {
        this.graph = graph;
        valueGrid = new Grid<>(graph.xSize(), graph.ySize(), Integer.MAX_VALUE);
        graph.setValue(new Coordinate(0, 0), 0);
    }

    public Grid<Integer> calculateValueGrid(Coordinate targetNode) {
        unvisitedValues2 = new HashSet<>();
        Coordinate currentNode = new Coordinate(0, 0);
        unvisitedValues2.add(new NextEntry(currentNode, LinePos.HORIZ, 0));
        unvisitedValues2.add(new NextEntry(currentNode, LinePos.VERT, 0));

        valueGrid.setValue(currentNode, 0);
        int i = 0;
        while (true) {
            System.out.println(i++);
            NextEntry entry = findCurrentNode();

            NextEntry entryF = entry;
            entry.coordinate().findNeighbours().stream()
                    .filter(graph::coordinateWithinRange)
                    .filter(n -> LinePos.fromDirection(Direction.findDirection(entryF.coordinate, n)) == entryF.orientation)
                    .forEach(neighbour -> processNeighbours(entryF, neighbour));


            unvisitedValues2.remove(entry);
            visited.add(entryF);

            if (unvisitedValues2.isEmpty() || entryF.coordinate.equals(targetNode)) {
                break;
            }

            //entry = findCurrentNode();
            //update current node


         //   System.out.println(valueGrid);
            //          System.out.println(unvisitedValues2);
           // System.out.println("------------");
        }

        return valueGrid;
    }


    private void processNeighbours(NextEntry entry, Coordinate neighbour) {
        //jump, then calculate 10 in a row. mark them all unvisted, mark jumping node as done
        var currentNode = entry.coordinate();


        Direction direction = Direction.findDirection(currentNode, neighbour);


        int startValue = entry.value();

        int jumpvalue = graph.getValue(neighbour);
        for (int i = 0; i < 3; i++) {
            neighbour = direction.travel(neighbour);
            if (!graph.coordinateWithinRange(neighbour)) {
                return;
            }
            jumpvalue += graph.getValue(neighbour);
        }

        int value = startValue + jumpvalue;


        LinePos orientation = LinePos.fromDirection(direction).flip();

        for (int j = 3; j < 10; j++) {

            int valueViaC = value;
            valueGrid.update(neighbour, val ->
                    (valueViaC < val) ? valueViaC : val
            );

            var next = new NextEntry(neighbour, orientation, valueViaC);


            if (!visited.contains(next)) {
                unvisitedValues2 = unvisitedValues2.stream()
                        .filter(ent -> !(ent.equals(next) && ent.value > next.value)).collect(Collectors.toSet());
                unvisitedValues2.add(next);
            }

            neighbour = direction.travel(neighbour);

            if (!graph.coordinateWithinRange(neighbour)) {
                return;
            }
            value += graph.getValue(neighbour);
        }
    }

    private NextEntry findCurrentNode() {
        return unvisitedValues2
                .stream().min(Comparator.comparingInt(e -> e.value()))
                .get();
    }


}
