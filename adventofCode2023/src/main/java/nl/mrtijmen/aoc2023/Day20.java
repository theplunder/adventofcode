package nl.mrtijmen.aoc2023;

import nl.mrtijmen.aoc2023.pulse.Module;
import nl.mrtijmen.aoc2023.pulse.*;
import nl.mrtijmen.aoccommons.util.FileNameUtil;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;
import nl.mrtijmen.aoccommons.util.MathUtil;
import nl.mrtijmen.aoccommons.util.ParseUtil;

import java.util.*;
import java.util.stream.Collectors;

public class Day20
{

    public static void main(String[] args) throws Exception
    {
        //List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName(1));
        // List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName(2));
        //       List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName(3));
        //List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName(4));


        List<String> data = FileReaderUtil.getStringList(FileNameUtil.dataFileName());


        //read data
        //examples:
        //broadcaster -> a, b, c
        //%a -> b

        Map<String, Module> modules = data.stream().map(Day20::parse).collect(Collectors.toMap(Module::name, (m) -> m));

        ConjuctionSpy ksSpy = new ConjuctionSpy(modules.get("ks"));
        modules.put(ksSpy.name(), ksSpy);

        ConjuctionSpy zhSpy = new ConjuctionSpy(modules.get("zh"));
        modules.put(zhSpy.name(), zhSpy);

        ConjuctionSpy jtSpy = new ConjuctionSpy(modules.get("jt"));
        modules.put(jtSpy.name(), jtSpy);

        ConjuctionSpy kbSpy = new ConjuctionSpy(modules.get("kb"));
        modules.put(kbSpy.name(), kbSpy);


        //link conjunctions
        linkConjunctions(modules.values());


        var eventQueue = new PulseEventQueue(modules.values());
        eventQueue.register(new Output("output"));

//        for (int i = 0; i < 1000; i++) {
//            eventQueue.pushButton();
//        }
//        long highCount = eventQueue.getHighCounter();
//        long lowCount = eventQueue.getLowCounter();
//        System.out.println("low: " + lowCount + ", high: " + highCount);
//        long result1 = lowCount * highCount;
//        System.out.println("Result Part 1 --> " + result1);

//
//

        Output rxModule = new Output("rx");
        eventQueue.register(rxModule);

        listConnections(modules.values());


        long i = 0;
        while (!rxModule.isHasReceivedPulse() && !(ksSpy.isDone() && zhSpy.isDone() && jtSpy.isDone() && kbSpy.isDone()))
        {
            i++;
            if (i % 10 == 0) System.out.println(i);
            eventQueue.pushButton();
        }


        ArrayList<Long> multiples = new ArrayList<>();
        multiples.addAll(ksSpy.interval.values());
        multiples.addAll(zhSpy.interval.values());
        multiples.addAll(jtSpy.interval.values());

        multiples.addAll(kbSpy.interval.values());

        long[] longs = new long[multiples.size()];

        for (int j = 0; j< multiples.size(); j++){
            longs[j] = multiples.get(j);
        }


        long result2 = MathUtil.leastCommonMultiple(longs);
        //    walker(eventQueue.getRouter());


        System.out.println("Result Part 2 --> " + result2);
    }


    public static void listConnections(Collection<Module> modules)
    {

        modules.stream()
               .forEach(c -> {
                   var connected = modules.stream()
                                          .filter(m -> m.getTargets().contains(c.name()))
                                          .map(Module::name)
                                          .toList();
                   System.out.println(c.name() + " receives from " + connected);
               });
    }

    private static class Notes
    {
        int hPer = 0;
        int hOffset = 0;
        int lPer = 1;
        int lOffset = 0;

        long period = 1;

        int pulsesPerPush = 1;

        Map<String, Map<String, Long>> frequencies = new HashMap<>();

    }


    public static void walker(Map<String, Module> routes)
    {
        Module module = routes.get(Broadcaster.NAME);
        Notes notes = new Notes();
        visit(routes, module, notes, null);
        System.out.println(visited.size());
        System.out.println(notes.frequencies);
    }

    private static void visit(Map<String, Module> routes, Module module, Notes notes, String previous)
    {
        List<String> targets = module.getTargets();
        for (String target : targets)
        {
            //probably only stop at conjunctions?
            if (visited.contains(target) || target.equals("destroy"))
            {
                continue;
            }

            Module targetModule = routes.get(target);
            switch (targetModule.type())
            {
                case FLIPFLOP -> notes.period *= 2;
                case CONJUNTION ->
                {
                    visited.add(target);

                    Conjuction con = (Conjuction) targetModule;
                    notes.frequencies.computeIfAbsent(con.name(), k -> new HashMap<>());
                    notes.frequencies.get(con.name()).put(previous, notes.period);
                    notes.period = 1;
                    System.out.println(notes.frequencies);
                    System.out.println("asdfasdf");
                }
            }

            System.out.println("visit " + target + " of type " + targetModule.type());
            visit(routes, targetModule, notes, module.name());
        }

    }


    private static Set<String> visited = new HashSet<>();


    public static void walkExperimental(Map<String, Module> routes)
    {

        Module module = routes.get(Broadcaster.NAME);
        Notes notes = new Notes();
        int i = 0;
        while (i++ < 100000)
        {

            int nInputs = 1;

            if ( /* number of intpus */ 1 == 1)
            {

                notes.pulsesPerPush = notes.pulsesPerPush * nInputs; //not true, flipflop in series sends only one pulse per 2 presses!

            }


            if (module.getClass().equals(FlipFlip.class))
            {
                notes.hPer = notes.lPer * 2;
                notes.lPer = notes.lPer * 2;
                notes.hOffset = 2;
            }

            if (module.getClass().equals(Conjuction.class))
            {

                ///no idea what it does with conjections
            }


        }


    }


    private static void linkConjunctions(Collection<Module> modules)
    {
        modules.stream()
               .filter(Conjuction.class::isInstance)
               .map(Conjuction.class::cast)
               .forEach(c -> modules.stream()
                                    .filter(m -> m.getTargets().contains(c.name()))
                                    .map(Module::name)
                                    .forEach(c::registerConnected));
    }

    private static Module parse(String line)
    {
        String[] split = line.split("->");
        List<String> targets = ParseUtil.lineToStringList(split[1], ",");

        if (split[0].strip().equals(Broadcaster.NAME))
        {
            return new Broadcaster(targets);
        }

        String name = split[0].substring(1).strip();
        return switch (split[0].charAt(0))
        {
            case '%' -> new FlipFlip(name, targets);
            case '&' -> new Conjuction(name, targets);
            default -> throw new IllegalStateException("Unexpected value: " + split[0]);
        };
    }


}
