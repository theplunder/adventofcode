package nl.mrtijmen.aoc2023;

import nl.mrtijmen.aoc2023.workflow.*;
import nl.mrtijmen.aoccommons.util.ArrayUtil;
import nl.mrtijmen.aoccommons.util.FileNameUtil;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;
import nl.mrtijmen.aoccommons.util.ParseUtil;

import java.util.*;
import java.util.function.Predicate;

import static nl.mrtijmen.aoc2023.workflow.GearProperty.*;
import static nl.mrtijmen.aoc2023.workflow.InspectablePredicate.*;

public class Day19 {

    /*ENUm:
    x: Extremely cool looking
m: Musical (it makes a noise when you hit it)
a: Aerodynamic
s: Shiny
     */

    //NAME{ INSTRUCTIONS }
    //INSTRICTIONS --> Comma separated
    //

    //Make 4 Instruction Classes =)


    public static void main(String[] args) throws Exception {
     //   List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName());
        List<String> data = FileReaderUtil.getStringList(FileNameUtil.dataFileName());

        var batchedData = makeInputBatches(data);
        var ruleData = batchedData.get(0);
        var gearData = batchedData.get(1);


        var rules = ruleData.stream().map(Day19::parse).toList();
        var gears = gearData.stream().map(Day19::parseGear).toList();

        WorkflowRouter router = new WorkflowRouter(rules);

        gears.forEach(router::process);

        long result1 = router.getAcceptBucket().stream().mapToLong(Part::score).sum();

        System.out.println("Result Part 1 --> " + result1);


//        var targets = rules.stream().flatMap(r -> r.rules().stream()).map(r -> r.target()).toList();
//
//        Map<String, Integer> count = new HashMap<>();
//        for (String target : targets) {
//            count.compute(target, (k, v) -> v == null ? 1 : v + 1);
//        }
//        System.out.println(count);
//
//        count.entrySet().stream().sorted(Comparator.comparingInt(Map.Entry::getValue)).forEach(System.out::println);

        RequirementCollector collector = new RequirementCollector(rules);
        collector.startInspect();
        System.out.println("Inspected");
        collector.getHeads().forEach(System.out::println);
        long result2 = collector.getHeads().stream()
                .map(RequirementAnalysor::requirementAnalysor)
                .peek(System.out::println)
                .mapToLong(RequirementAnalysor::scoreCount).sum();

        System.out.println("Result Part 2 --> " + result2);
    }


    private static Part parseGear(String in) {
        //{x=787,m=2655,a=1222,s=2876}
        in = in.substring(1, in.length() - 1);
        EnumMap<GearProperty, Integer> properyMap = new EnumMap<>(GearProperty.class);

        var values = ParseUtil.lineToStringList(in, ",");
        for (String value : values) {
            String[] split = value.split("=");
            properyMap.put(GearProperty.valueOf(split[0].toUpperCase()), Integer.parseInt(split[1]));
        }
        return new Part(properyMap.get(X), properyMap.get(M), properyMap.get(A), properyMap.get(S));
    }

    private static RuleSet parse(String input) {
        System.out.println(input);

        int endOfName = input.indexOf('{');

        String name = input.substring(0, endOfName);

        List<String> instructions = ParseUtil.lineToStringList(input.substring(endOfName + 1, input.length() - 1), ",");

        List<WorkflowRule> rules = instructions.stream().map(Day19::parseRule).toList();
        return new RuleSet(name, rules);
    }

    private static WorkflowRule parseRule(String in) {

        if (!in.contains(":")) {
            return new WorkflowRule(acceptAll(), in);
        }

        //"s>2770:qs"
        int seperator = in.indexOf(':');
        var property = GearProperty.valueOf(in.substring(0, 1).toUpperCase());
        var operator = in.charAt(1);
        int value = Integer.parseInt(in.substring(2, seperator));
        String target = in.substring(seperator + 1);

        var predicate = operator == '>' ? greaterThan(property, value) : lesserThan(property, value);

        return new WorkflowRule(predicate, target);
    }

//    public static Predicate<Part> greaterThan(GearProperty property, int value) {
//        return part -> (part.property(property) > value);
//    }
//
//    public static InspectablePredicate test(GearProperty property, int value) {
//        return part -> (part.property(property) > value);
//
//    }
//    public static Predicate<Part> lesserThan(GearProperty property, int value) {
//        return part -> (part.property(property) < value);
//    }

    // private static final Predicate<Part> accept_all = part -> true;


    private static List<List<String>> makeInputBatches(List<String> data) {
        List<List<String>> batches = new ArrayList<>();

        List<String> batch = new ArrayList<>();
        for (String line : data) {
            //end and/or start of new mapping
            if (line.isBlank()) {
                if (!batch.isEmpty()) {
                    batches.add(batch);
                }
                batch = new ArrayList<>();
            } else {
                batch.add(line);
            }
        }
        batches.add(batch);
        return batches;
    }


}