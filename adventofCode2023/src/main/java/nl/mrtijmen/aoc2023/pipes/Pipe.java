package nl.mrtijmen.aoc2023.pipes;

import nl.mrtijmen.aoccommons.util.grid.Coordinate;
import nl.mrtijmen.aoccommons.util.grid.Direction;

import static nl.mrtijmen.aoccommons.util.grid.Direction.*;

public record Pipe(Coordinate position, PipeType type)
{


    public Coordinate traverse(Coordinate origin)
    {
        //find relative position
        int i = position.findNeighbours().indexOf(origin);
        Direction comingFrom = indexToDirection(i);

        return switch (type)
        {
            case VERTICAL -> switch (comingFrom)
            {
                case UP -> position.up();
                case DOWN -> position.down();
                default ->
                        throw new UnsupportedOperationException("Entered from illegal direction. Coming from " + origin + " going to " + position + " relativeDriection " + comingFrom);
            };
            case HORIZONTAL -> switch (comingFrom)
            {
                case LEFT -> position.right();
                case RIGHT -> position.left();
                default ->
                        throw new UnsupportedOperationException("Entered from illegal direction. Coming from " + origin + " going to " + position + " relativeDriection " + comingFrom);
            };
            case BEND_NE -> switch (comingFrom)
            {
                case UP -> position.right();
                case RIGHT -> position.down();
                default ->
                        throw new UnsupportedOperationException("Entered from illegal direction. Coming from " + origin + " going to " + position + " relativeDriection " + comingFrom);
            };
            case BEND_NW -> switch (comingFrom)
            {
                case UP -> position.left();
                case LEFT -> position.down();
                default ->
                        throw new UnsupportedOperationException("Entered from illegal direction. Coming from " + origin + " going to " + position + " relativeDriection " + comingFrom);
            };
            case BEND_SW -> switch (comingFrom)
            {
                case DOWN -> position.left();
                case LEFT -> position.up();
                default ->
                        throw new UnsupportedOperationException("Entered from illegal direction. Coming from " + origin + " going to " + position + " relativeDriection " + comingFrom);
            };
            case BEND_SE -> switch (comingFrom)
            {
                case DOWN -> position.right();
                case RIGHT -> position.up();
                default ->
                        throw new UnsupportedOperationException("Entered from illegal direction. Coming from " + origin + " going to " + position + " relativeDriection " + comingFrom);
            };
            case NO_PIPE, START ->
                    throw new UnsupportedOperationException("pipetype " + type + " does not support traversal. Coming from " + origin + " going to " + position + " relativeDriection " + comingFrom);
        };
    }

    //TODO probably move this to Coordinate class
    private Direction indexToDirection(int i)
    {
        return switch (i)
        {
            case 0 -> DOWN;
            case 1 -> RIGHT;
            case 2 -> UP;
            case 3 -> LEFT;
            default -> throw new IllegalArgumentException("index out of range " + i);
        };
    }


}
