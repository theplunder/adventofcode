package nl.mrtijmen.aoc2023.pipes;

import java.util.Arrays;

public enum PipeType
{

//    | is a vertical pipe connecting north and south.
//    - is a horizontal pipe connecting east and west.
//    L is a 90-degree bend connecting north and east.
//    J is a 90-degree bend connecting north and west.
//    7 is a 90-degree bend connecting south and west.
//    F is a 90-degree bend connecting south and east.
//    . is ground; there is no pipe in this tile.
//    S is the starting position of the animal; there is a pipe on this tile, but your sketch doesn't show what shape the pipe has. <<-----

    VERTICAL("|"),
    HORIZONTAL("-"),
    BEND_NE("L"),
    BEND_NW("J"),
    BEND_SW("7"),
    BEND_SE("F"),
    NO_PIPE("."),
    START("S");

    private String code;

    PipeType(String code)
    {
        this.code = code;
    }

    public static PipeType fromCode(String code)
    {
        return Arrays.stream(PipeType.values()).filter(p -> p.code().equals(code)).findAny().orElseThrow();
    }

    public String code()
    {
        return code;
    }

    
}
