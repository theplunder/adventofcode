package nl.mrtijmen.aoc2023;

import nl.mrtijmen.aoccommons.util.ArrayUtil;
import nl.mrtijmen.aoccommons.util.FileNameUtil;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;
import nl.mrtijmen.aoccommons.util.ParseUtil;
import nl.mrtijmen.aoccommons.util.grid.Coordinate;
import nl.mrtijmen.aoccommons.util.grid.Grid;

import java.util.*;

public class Day03
{

    public static void main(String[] args) throws Exception
    {
        //  List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName());
        List<String> data = FileReaderUtil.getStringList(FileNameUtil.dataFileName());

        List<List<String>> matrix = ParseUtil.parseStringMatrix(data, "");
        var charMatrix = ArrayUtil.convert(matrix, s -> s.charAt(0));
        Grid<Character> grid = new Grid<>(charMatrix);

        System.out.println(grid);

        List<Coordinate> numbers = new ArrayList<>();
        List<Coordinate> symbols = new ArrayList<>();
        var gearCandidates = new HashMap<Coordinate, Set<NumberSet>>();

        grid.visitAllCoordinates(((coordinate, val) -> {
            if (val.equals('.'))
            {
                return;
            }
            if (Character.isDigit(val))
            {
                numbers.add(coordinate);
                return;
            }
            symbols.add(coordinate);

        }));

        Set<NumberSet> numberSets = findNeighbouringNumbers(grid, symbols, gearCandidates);

        System.out.println(numberSets);
        int result = numberSets.stream().mapToInt(NumberSet::value).sum();
        System.out.println("Result part 1 --> " + result);

        long result2 = gearCandidates.values().stream()
                                     .filter(sets -> sets.size() == 2)
                                     .map(sets -> sets.stream().map(NumberSet::value).toList())
                                     .mapToLong(l -> (long) l.get(0) * l.get(1))
                                     .sum();
        System.out.println("Result part 2 --> " + result2);
    }


    //really ugly this mutable gearCanditate Map;
    private static Set<NumberSet> findNeighbouringNumbers(Grid<Character> grid, List<Coordinate> symbols, Map<Coordinate, Set<NumberSet>> gearCanditates)
    {
        Set<NumberSet> numbers = new HashSet<>();
        for (Coordinate coordinate : symbols)
        {
            //    System.out.println("neightbour of " + coordinate + "are " + coordinate.findNeighbours(true));
            coordinate.findNeighbours(true).forEach(neighbour ->
                    {
                        if (!grid.coordinateWithinRange(neighbour))
                        {
                            return;
                        }

                        Character val = grid.getValue(neighbour);
                        if (Character.isDigit(val))
                        {
                            NumberSet number = findNumbersInLine(neighbour, grid);
                            // we might find the same number multiple times, the Set takes care of doubles.
                            numbers.add(number);

                            //this is a gear!
                            if ('*' == grid.getValue(coordinate))
                            {
                                gearCanditates.computeIfAbsent(coordinate, (c) -> new HashSet<>());
                                gearCanditates.get(coordinate).add(number);
                            }
                        }
                    }
            );
        }
        return numbers;
    }

    private static NumberSet findNumbersInLine(Coordinate coordinate, Grid<Character> grid)
    {
        //    System.out.println("checking: " + coordinate);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(grid.getValue(coordinate));

        //get all on left
        Coordinate probeLeft = coordinate.left();
        while ((grid.coordinateWithinRange(probeLeft))
                && Character.isDigit(grid.getValue(probeLeft)))
        {
            stringBuilder.insert(0, grid.getValue(probeLeft));
            probeLeft = probeLeft.left();
        }

        Coordinate startPoint = probeLeft.right();

        //get all on right
        Coordinate probeRight = coordinate.right();
        while ((grid.coordinateWithinRange(probeRight))
                && Character.isDigit(grid.getValue(probeRight)))
        {
            stringBuilder.append(grid.getValue(probeRight));
            probeRight = probeRight.right();
        }

        var result = new NumberSet(startPoint, Integer.parseInt(stringBuilder.toString()));
        //     System.out.println("found: " + result);
        return result;

    }

    private record NumberSet(Coordinate startingPoint, int value)
    {
    }

}