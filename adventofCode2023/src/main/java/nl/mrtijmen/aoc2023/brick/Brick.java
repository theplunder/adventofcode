package nl.mrtijmen.aoc2023.brick;

import nl.mrtijmen.aoccommons.util.grid.Coordinate3D;

public record Brick(Coordinate3D start, Coordinate3D end, int brickId) {


}
