package nl.mrtijmen.aoc2023.brick;

import nl.mrtijmen.aoccommons.util.grid.Coordinate;
import nl.mrtijmen.aoccommons.util.grid.Coordinate3D;
import nl.mrtijmen.aoccommons.util.grid.Grid;

import java.util.*;

public class BrickPile
{


    private final List<Brick> pile = new ArrayList<>();

    private final int xMaxGrid;
    private final int yMaxGrid;

    private Set<Coordinate3D> occupiedPositions = new HashSet<>();

    private Grid<Integer> heightMap;

    private Map<Coordinate3D, Integer> coordinateToBrickId = new HashMap<>();
    private Map<Integer, List<Coordinate3D>> brickIdToCoordinates = new HashMap<>();


    public BrickPile(int xMax, int yMax)
    {
        this.xMaxGrid = xMax;
        this.yMaxGrid = yMax;
        heightMap = new Grid<>(xMax, yMax, 0);
    }


    public void addBrick(Brick brick)
    {
        //find list of coordinates, maybe move this to the Brick class...
        List<Coordinate3D> fullBrick = new ArrayList<>();
        for (long x = brick.start().x(); x <= brick.end().x(); x++)
        {
            for (long y = brick.start().y(); y <= brick.end().y(); y++)
            {
                for (long z = brick.start().z(); z <= brick.end().z(); z++)
                {
                    fullBrick.add(new Coordinate3D(x, y, z));

                }
            }
        }

        //find highest z to settle

        long highestZ = 0;
        long lowestZOfBrick = 100000;
        for (Coordinate3D c : fullBrick)
        {
            //           System.out.println(fullBrick);
            highestZ = Math.max(heightMap.getValue((int) c.x(), (int) c.y()), highestZ);
            lowestZOfBrick = Math.min(c.z(), lowestZOfBrick);
        }

        //     System.out.println("Highest point in projection: " + highestZ);
        //     System.out.println("Lowest point of brick: " + lowestZOfBrick);

        long fallDistance = lowestZOfBrick - highestZ - 1;
        //    System.out.println("the brick has to fall: " + fallDistance);


        fullBrick = fullBrick.stream()
                             .map(c -> new Coordinate3D(c.x(), c.y(), c.z() - fallDistance))
                             .toList();

        for (Coordinate3D c : fullBrick)
        {
            long currentHeight = heightMap.getValue((int) c.x(), (int) c.y());
            if (c.z() < currentHeight) throw new IllegalStateException();
            heightMap.setValue(new Coordinate(c.x(), c.y()), (int) c.z());
        }

        //--------updates (other than heightmap)

        fullBrick.stream().forEach(b -> {
            if (occupiedPositions.contains(b)) throw new IllegalStateException("");
        });
        occupiedPositions.addAll(fullBrick);
        pile.add(brick);

        fullBrick.forEach(fb -> coordinateToBrickId.put(fb, brick.brickId()));

        brickIdToCoordinates.put(brick.brickId(), fullBrick);


        //     System.out.println(heightMap);

    }

    public List<Integer> listRemovableBricks()
    {
        Set<Integer> ids = brickIdToCoordinates.keySet();
        List<Integer> removables = new ArrayList<>();

        for (Integer brickId : ids)
        {
            List<Coordinate3D> coordinates = brickIdToCoordinates.get(brickId);
            Set<Integer> isSupporting = new HashSet<>();

            for (Coordinate3D brickPosition : coordinates)
            {
                var oneAbove = brickPosition.up();
                if (!coordinateToBrickId.containsKey(oneAbove) || coordinateToBrickId.get(oneAbove).equals(brickId))
                {
                    continue;
                }
                isSupporting.add(coordinateToBrickId.get(oneAbove));
            }

            boolean canBeRemoved = true;
            for (Integer supportedBrickId : isSupporting)
            {
                List<Coordinate3D> supportedCoordinates = brickIdToCoordinates.get(supportedBrickId);

                int otherSuppportCount = 0;
                for (Coordinate3D brickPosition : supportedCoordinates)
                {
                    var oneDown = brickPosition.down();
                    if (!coordinateToBrickId.containsKey(oneDown)
                            || coordinateToBrickId.get(oneDown).equals(brickId)
                            || coordinateToBrickId.get(oneDown).equals(supportedBrickId))
                    {
                        continue;
                    }
                    otherSuppportCount++;
                }
                if (otherSuppportCount == 0) canBeRemoved = false;
            }
            if (canBeRemoved)
            {
            //    System.out.println("Can remove:" + brickIdToCoordinates.get(brickId));
                removables.add(brickId);
            }
        }
        return removables;
    }


    public Map<Integer, Set<Integer>> buildSupportingMap()
    {
        Map<Integer, Set<Integer>> supportingMap = new HashMap<>();
        Set<Integer> ids = brickIdToCoordinates.keySet();

        for (Integer brickId : ids)
        {
            List<Coordinate3D> coordinates = brickIdToCoordinates.get(brickId);
            Set<Integer> isSupporting = new HashSet<>();

            for (Coordinate3D brickPosition : coordinates)
            {
                var oneAbove = brickPosition.up();
                if (!coordinateToBrickId.containsKey(oneAbove) || coordinateToBrickId.get(oneAbove).equals(brickId))
                {
                    continue;
                }
                isSupporting.add(coordinateToBrickId.get(oneAbove));
            }
            supportingMap.put(brickId, isSupporting);
        }
        return supportingMap;
    }

}
