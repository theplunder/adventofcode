package nl.mrtijmen.aoc2022.rockpaperscissor;

public record MatchUp (RockPaperScissor self, RockPaperScissor other){

    public Result resolve(){
        return self.compare(other);
    }
}
