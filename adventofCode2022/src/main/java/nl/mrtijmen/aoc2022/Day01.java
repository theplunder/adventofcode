package nl.mrtijmen.aoc2022;

import nl.mrtijmen.aoccommons.util.FileNameUtil;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;
import org.junit.platform.commons.util.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Day01 {

    public static void main(String[] args) throws Exception {
        //   List<String> calories = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName());
        List<String> calories = FileReaderUtil.getStringList(FileNameUtil.dataFileName());

        List<Integer> grouped = groupPerElf(calories);
        int max = grouped.stream().mapToInt(Integer::intValue).max().orElseThrow();
        System.out.println("result part 1: " + max);
        int maxTop3 = grouped.stream().sorted(Collections.reverseOrder()).limit(3).mapToInt(Integer::intValue).sum();
        System.out.println("result part 2: " + maxTop3);
    }

    private static List<Integer> groupPerElf(List<String> calories) {
        List<Integer> collectedCalories = new ArrayList<>();
        int currentElf = 0;
        for (String calorie : calories) {
            if (StringUtils.isBlank(calorie)) {
                collectedCalories.add(currentElf);
                currentElf = 0;
                continue;
            }
            int cal = Integer.parseInt(calorie);
            currentElf += cal;
        }
        collectedCalories.add(currentElf);
        return collectedCalories;
    }
}
