package nl.mrtijmen.aoc2021;

import nl.mrtijmen.aoc2021.bingo.BingoCard;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;
import nl.mrtijmen.aoccommons.util.ParseUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class Day04
{

    public static void main(String[] args) throws Exception
    {
        //  List<String> rawInput = FileReaderUtil.getStringList("./src/main/resources/Day04InputExample.txt");
        List<String> rawInput = FileReaderUtil.getStringList("./src/main/resources/Day04Input.txt");

        List<Integer> calledNumbers = lineToNumberList(rawInput.get(0), ",");

        List<BingoCard> cards = buildCards(rawInput);

        System.out.println(cards);


        List<BingoCard> ranking = playGame(calledNumbers, cards);
        BingoCard winner = ranking.get(0);
        System.out.println(winner);
        int winnerNumber = winner.getLastMarkedNumber();
        List<Integer> unmarked = winner.listUnmarkedNumbers();

        System.out.println("winnerNumber: " + winnerNumber);
        System.out.println(unmarked);

        int sum = unmarked.stream().reduce(Integer::sum).get();
        System.out.println("sum: " + sum);
        System.out.println("result part 1: " + (sum * winnerNumber));

        System.out.println("==== Part 2 ========");
        BingoCard looser = ranking.get(ranking.size() - 1);
        System.out.println(looser);
        int looserNumber = looser.getLastMarkedNumber();
        unmarked = looser.listUnmarkedNumbers();

        System.out.println("winnerNumber: " + looserNumber);
        System.out.println(unmarked);

        sum = unmarked.stream().reduce(Integer::sum).get();
        System.out.println("sum: " + sum);
        System.out.println("result part 2: " + (sum * looserNumber));


    }

    private static List<BingoCard> playGame(List<Integer> calledNumbers, List<BingoCard> cards)
    {
        List<BingoCard> ranking = new ArrayList<>();
        for (int calledNumber : calledNumbers)
        {
            Iterator<BingoCard> cardIterator = cards.iterator();
            while (cardIterator.hasNext())
            {
                BingoCard card = cardIterator.next();
                boolean winner = card.markNumber(calledNumber);
                if (winner)
                {
                    ranking.add(card);
                    cardIterator.remove();
                }
            }
        }
        return ranking;
    }

    private static List<BingoCard> buildCards(List<String> rawInput)
    {
        List<BingoCard> cards = new ArrayList<>();
        double cardCount = (rawInput.size() - 1) / 6.;
        for (int i = 0; i < cardCount; i++)
        {
            List<List<Integer>> card = ParseUtil.parseMatrix(rawInput.subList(i * 6 + 2, (i + 1) * 6 + 1), " ");
            cards.add(new BingoCard(card));
        }
        return cards;
    }

    private static List<Integer> lineToNumberList(String line, String seperator)
    {
        return Arrays.stream(line.split(seperator)).filter(s -> !s.isBlank()).map(Integer::parseInt).toList();
    }


}
