package nl.mrtijmen.aoc2021.movement;

import nl.mrtijmen.aoccommons.util.grid.Coordinate;

public record Instruction(Direction dir, long value)
{
    public Coordinate apply(Coordinate c)
    {
        return switch (dir)
                {
                    case UP -> new Coordinate(c.x(), c.y() + value);
                    case DOWN -> new Coordinate(c.x(), c.y() - value);
                    case FORWARD -> new Coordinate(c.x() + value, c.y());
                    case BACKWARD -> new Coordinate(c.x() - value, c.y() + value);
                };
    }

    public CoordinateWithAim apply(CoordinateWithAim c)
    {
        return switch (dir)
                {
                    case UP -> new CoordinateWithAim(c.x(), c.y(), c.aim() - value);
                    case DOWN -> new CoordinateWithAim(c.x(), c.y(), c.aim() + value);
                    case FORWARD -> new CoordinateWithAim(c.x() + value, c.y() - (value * c.aim()), c.aim());
                    case BACKWARD -> new CoordinateWithAim(c.x() - value, c.y(), c.aim());
                };
    }

}
