package nl.mrtijmen.aoc2021;

import nl.mrtijmen.aoccommons.util.FileNameUtil;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;
import nl.mrtijmen.aoccommons.util.grid.Coordinate;
import nl.mrtijmen.aoccommons.util.grid.Grid;

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

public class Day09
{
    private boolean withDiagonalConnections;

    public static void main(String[] args) throws Exception
    {
        // List<String> raw = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName());
        List<String> raw = FileReaderUtil.getStringList(FileNameUtil.dataFileName());
        List<List<Integer>> input = raw.stream()
                                       .map(l -> Arrays.stream(l.split("")).map(Integer::parseInt).toList())
                                       .toList();
        var grid = new Grid<>(input);

        AtomicLong counter = new AtomicLong(0);
        grid.visitAllCoordinates((coordinate, v) -> {
            boolean isRisk = coordinate.findNeighbours().stream()
                                       .filter(grid::coordinateWithinRange)
                                       .mapToInt(grid::getValue).allMatch(value -> value > v);
            if (isRisk)
            {
                counter.addAndGet(v + 1);
            }

        });
        System.out.println("result part 1: " + counter.get());

        List<Map<Coordinate, Integer>> basins = basinCounter(grid);
        List<Integer> basinSizes = basins.stream().map(Map::size).sorted((i1, i2) -> i2 - i1).limit(3).toList();
        long result = 1;
        for (Integer i : basinSizes)
        {
            result *= i;
        }
        System.out.println("result part 2: " + result);
    }

    public static List<Map<Coordinate, Integer>> basinCounter(Grid<Integer> grid)
    {
        //redundant?
        Set<Coordinate> visitedCoordinates = new HashSet<>();

        List<Map<Coordinate, Integer>> basins = new ArrayList<>();

        grid.visitAllCoordinates((coordinate, value) -> {
            if (visitedCoordinates.contains(coordinate))
            {
                return;
            }
            visitedCoordinates.add(coordinate);
            if (value >= 9)
            {
                return;
            }
            Map<Coordinate, Integer> basin = new HashMap<>();
            recursiveVisit(grid, visitedCoordinates, coordinate, value, basin);
            basins.add(basin);
        });
        return basins;
    }

    //I should add this recursive visitor to my Grid class
    private static void recursiveVisit(Grid<Integer> grid, Set<Coordinate> visitedCoordinates, Coordinate coordinate, Integer value, Map<Coordinate, Integer> basin)
    {
        basin.put(coordinate, value);
        visitedCoordinates.add(coordinate);
        List<Coordinate> neighbours =
                coordinate.findNeighbours().stream()
                          .filter(grid::coordinateWithinRange)
                          .filter(c -> !basin.containsKey(c))
                          .filter(c -> !visitedCoordinates.contains(c))
                          .filter(c -> grid.getValue(c) < 9)
                          .toList();
        if (!neighbours.isEmpty())
        {
            neighbours.forEach(c -> recursiveVisit(grid, visitedCoordinates, c, grid.getValue(c), basin));
        }
    }

    public boolean isWithDiagonalConnections()
    {
        return withDiagonalConnections;
    }

    public void setWithDiagonalConnections(boolean withDiagonalConnections)
    {
        this.withDiagonalConnections = withDiagonalConnections;
    }

}
