package nl.mrtijmen.aoc2021;

import nl.mrtijmen.aoccommons.util.FileNameUtil;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;
import nl.mrtijmen.aoccommons.util.ParseUtil;
import nl.mrtijmen.aoccommons.util.grid.Coordinate;
import nl.mrtijmen.aoccommons.util.grid.Grid;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Day11
{
    public static void main(String[] args) throws Exception
    {
        List<String> rawInput = FileReaderUtil.getStringList(FileNameUtil.dataFileName());
        //List<String> rawInput = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName());
        List<List<Integer>> lines = ParseUtil.parseMatrix(rawInput, "");

        Grid<Integer> grid = new Grid<>(lines);
        System.out.println(grid);


        int counter = 0;
        for (int step = 0; step < 100; step++)
        {
            System.out.println("Step: " + (step + 1));
            grid.visitAndUpdateAllCoordinates((c, i) -> i + 1); //add one to all coordinates
            counter += updateGridOneStep(grid);
            System.out.println(grid);
        }


        System.out.println(counter);

        boolean done = false;
        int step = 100;
        for (; !done || step > 100000; step++)
        {
            System.out.println("Step: " + step + 1);
            grid.visitAndUpdateAllCoordinates((c, i) -> i + 1); //add one to all coordinates
            int nrOfFlashes = updateGridOneStep(grid);
            done = (nrOfFlashes == grid.size());

            System.out.println(grid);
        }

        System.out.println("simul flashes at: " + step);


    }

    private static int updateGridOneStep(Grid<Integer> grid)
    {
        Set<Coordinate> flashed = new HashSet<>();
        int before = 0;
        int after = -1;
        while (before != after)
        {
            before = flashed.size();
            grid.visitAllCoordinates((c, i) -> {
                if (i <= 9 || flashed.contains(c))
                {
                    return;
                }
                flashed.add(c);
                c.findNeighbours(true)
                 .stream()
                 .filter(grid::coordinateWithinRange)
                 .forEach(n -> grid.update(n, v -> v + 1));
            });

            after = flashed.size();
        }
        grid.visitAndUpdateAllCoordinates((c, i) -> i > 9 ? 0 : i);

        return after;
    }
}
