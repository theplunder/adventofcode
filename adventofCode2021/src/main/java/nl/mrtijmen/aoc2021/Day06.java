package nl.mrtijmen.aoc2021;

import nl.mrtijmen.aoccommons.util.FileReaderUtil;
import nl.mrtijmen.aoccommons.util.ParseUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Day06
{

    public static void main(String[] args) throws Exception
    {
       // String raw = FileReaderUtil.getStringList("./src/main/resources/Day06InputExample.txt").get(0);
        String raw = FileReaderUtil.getStringList("./src/main/resources/Day06Input.txt").get(0);

        List<Integer> input = ParseUtil.lineToNumberList(raw, ",");

        //day, nr of lampfish that will reproduce that day
        Map<Integer, Long> reproductionMap = new HashMap<>();
        System.out.println(input);
        input.forEach(i -> reproductionMap.compute(i+1, (k, v) -> v == null ? 1 : v+1));

        long fishcount = countFishfAfterDays(input, reproductionMap, 80);
        System.out.println("result part 1: " + fishcount);

        long fishcount2 = countFishfAfterDays(input, reproductionMap, 256);
        System.out.println("result part 2: " + fishcount2);
    }

    private static long countFishfAfterDays(List<Integer> input, Map<Integer, Long> reproductionMap, int days)
    {
        reproductionMap = new HashMap<>(reproductionMap);

        long fishcount = input.size();


        for (int day = 0; day <= days; day++)
        {
        //    System.out.println(reproductionMap);
            Long newfish = reproductionMap.get(day);
            if (newfish == null)
            {
                System.out.println("day: " + day + " fishcount: " + fishcount);
                continue;
            }
            reproductionMap.compute(day + 7, (k, v) -> v == null ? newfish : v + newfish);
            reproductionMap.compute(day + 9, (k, v) -> v == null ? newfish : v + newfish);
            fishcount += newfish;
            System.out.println("day: " + day + " fishcount: " + fishcount);
        }
        return fishcount;
    }

}
