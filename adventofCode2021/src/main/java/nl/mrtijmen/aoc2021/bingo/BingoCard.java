package nl.mrtijmen.aoc2021.bingo;

import java.util.ArrayList;
import java.util.List;

public class BingoCard
{
    private final List<List<Integer>> card;
    private final List<Integer> collumnMatches = new ArrayList<>(List.of(0, 0, 0, 0, 0));
    private final List<Integer> rowMaches = new ArrayList<>(List.of(0, 0, 0, 0, 0));
    private final List<Integer> markedNumbers = new ArrayList<>();

    public BingoCard(List<List<Integer>> card)
    {
        this.card = card.stream().map(ArrayList::new).map(al -> (List<Integer>) al).toList();
    }

    public boolean markNumber(int number)
    {
        for (int i = 0; i < card.size(); i++)
        {
            if (card.get(i).contains(number))
            {
                int j = card.get(i).indexOf(number);
                collumnMatches.set(j, collumnMatches.get(j) + 1);
                rowMaches.set(i, rowMaches.get(i) + 1);
                markedNumbers.add(number);
                if (collumnMatches.get(j) == 5 || rowMaches.get(i) == 5)
                {
                    return true;
                }
            }
        }
        return false;
    }

    public List<Integer> listUnmarkedNumbers()
    {
        return card.stream()
                   .flatMap(List::stream)
                   .filter(i -> !markedNumbers.contains(i))
                   .toList();
    }

    public int getLastMarkedNumber()
    {
        if (markedNumbers.size() == 0)
        {
            return -1;
        }
        return markedNumbers.get(markedNumbers.size() - 1);
    }

    @Override
    public String toString()
    {
        return "BingoCard{" +
                "card=" + card +
                '}';
    }
}
