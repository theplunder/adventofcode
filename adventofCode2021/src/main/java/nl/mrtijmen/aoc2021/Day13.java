package nl.mrtijmen.aoc2021;

import nl.mrtijmen.aoc2021.origami.FoldInstruction;
import nl.mrtijmen.aoc2021.origami.Mark;
import nl.mrtijmen.aoccommons.util.FileNameUtil;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;
import nl.mrtijmen.aoccommons.util.ParseUtil;
import nl.mrtijmen.aoccommons.util.grid.Coordinate;
import nl.mrtijmen.aoccommons.util.grid.Grid;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Day13
{
    public static void main(String[] args) throws Exception
    {
        List<String> raw = FileReaderUtil.getStringList(FileNameUtil.dataFileName());
        //List<String> raw = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName());

        Set<Coordinate> markedPoints = new HashSet<>();
        List<FoldInstruction> instructions = new ArrayList<>();

        for (String line : raw)
        {
            line = line.strip();
            if (line.isBlank())
            {
                continue;
            }
            if (line.startsWith("fold along"))
            {
                FoldInstruction.Orientation orientation = line.contains("x") ? FoldInstruction.Orientation.HORIZONTAL : FoldInstruction.Orientation.VERTICAL;
                int foldLine = Integer.parseInt(line.split("=")[1]);
                instructions.add(new FoldInstruction(foldLine, orientation));
                continue;
            }
            markedPoints.add(ParseUtil.toCoordinate(line, ","));
        }

        System.out.println(markedPoints);
        System.out.println(instructions);

        Set<Coordinate> afterFirstInstruction = markedPoints.stream()
                                                            .map(c -> fold(c, instructions.get(0)))
                                                            .collect(Collectors.toSet());

        System.out.println("result: " + afterFirstInstruction.size());

        Set<Coordinate> foldingPaper = markedPoints;
        for (FoldInstruction instruction : instructions)
        {
            System.out.println(instruction);
            foldingPaper = foldingPaper.stream().map(c -> fold(c, instruction)).collect(Collectors.toSet());
        }

        long maxX = foldingPaper.stream().mapToLong(Coordinate::x).max().getAsLong();
        long maxY = foldingPaper.stream().mapToLong(Coordinate::y).max().getAsLong();

        Grid<Mark> grid = new Grid<>((int) maxX+1, (int) maxY+1, Mark.UMARKED);
        foldingPaper.forEach(c -> grid.update(c, v -> Mark.MARKED));
        System.out.println(foldingPaper);
        System.out.println(grid);

    }

    private static Coordinate fold(Coordinate origin, FoldInstruction foldInstruction)
    {
        return switch (foldInstruction.orientation())
                {
                    case VERTICAL -> {
                        if (foldInstruction.line() < origin.y())
                        {
                            yield new Coordinate(origin.x(), (2L * foldInstruction.line()) - origin.y());
                        } else
                        {
                            yield origin;
                        }
                    }
                    case HORIZONTAL -> {
                        if (foldInstruction.line() < origin.x())
                        {
                            yield new Coordinate((2L * foldInstruction.line()) - origin.x(), origin.y());
                        } else
                        {
                            yield origin;
                        }
                    }
                };
    }
}
