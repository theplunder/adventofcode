package nl.mrtijmen.aoc2021;

import nl.mrtijmen.aoc2021.display.SegmentDisplay;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;

import java.util.*;

public class Day08
{

    public static void main(String[] args) throws Exception
    {
        //List<String> raw = FileReaderUtil.getStringList("./src/main/resources/Day08InputExample.txt");
        List<String> raw = FileReaderUtil.getStringList("./src/main/resources/Day08Input.txt");

        List<List<String>> outputs = new ArrayList<>();
        List<List<String>> signals = new ArrayList<>();

        raw.stream().map(s -> s.split("\\|")).forEach(
                (String[] s) -> {
                    List<String> signal = Arrays.stream(s[0].strip().split(" ")).map(String::strip).toList();
                    signals.add(signal);
                    List<String> output = Arrays.stream(s[1].strip().split(" ")).map(String::strip).toList();
                    outputs.add(output);
                }
        );
        System.out.println(outputs);

        long result = outputs.stream()
                             .flatMap(Collection::stream)
                             .mapToInt(String::length)
                             .filter(l -> l == 2 || l == 4 || l == 3 || l == 7)
                             .count();
        System.out.println("result part 1: " + result);

        long displayNrSum = 0;
        for (int i = 0; i < outputs.size(); i++)
        {
            Map<SegmentDisplay, String> decoded = decode(signals.get(i));
            List<String> output = outputs.get(i);
            int displayNr = 0;
            for (int j = 0; j < output.size(); j++)
            {
                final int x = j;
                int out = decoded.entrySet()
                                 .stream()
                                 .filter(e -> equalsIgnoreOrder(e.getValue(), output.get(x)))
                                 .map(Map.Entry::getKey)
                                 .map(SegmentDisplay::getValue)
                                 .findFirst()
                                 .orElseThrow(() -> new RuntimeException("fout!"));
                displayNr += (out * Math.pow(10, output.size() - j - 1));
            }
            System.out.println(displayNr);
            displayNrSum += displayNr;
        }

        System.out.println("result part 2: " + displayNrSum);

    }

    private static Map<SegmentDisplay, String> decode(List<String> signalsIn)
    {
        List<String> signals = new ArrayList<>(signalsIn);
        Map<SegmentDisplay, String> results = new EnumMap<>(SegmentDisplay.class);
        for (String signal : signals)
        {
            List<SegmentDisplay> segmentDisplays = SegmentDisplay.getSegmentsWithCount(signal.length());
            if (segmentDisplays.size() == 1)
            {
                results.put(segmentDisplays.get(0), signal);
            }
        }
        results.values().forEach(signals::remove);
        //find 3
        String candidate3 = findCandidateFor(signals, results, SegmentDisplay.THREE, SegmentDisplay.ONE);
        results.put(SegmentDisplay.THREE, candidate3);
        signals.remove(candidate3);

        String candidate9 = findCandidateFor(signals, results, SegmentDisplay.NINE, SegmentDisplay.THREE);
        results.put(SegmentDisplay.NINE, candidate9);
        signals.remove(candidate9);

        String candidate0 = findCandidateFor(signals, results, SegmentDisplay.ZERO, SegmentDisplay.SEVEN);
        results.put(SegmentDisplay.ZERO, candidate0);
        signals.remove(candidate0);

        //now 6 is free

        String candidate6 = signals.stream()
                                   .filter(s -> s.length() == SegmentDisplay.SIX.segmentCout())
                                   .findAny()
                                   .get();

        results.put(SegmentDisplay.SIX, candidate6);
        signals.remove(candidate6);

        //now use 6 to get 5
        String candidate5 = signals.stream()
                                   .filter(s -> s.length() == SegmentDisplay.FIVE.segmentCout())
                                   .filter(s -> containsAllCharsOf(results.get(SegmentDisplay.SIX), s))
                                   .findAny().orElseThrow(() -> new RuntimeException("wrong data!"));
        results.put(SegmentDisplay.FIVE, candidate5);
        signals.remove(candidate5);

        //one 2 left =)
        results.put(SegmentDisplay.TWO, signals.get(0));
        System.out.println(results);
        return results;
    }

    private static String findCandidateFor(List<String> signals, Map<SegmentDisplay, String> results, SegmentDisplay target, SegmentDisplay similarSized)
    {
        return signals.stream()
                      .filter(s -> s.length() == target.segmentCout())
                      .filter(s -> containsAllCharsOf(s, results.get(similarSized)))
                      .findAny()
                      .orElseThrow(() -> new RuntimeException("wrong data!"));
    }

    public static boolean containsAllCharsOf(String s1, String s2)
    {
        for (char c : s2.toCharArray())
        {
            if (!s1.contains(Character.toString(c)))
            {
                return false;
            }
        }
        return true;
    }

    public static boolean equalsIgnoreOrder(String s1, String s2)
    {
        if (s1.length() != s2.length())
        {
            return false;
        }
        if (s1.equals(s2))
        {
            return true;
        }
        for (char c : s2.toCharArray())
        {
            if (!s1.contains(Character.toString(c)))
            {
                return false;
            }
        }
        return true;
    }

}
