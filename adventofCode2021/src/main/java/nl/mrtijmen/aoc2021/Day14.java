package nl.mrtijmen.aoc2021;

import nl.mrtijmen.aoccommons.util.FileNameUtil;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Day14
{

    private static Map<String, String> convertions = new HashMap<>();
    private static int maxDepth = 40;
    private static Map<String, Long> countersPart1 = new HashMap<>();
    private static Map<String, Long> countersPart2 = new HashMap<>();
    private static Map<ResultKey, Map<String, Long>> resultMap = new HashMap<>();

    public static void main(String[] args) throws Exception
    {
        List<String> raw = FileReaderUtil.getStringList(FileNameUtil.dataFileName());
        //List<String> raw = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName());

        String polymerStart = raw.get(0).strip();
        for (int i = 2; i < raw.size(); i++)
        {
            String line = raw.get(i);
            String[] split = line.split("->");
            convertions.put(split[0].strip(), split[1].strip());
        }

        System.out.println(polymerStart);
        System.out.println(convertions);

        part1(polymerStart);
        part2(polymerStart);


    }

    private static void part1(String polymerStart)
    {
        String char2 = null;
        for (int i = 0; i < polymerStart.length() - 1; i++)
        {
            String char1 = Character.toString(polymerStart.charAt(i));
            char2 = Character.toString(polymerStart.charAt(i + 1));
            //System.out.print(char1);
            walk(char1, char2, 0);
        }
        System.out.println(char2);
        countersPart1.compute(char2, (c, v) -> v == null ? 1L : v + 1);
        System.out.println(countersPart1);

        long min = countersPart1.values().stream().mapToLong(l -> l).min().getAsLong();
        long max = countersPart1.values().stream().mapToLong(l -> l).max().getAsLong();
        System.out.println("result: " + (max - min));
    }

    private static void part2(String polymerStart)
    {
        String char2 = null;
        for (int i = 0; i < polymerStart.length() - 1; i++)
        {
            String char1 = Character.toString(polymerStart.charAt(i));
            char2 = Character.toString(polymerStart.charAt(i + 1));
            walkWithMemorization(char1, char2, 0);
        }

        countersPart2.compute(char2, (c, v) -> v == null ? 1L : v + 1);
        System.out.println(countersPart2);

        long min = countersPart2.values().stream().mapToLong(l -> l).min().getAsLong();
        long max = countersPart2.values().stream().mapToLong(l -> l).max().getAsLong();
        System.out.println("result: " + (max - min));
    }

    private static void walk(String char1, String char2, int depth)
    {
        if (depth >= maxDepth)
        {
            //    System.out.print(char1);
            countersPart1.compute(char1, (c, v) -> v == null ? 1L : v + 1);
            return;
        }
        String newChar = convertions.get(char1 + char2);
        if (newChar != null)
        {
            //  System.out.println(char1+char2 + "->" + newChar + "depth: " + depth );
            walk(char1, newChar, depth + 1);
            walk(newChar, char2, depth + 1);
        }

    }

    private static Map<String, Long> walkWithMemorization(String char1, String char2, int depth)
    {
        if (depth >= maxDepth)
        {
            var singleResult = Map.of(char1, 1L);
            addToCounterMap(countersPart2, singleResult);
            return singleResult;
        }
        String newChar = convertions.get(char1 + char2);
        if (newChar != null)
        {
            //no need to walk if we know the result already...
            ResultKey left = new ResultKey(char1, newChar, depth + 1);
            var leftResult = resultMap.get(left);
            if (leftResult == null)
            {
                leftResult = walkWithMemorization(char1, newChar, depth + 1);
                resultMap.put(left, leftResult);
            } else
            {
                addToCounterMap(countersPart2, leftResult);
            }

            ResultKey right = new ResultKey(newChar, char2, depth + 1);
            var rightResult = resultMap.get(right);
            if (rightResult == null)
            {
                rightResult = walkWithMemorization(newChar, char2, depth + 1);
                resultMap.put(right, rightResult);
            } else
            {
                addToCounterMap(countersPart2, rightResult);
            }

            return combineMaps(leftResult, rightResult);
        }

        //Doesn't happen
        System.out.println("IT HAPPENED!");
        return Collections.emptyMap();
    }

    private static Map<String, Long> combineMaps(Map<String, Long> left, Map<String, Long> right)
    {
        Map<String, Long> combined = new HashMap<>();
        left.forEach((k, v) -> combined.compute(k, (c, val) -> val == null ? v : val + v));
        right.forEach((k, v) -> combined.compute(k, (c, val) -> val == null ? v : val + v));
        return combined;
    }

    private static void addToCounterMap(Map<String, Long> counters, Map<String, Long> toAdd)
    {
        toAdd.forEach((k, v) -> counters.compute(k, (c, val) -> val == null ? v : val + v));
    }

    private static record ResultKey(String cha1, String cha2, int depth)
    {
    }
}
