package nl.mrtijmen.aoc2021.graph;

import java.util.*;
import java.util.stream.Collectors;

public class Node
{
    private final String name;
    private final int maxVisits;
    private final Set<Node> neighbours = new HashSet<>();
    private int visitCount = 0;

    public Node(String name, int maxVisits)
    {
        this.name = name;
        this.maxVisits = maxVisits;
    }

    public void addNeighbour(Node neighbour)
    {
        neighbour.addNeighbourRevers(this);
        neighbours.add(neighbour);
    }

    private void addNeighbourRevers(Node neighbour)
    {
        neighbours.add(neighbour);
    }

    public int visit()
    {
        if (maxVisits == -1)
        {
            return -1;
        }
        visitCount += 1;
        return visitCount;
    }

    public int exit()
    {
        if (maxVisits == -1)
        {
            return -1;
        }
        visitCount -= 1;
        return visitCount;
    }

    public boolean canVisit()
    {
        return (maxVisits == -1 || visitCount < maxVisits);
    }

    public String getName()
    {
        return name;
    }

    public int getMaxVisits()
    {
        return maxVisits;
    }

    public Set<Node> getNeighbours()
    {
        return Collections.unmodifiableSet(neighbours);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Node node = (Node) o;
        return maxVisits == node.maxVisits && name.equals(node.name);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(name, maxVisits);
    }


    @Override
    public String toString()
    {
        return "Node{" +
                "name='" + name + '\'' +
                ", maxVisits=" + maxVisits +
                ", neighbours=" + neighbours.stream().map(n -> n.name).collect(Collectors.joining(",")) +
                '}';
    }
}
