package nl.mrtijmen.aoc2021;

import com.google.common.base.Strings;

import java.util.Arrays;
import java.util.stream.Collectors;

public class Day16
{

    private static final String INPUT = "005410C99A9802DA00B43887138F72F4F652CC0159FE05E802B3A572DBBE5AA5F56F6B6A4600FCCAACEA9CE0E1002013A55389B064C0269813952F983595234002DA394615002A47E06C0125CF7B74FE00E6FC470D4C0129260B005E73FCDFC3A5B77BF2FB4E0009C27ECEF293824CC76902B3004F8017A999EC22770412BE2A1004E3DCDFA146D00020670B9C0129A8D79BB7E88926BA401BAD004892BBDEF20D253BE70C53CA5399AB648EBBAAF0BD402B95349201938264C7699C5A0592AF8001E3C09972A949AD4AE2CB3230AC37FC919801F2A7A402978002150E60BC6700043A23C618E20008644782F10C80262F005679A679BE733C3F3005BC01496F60865B39AF8A2478A04017DCBEAB32FA0055E6286D31430300AE7C7E79AE55324CA679F9002239992BC689A8D6FE084012AE73BDFE39EBF186738B33BD9FA91B14CB7785EC01CE4DCE1AE2DCFD7D23098A98411973E30052C012978F7DD089689ACD4A7A80CCEFEB9EC56880485951DB00400010D8A30CA1500021B0D625450700227A30A774B2600ACD56F981E580272AA3319ACC04C015C00AFA4616C63D4DFF289319A9DC401008650927B2232F70784AE0124D65A25FD3A34CC61A6449246986E300425AF873A00CD4401C8A90D60E8803D08A0DC673005E692B000DA85B268E4021D4E41C6802E49AB57D1ED1166AD5F47B4433005F401496867C2B3E7112C0050C20043A17C208B240087425871180C01985D07A22980273247801988803B08A2DC191006A2141289640133E80212C3D2C3F377B09900A53E00900021109623425100723DC6884D3B7CFE1D2C6036D180D053002880BC530025C00F700308096110021C00C001E44C00F001955805A62013D0400B400ED500307400949C00F92972B6BC3F47A96D21C5730047003770004323E44F8B80008441C8F51366F38F240";
    static int versionSum = 0;

    public static void main(String[] args) throws Exception
    {
        //List<String> raw = FileReaderUtil.getStringList(FileNameUtil.dataFileName());
        //List<String> raw = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName());


        System.out.println("0123456".substring(1, 3));

        //String line = "D2FE28";
        //String line = "38006F45291200";
        // String line = "EE00D40C823060";
        // String line = "8A004A801A8002F478"; //16
        // String line =  "620080001611562C8802118E34"; //12
        // String line = "C0015000016115A2E0802F182340"; //23
        //    String line =  "A0016C880162017C3686B18A3D4780"; //31


        //          String line = "C200B40A82";
        // String line = "04005AC33890";
        //   String line = "880086C3E88112";
        // String line = "CE00C43D881120";
        //     String line = "D8005AC2A8F0";
        //    String line = "F600BC2D8F";
        //   String line = "9C005AC2F8F0";
       // String line = "9C0141080250320F1802104A08";


        String line = INPUT;

        String result = Arrays.stream(line.split(""))
                              .map(s -> Integer.parseInt(s, 16))
                              .map(Integer::toBinaryString)
                              .map(s -> Strings.padStart(s, 4, '0'))
                              .collect(Collectors.joining());
        System.out.println(result);

        processPackage(result);

        System.out.println("result part 1: " + versionSum);

    }

    private static void processPackage(String string)
    {
        System.out.println("processing " + string);

        if (!string.contains("1"))
        {
            //trailing 0's
            return;
        }

        Header header = readHeader(string);
        string = string.substring(6);
        if (header.type() == 4)
        {
            ProcessResult sub = processLiteralValue(string);
            processPackage(string.substring(sub.nrOfChars));
            return;
        }

        Operation operation = Operation.fromType(header.type());
        ProcessResult sub;

        if (string.charAt(0) == '0')
        {
            int subPackageLength = Integer.parseInt(string.substring(1, 1 + 15), 2);
            sub = processPackagesWithLength(string.substring(16), subPackageLength, operation);
        } else
        {
            int nrOfSubpackages = Integer.parseInt(string.substring(1, 1 + 11), 2);
            sub = processPackagesWithCount(string.substring(1 + 11), nrOfSubpackages, operation);
        }
        System.out.println("result part 2: " + sub.result());
    }

    private static ProcessResult processPackagesWithLength(String string, int length, Operation operation)
    {
        System.out.println("with length " + length + " processing " + string);
        if (length > string.length())
        {
            throw new RuntimeException("lengh tooo long");
        }

        int processedLength = 0;
        Long result = null;
        while (processedLength < length)
        {
            Header header = readHeader(string);
            string = string.substring(6);
            processedLength += 6;
            ProcessResult sub;

            if (header.type() == 4)
            {
                sub = processLiteralValue(string);
            } else
            {
                Operation subOperation = Operation.fromType(header.type());
                if (string.charAt(0) == '0')
                {
                    System.out.println(string.substring(1, 16) + " L-> " + Integer.parseInt(string.substring(1, 1 + 15), 2));
                    int subPackageLength = Integer.parseInt(string.substring(1, 16), 2);
                    string= string.substring(16);
                    processedLength+=16;
                    sub = processPackagesWithLength(string, subPackageLength, subOperation);

                } else
                {
                    System.out.println(string.substring(1, 1 + 11) + " C-> " + Integer.parseInt(string.substring(1, 1 + 11), 2));
                    int nrOfSubpackages = Integer.parseInt(string.substring(1, 1 + 11), 2);
                    string= string.substring(12);
                    processedLength+=12;
                    sub = processPackagesWithCount(string, nrOfSubpackages, subOperation);
                }
            }
            result = operation.apply(result, sub.result);
            processedLength += sub.nrOfChars;
            string = string.substring(sub.nrOfChars);
        }

        System.out.println("return from length: " + result);
        return new ProcessResult(result, processedLength);

    }


    private static ProcessResult processPackagesWithCount(String string, int count, Operation operation)
    {
        System.out.println("with count " + count + " processing " + string);


        int processedLength = 0;
        Long result = null;
        for (int i = 0; i < count; i++)
        {
            Header header = readHeader(string);
            string = string.substring(6);
            processedLength += 6;

            ProcessResult sub;

            if (header.type() == 4)
            {
                sub = processLiteralValue(string);
            } else
            {
                Operation subOperation = Operation.fromType(header.type());

                if (string.charAt(0) == '0')
                {
                    int subPackageLength = Integer.parseInt(string.substring(1, 1 + 15), 2);
                    string= string.substring(16);
                    processedLength+=16;
                    sub = processPackagesWithLength(string, subPackageLength, subOperation);

                } else
                {
                    final String substring = string.substring(1,12);
                    System.out.println("-------> " +substring);
                    int nrOfSubpackages = Integer.parseInt(substring, 2);
                    string= string.substring(12);
                    processedLength+=12;
                    sub = processPackagesWithCount(string, nrOfSubpackages, subOperation);
                }
            }
            result = operation.apply(result, sub.result);
            processedLength += sub.nrOfChars;
            string = string.substring(sub.nrOfChars);
        }

        System.out.println("return from count: " + result);
        return new ProcessResult(result, processedLength);
    }


    private static ProcessResult processLiteralValue(String string)
    {
        System.out.println("to literal -> " + string);
        StringBuilder result = new StringBuilder();

        for (int i = 0; i <= string.length() - 5; i += 5)
        {
            result.append(string.substring(i + 1, i + 5));
            //System.out.println(i + ": " + string.charAt(i));
            if (string.charAt(i) == '0')
            {
                System.out.println(Long.parseLong(result.toString(), 2));
                return new ProcessResult(Long.parseLong(result.toString(), 2), i + 5);
            }
        }
        throw new RuntimeException("fout");
    }

    private static Header readHeader(String string)
    {
        String nr1 = string.substring(0, 3);
        String nr2 = string.substring(3, 6);

        int packetVersion = Integer.parseInt(nr1, 2);
        int packetType = Integer.parseInt(nr2, 2);

        versionSum += packetVersion;

        System.out.println("version: " + packetVersion);
        System.out.println("type: " + packetType);
        return new Header(packetVersion, packetType);
    }

    enum Operation
    {
        SUM, PRODUCT, MIN, MAX, GT, LT, EQUAL;

        static Operation fromType(int packetType)
        {
            System.out.println("PackageType: " + packetType);
            return switch (packetType)
                    {
                        case 0 -> SUM;
                        case 1 -> PRODUCT;
                        case 2 -> MIN;
                        case 3 -> MAX;
                        case 5 -> GT;
                        case 6 -> LT;
                        case 7 -> EQUAL;
                        default -> null;
                    };
        }

        long apply(Long nr1, Long nr2)
        {
            //hack
            if (nr1 == null && (this == GT || this == LT || this == EQUAL))
            {
                return nr2;
            }

            //case SUM
            return switch (this)
                    {
                        case SUM -> (nr1 == null ? 0 : nr1) + nr2;
                        case PRODUCT -> (nr1 == null ? 1 : nr1) * nr2;
                        case MIN -> Long.min((nr1 == null ? Long.MAX_VALUE : nr1), nr2);
                        case MAX -> Long.max((nr1 == null ? 0 : nr1), nr2);
                        case GT -> (nr1 > nr2) ? 1 : 0;
                        case LT -> (nr1 < nr2) ? 1 : 0;
                        case EQUAL -> (nr1.equals(nr2)) ? 1 : 0;
                    };

        }
    }

    private static record Header(int version, int type)
    {
    }

    private static record ProcessResult(Long result, int nrOfChars)
    {
    }
}
