package nl.mrtijmen.aoc2021.origami;

public record FoldInstruction(int line, Orientation orientation)
{
    public enum Orientation
    {
        HORIZONTAL, VERTICAL;
    }
}
