package nl.mrtijmen.aoc2021.origami;

public enum Mark
{
    UMARKED("."),
    MARKED("#");

    private final String mark;

    Mark(String mark)
    {
        this.mark = mark;
    }

    public final String mark()
    {
        return mark;
    }

    @Override
    public String toString()
    {
        return mark;
    }
}
