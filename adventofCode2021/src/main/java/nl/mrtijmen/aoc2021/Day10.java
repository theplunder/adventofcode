package nl.mrtijmen.aoc2021;

import nl.mrtijmen.aoccommons.util.FileNameUtil;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;

import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.function.Function;

public class Day10
{
    public static void main(String[] args) throws Exception
    {
        //List<String> raw = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName());
        List<String> raw = FileReaderUtil.getStringList(FileNameUtil.dataFileName());

        int result = raw.stream()
                        .map(Day10::findFirstUnmatchedBracked)
                        .filter(Objects::nonNull)
                        .mapToInt(Day10::pointPart1)
                        .sum();

        System.out.println("result part 1 " + result);

        List<Long> scores = raw.stream()
                               .map(Day10::finishIncomplete)
                               .filter(Objects::nonNull)
                               .map(Day10::scoreList)
                               .sorted().toList();

        long result2 = scores.get(scores.size() / 2);
        System.out.println("result part 2 " + result2);
    }

    public static int pointPart1(Character c)
    {
        return switch (c)
                {
                    case ')' -> 3;
                    case ']' -> 57;
                    case '}' -> 1197;
                    case '>' -> 25137;
                    default -> 0;
                };
    }

    private static Character findFirstUnmatchedBracked(String input)
    {
        char[] chars = input.toCharArray();
        System.out.println(input);
        Stack<BracketPair> stack = new Stack<>();
        for (char c : chars)
        {
            if (BracketPair.isOpenChar(c))
            {
                stack.push(BracketPair.fromOpenBracket(c));
            } else
            {
                if (c == stack.peek().close)
                {
                    stack.pop();
                } else
                {
                    System.out.println("unmatched: " + c);
                    return c;
                }
            }
        }
        System.out.println("incomplete");
        return null;
    }

    private static List<Character> finishIncomplete(String input)
    {
        char[] chars = input.toCharArray();
        System.out.println(input);
        Stack<BracketPair> stack = new Stack<>();
        for (char c : chars)
        {
            if (BracketPair.isOpenChar(c))
            {
                stack.push(BracketPair.fromOpenBracket(c));
            } else
            {
                if (c == stack.peek().close)
                {
                    stack.pop();
                } else
                {
                    System.out.println("unmatched: " + c);
                    return null;
                }
            }
        }

        List<Character> list = new ArrayList<>();
        while (!stack.isEmpty())
        {
            list.add(stack.pop().close);
        }

        System.out.println(list);
        return list;
    }

    public static long pointPart2(Character c)
    {
        return switch (c)
                {
                    case ')' -> 1;
                    case ']' -> 2;
                    case '}' -> 3;
                    case '>' -> 4;
                    default -> throw new RuntimeException("fout char: " + c);
                };
    }

    public static <T extends Enum<?>, A> T enumInit(Class<T> enumClass, Function<T, A> createFunction, A equalsTarget) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException
    {
        return Arrays.stream((T[]) enumClass.getDeclaredMethod("values").invoke(null))
                     .filter(a -> equalsTarget.equals(createFunction.apply(a)))
                     .findFirst()
                     .orElseThrow(() -> new IllegalArgumentException("asdfds"));
    }

    public static long scoreList(List<Character> list)
    {
        return list.stream()
                   .map(Day10::pointPart2)
                   .reduce(0L, (subtotal, element) -> subtotal * 5L + element);
    }

    private enum BracketPair
    {
        CURLY('{', '}'),
        BOX('[', ']'),
        ROUND('(', ')'),
        ANGLE('<', '>');

        private static final List<Character> openChars = Arrays.stream(BracketPair.values())
                                                               .map(BracketPair::getOpen)
                                                               .toList();
        private final Character open;
        private final Character close;

        BracketPair(Character open, Character close)
        {
            this.open = open;
            this.close = close;
        }

        public static BracketPair fromOpenBracket(Character open)
        {
            return Arrays.stream(BracketPair.values())
                         .filter(b -> b.open.equals(open))
                         .findAny()
                         .orElseThrow(() -> new IllegalArgumentException("fout"));
        }

        public static boolean isOpenChar(char c)
        {
            return openChars.contains(c);
        }

        public Character getOpen()
        {
            return open;
        }

        public Character getClose()
        {
            return close;
        }

    }
}
