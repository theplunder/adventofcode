package nl.mrtijmen.aoc2021;

import nl.mrtijmen.aoc2021.graph.Node;
import nl.mrtijmen.aoccommons.util.FileNameUtil;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;

import java.util.*;

public class Day12
{

    private static Map<String, Node> nodeCache = new HashMap<>();

    private static Node doublyVisitedCave = null;

    public static void main(String[] args) throws Exception
    {
        //List<String> raw = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName(1));
        // List<String> raw = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName(2));
        List<String> raw = FileReaderUtil.getStringList(FileNameUtil.dataFileName());

        raw.stream().map(s -> s.split("-")).forEach(sArr -> {
                    Node node = buildNode(sArr[0]);
                    node.addNeighbour(buildNode(sArr[1]));
                }
        );

        System.out.println(nodeCache);
        List<List<Node>> paths = visitAllNodes(nodeCache.get("start"), nodeCache.get("end"));
        System.out.println("result part 1; nr of paths: " + paths.size());


        System.out.println(nodeCache);
        List<List<Node>> paths2 = visitAllNodes2(nodeCache.get("start"), nodeCache.get("end"));
        System.out.println("result part 2; nr of paths: " + paths2.size());

    }

    private static List<List<Node>> visitAllNodes(Node start, Node end)
    {
        Stack<Node> visitStack = new Stack<>();
        List<List<Node>> paths = new ArrayList<>();
        nodeVisitor(start, end, visitStack, paths);
        return paths;
    }

    private static void nodeVisitor(Node start, Node end, Stack<Node> visitStack, List<List<Node>> paths)
    {
        start.visit();
        visitStack.push(start);
        for (Node node : start.getNeighbours())
        {
            if (!node.canVisit())
            {
                continue;
            }

            if (node != end)
            {
                nodeVisitor(node, end, visitStack, paths);
            } else
            {
                System.out.println(visitStack);
                paths.add(visitStack.stream().toList());
            }
        }
        start.exit();
        visitStack.pop();
    }

    private static List<List<Node>> visitAllNodes2(Node start, Node end)
    {
        Stack<Node> visitStack = new Stack<>();
        List<List<Node>> paths = new ArrayList<>();
        nodeVisitor2(start, end, visitStack, paths);
        return paths;
    }

    private static void nodeVisitor2(Node start, Node end, Stack<Node> visitStack, List<List<Node>> paths)
    {
        start.visit();
        visitStack.push(start);
        for (Node node : start.getNeighbours())
        {
            //are we a candidate for a double-visit?
            boolean canDoubleVisit;
            //       System.out.println("can double visit: " + node);
            if (node == nodeCache.get("start") || node == nodeCache.get("end"))
            {
                canDoubleVisit = false;
            } else
            {
                canDoubleVisit = node.getMaxVisits() != -1;
            }


            if (!node.canVisit())
            {
                if (canDoubleVisit && doublyVisitedCave == null)
                {
                    //            System.out.println("doing double visit! " + node);
                    doublyVisitedCave = node;
                } else
                {
                    continue;
                }
            }

            if (node != end)
            {
                nodeVisitor2(node, end, visitStack, paths);
            } else
            {
                System.out.println(visitStack);
                paths.add(visitStack.stream().toList());
            }
            if (doublyVisitedCave == node)
            {
                doublyVisitedCave = null;
            }
        }
        start.exit();
        visitStack.pop();
    }


    private static Node buildNode(String name)
    {
        return nodeCache.computeIfAbsent(name.strip(), s -> {
            s = s.strip();
            if (s.equals(s.toUpperCase()))
            {
                return new Node(s, -1);
            }
            return new Node(s, 1);
        });
    }

}
