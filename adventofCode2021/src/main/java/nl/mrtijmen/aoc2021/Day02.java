package nl.mrtijmen.aoc2021;

import nl.mrtijmen.aoccommons.util.grid.Coordinate;
import nl.mrtijmen.aoc2021.movement.CoordinateWithAim;
import nl.mrtijmen.aoc2021.movement.Direction;
import nl.mrtijmen.aoc2021.movement.Instruction;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;

import java.util.List;

public class Day02
{
    public static void main(String[] args) throws Exception
    {
       // List<String> input = FileReaderUtil.getStringList("./src/main/resources/Day02InputExample.txt");
         List<String> input = FileReaderUtil.getStringList("./src/main/resources/Day02Input.txt");

        List<Instruction> instructions =
                input.stream().map(s -> {
                    String[] split = s.split(" ");
                    return new Instruction(Direction.fromCode(split[0].strip()), Long.parseLong(split[1].strip()));
                }).toList();

        Coordinate coordinate = new Coordinate(0,0);
        for(Instruction instruction : instructions){
            coordinate = instruction.apply(coordinate);
        }
        System.out.println(coordinate);
        System.out.println("Result part 1: " + (-1*coordinate.x()* coordinate.y()));



        CoordinateWithAim coordinateWithAim = new CoordinateWithAim(0,0, 0);
        for(Instruction instruction : instructions){

            coordinateWithAim = instruction.apply(coordinateWithAim);
        }
        System.out.println(coordinateWithAim);
        System.out.println("Result part 2: " + (-1*coordinateWithAim.x()* coordinateWithAim.y()));

    }

}
