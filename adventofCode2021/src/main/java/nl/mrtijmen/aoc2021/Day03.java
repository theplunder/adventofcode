package nl.mrtijmen.aoc2021;

import nl.mrtijmen.aoccommons.util.FileReaderUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

public class Day03
{

    public static void main(String[] args) throws Exception
    {
     //   List<String> input = FileReaderUtil.getStringList("./src/main/resources/Day03InputExample.txt");
   List<String> input = FileReaderUtil.getStringList("./src/main/resources/Day03Input.txt");

        //part1(input);
        part2(input);


    }

    private static void part2(List<String> input)
    {
        long oxygen = elementFinder(input, (count, list) -> (count < list.size()/2.)?'0':'1');
        System.out.println("oxygen: " + oxygen );

        long co2 = elementFinder(input, (count, list) -> (count < list.size()/2.)?'1':'0');
        System.out.println("co2: " + co2 );

        System.out.println("result part 2: " + (oxygen * co2));
    }

    private static int elementFinder(List<String> input, BiFunction<Long, List<String> , Character> pred)
    {
        List<String> numbers = new ArrayList<>(input);
        int size = input.get(0).length();
        for (int i = 0; i < size; i++)
        {
            final int ii = i;
            long count = numbers.stream().filter(s->s.charAt(ii) == '1').count();

            char charToKeep = pred.apply(count, numbers);
            numbers = numbers.stream().filter(s->s.charAt(ii) == charToKeep).toList();

            if (numbers.size() == 1){
                break;
            }
        }
        System.out.println(numbers.size());
        return Integer.parseInt(numbers.get(0), 2);
    }

    private static void part1(List<String> input)
    {
        int[] counters = new int[input.get(0).length()];
        for (String code : input)
        {
            for (int i = 0; i < code.length(); i++)
            {
                if (code.charAt(i) == '1')
                {
                    counters[i]++;
                }
            }
        }

        System.out.println(Arrays.stream(counters).mapToObj(String::valueOf)
                                 .collect(Collectors.joining()));

        System.out.println("inputSize: " + input.size());


        String gammaBinary = Arrays.stream(counters)
                                   .map(i -> i > input.size() / 2 ? 1 : 0)
                                   .mapToObj(String::valueOf)
                                   .collect(Collectors.joining());
        String epsilonBinary = Arrays.stream(counters)
                                     .map(i -> i > input.size() / 2 ? 0 : 1)
                                     .mapToObj(String::valueOf)
                                     .collect(Collectors.joining());

        int gamma = Integer.parseInt(gammaBinary, 2);
        int epsilon = Integer.parseInt(epsilonBinary, 2);
        System.out.println("result part 1: " + (gamma * epsilon));
    }

}
