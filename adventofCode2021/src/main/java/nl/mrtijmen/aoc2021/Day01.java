package nl.mrtijmen.aoc2021;

import nl.mrtijmen.aoccommons.util.FileReaderUtil;

import java.util.ArrayList;
import java.util.List;

public class Day01
{

    public static void main(String[] args) throws Exception
    {
        //   List<Integer> depthMeasurements = FileReaderUtil.getNumberList("./src/main/resources/Day01InputExample.txt");
        List<Integer> depthMeasurements = FileReaderUtil.getNumberList("./src/main/resources/Day01Input.txt");

        long countIncreased = getCountIncreased(depthMeasurements, 1);
        System.out.println("Depth increased: " + countIncreased + " times");


        long countIncreasedSliding = getCountIncreased(depthMeasurements, 3);
        System.out.println("Sliding Depth increased: " + countIncreasedSliding + " times");

    }

    private static long getCountIncreased(List<Integer> depthMeasurements, int window)
    {
        List<Integer> diff = new ArrayList<>();
        for (int i = 0; i < depthMeasurements.size() - window; i++)
        {
            diff.add(depthMeasurements.get(i + window) - depthMeasurements.get(i));
        }
        return diff.stream().filter(i -> i > 0).count();
    }
}
