package nl.mrtijmen.aoc2021.display;

import java.util.Arrays;
import java.util.List;

/**
 * 1
 * 2 3
 * 4
 * 5 6
 * 7
 */
public enum SegmentDisplay
{
    ZERO(0, 1, 2, 3, 5, 6, 7),
    ONE(1, 3, 6),
    TWO(2, 1, 3, 4, 5, 7),
    THREE(3, 1, 3, 4, 6, 7),
    FOUR(4, 2, 3, 4, 6),
    FIVE(5, 1, 2, 4, 6, 7),
    SIX(6, 1, 2, 4, 5, 6, 7),
    SEVEN(7, 1, 3, 6),
    EIGHT(8, 1, 2, 3, 4, 5, 6, 7),
    NINE(9, 1, 2, 3, 4, 6, 7);

    private final int value;
    private final int[] segments;
    private final List<Integer> segmentsArray;

    SegmentDisplay(int value, int... segments)
    {
        this.value = value;
        this.segments = segments;
        segmentsArray = Arrays.stream(segments).boxed().toList();
    }

    public static List<SegmentDisplay> getSegmentsWithCount(int n)
    {
        return Arrays.stream(SegmentDisplay.values()).filter(s -> s.segmentCout() == n).toList();
    }

    public int getValue()
    {
        return value;
    }

    public List<Integer> getSegments()
    {
        return segmentsArray;
    }

    public int segmentCout()
    {
        return segments.length;
    }

}
