package nl.mrtijmen.aoc2021;

import nl.mrtijmen.aoccommons.util.grid.Coordinate;

public class Day17
{

    public static void main(String[] args)
    {
         //   TargetArea targetArea = new TargetArea(20, 30, -10, -5);
//////actual input:
        TargetArea targetArea = new TargetArea(96, 125, -144, -98);

//        Coordinate initalSpeed = new Coordinate(6, 9);
//        for (int i = 0; i < 22; i++)
//        {
//            System.out.println(afterSteps(initalSpeed, i));
//        }
        long result = findMaxY(targetArea);
        System.out.println("result part1: " + result);

        int yMin = targetArea.yMin;
        int yMax = (-1 * targetArea.yMin) - 1;

        int xMin = findMinX(targetArea);
        int xMax = targetArea.xMax;

        int counter = 0;

        for (int x = xMin; x <= xMax; x++)
        {
            for (int y = yMin; y <= yMax; y++)
            {
                boolean hit = willHitArea(new Coordinate(x, y), targetArea);
                if (hit)
                {
                    counter++;
                }
            }

        }
        System.out.println("part 2: " + counter);

        //part2 -> find ranges of X separate form ranges of y
    }

    private static Coordinate afterSteps(Coordinate initalSpeed, int nSteps)
    {
        long xSteps = nSteps > initalSpeed.x() ? initalSpeed.x() : nSteps;
        long x = initalSpeed.x() * (xSteps + 1) - triagonal(xSteps);
        long y = initalSpeed.y() * (nSteps + 1) - triagonal(nSteps);
        return new Coordinate(x, y);
    }

    private static long triagonal(long n)
    {
        return (long) (n * (n + 1) / 2.);
    }


    private static int findMinX(TargetArea targetArea)
    {
        int target = targetArea.xMin;
        int i = 1;
        for (; triagonal(i) < target; i++)
        {

        }
        return i;
    }


    //we always come back at 0.
    //highest y == fastest downspeed, so, within one step from 0 to within target Area
    private static long findMaxY(TargetArea targetArea)
    {
        int y0 = (-1 * targetArea.yMin) - 1;

        return afterSteps(new Coordinate(0, y0), y0 - 1).y();
    }

    private static boolean willHitArea(Coordinate init, TargetArea targetArea)
    {
        for (int i = 0; i < 10000; i++)
        {
            Coordinate after = afterSteps(init, i);
            if (targetArea.withinArea(after))
            {
                return true;
            }
            if (after.y() < targetArea.yMin || after.x() > targetArea.xMax)
            {
                return false;
            }
        }
        System.out.println(init);

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        return false;


    }

    private static record TargetArea(int xMin, int xMax, int yMin, int yMax)
    {
        public boolean withinArea(Coordinate coordinate)
        {
            return coordinate.x() >= xMin && coordinate.x() <= xMax && coordinate.y() >= yMin && coordinate.y() <= yMax;
        }
    }


}
