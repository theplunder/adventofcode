package nl.mrtijmen.aoc2024.labguard;

import nl.mrtijmen.aoccommons.util.grid.Coordinate;
import nl.mrtijmen.aoccommons.util.grid.Direction;

public record RoutePoint(Coordinate coordinate, Direction direction)
{
}
