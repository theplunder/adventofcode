package nl.mrtijmen.aoc2024;

import nl.mrtijmen.aoc2024.printer.OrderRule;
import nl.mrtijmen.aoccommons.util.FileNameUtil;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;
import nl.mrtijmen.aoccommons.util.ParseUtil;

import java.util.ArrayList;
import java.util.List;

public class Day05
{

    public static void main(String[] args) throws Exception
    {
        //  List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName(1));
        List<String> data = FileReaderUtil.getStringList(FileNameUtil.dataFileName());

        int splitIndex = data.indexOf("");
        List<OrderRule> rules = parseRules(data.subList(0, splitIndex));
        //   System.out.println(rules);
        List<List<Integer>> pages = parsePages(data.subList(splitIndex + 1, data.size()));
        //   System.out.println(pages);


        long result1 = part1(rules, pages);
        System.out.println("Result Part 1 --> " + result1);

        long result2 = part2(rules, pages);
        System.out.println("Result Part 2 --> " + result2);
    }


    //To get the printers going as soon as possible, start by identifying which updates are already in the right order.

    private static long part1(List<OrderRule> rules, List<List<Integer>> pages)
    {
        int result = 0;
        for (var page : pages)
        {
            var sorted = sortPages(rules, page);
            if (sorted.equals(page))
            {
                result += sorted.get(((sorted.size() + 1) / 2) - 1);
            }
        }
        return result;
    }

    private static long part2(List<OrderRule> rules, List<List<Integer>> pages)
    {
        int result = 0;
        for (var page : pages)
        {
            var sorted = sortPages(rules, page);
            if (!sorted.equals(page))
            {
                System.out.println("--------->" + sorted);
                result += sorted.get(((sorted.size() + 1) / 2) - 1);
            }
        }
        return result;
    }

    private static List<Integer> sortPages(List<OrderRule> rules, List<Integer> pageList)
    {
        System.out.println("stating: " + pageList);
        //filter relevant rules.
        var filteredRules = rules.stream()
                                 .filter(r -> pageList.contains(r.before()) && pageList.contains(r.after()))
                                 .toList();

        List<Integer> book = new ArrayList<>();

        for (int page : pageList)
        {
            int minIndex = 0;

            System.out.println(page);
            for (OrderRule rule : filteredRules)
            {
                if (!((page == rule.before() && book.contains(rule.after())) ||
                        (page == rule.after() && book.contains(rule.before()))))
                {
                    continue;
                }

                System.out.println(rule);
                if (page == rule.before())
                {
                    int index = book.indexOf(rule.after());
                    minIndex = Math.min(index, minIndex);

                } else //page == rule.after()
                {
                    int index = book.indexOf(rule.before());
                    minIndex = Math.max(index + 1, minIndex);
                }
//                System.out.println(book);
//                System.out.println("min: " + minIndex);
            }

            book.add(minIndex, page);
        }
        System.out.println(book);

        return book;
    }


    private static List<OrderRule> parseRules(List<String> data)
    {
        return data.stream().map(OrderRule::fromString).toList();
    }

    private static List<List<Integer>> parsePages(List<String> data)
    {
        return data.stream().map(s -> ParseUtil.lineToNumberList(s, ",")).toList();
    }

}
