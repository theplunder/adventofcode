package nl.mrtijmen.aoc2024;

import nl.mrtijmen.aoc2024.labguard.MapObject;
import nl.mrtijmen.aoc2024.labguard.RoutePoint;
import nl.mrtijmen.aoccommons.util.FileNameUtil;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;
import nl.mrtijmen.aoccommons.util.ParseUtil;
import nl.mrtijmen.aoccommons.util.grid.Coordinate;
import nl.mrtijmen.aoccommons.util.grid.Direction;
import nl.mrtijmen.aoccommons.util.grid.Grid;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static nl.mrtijmen.aoccommons.util.grid.Direction.*;

public class Day06
{
    public static void main(String[] args) throws Exception
    {
        Coordinate.UP_IS_NEGATIVE_Y = true;
        //List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName());
        List<String> data = FileReaderUtil.getStringList(FileNameUtil.dataFileName());

        Grid<MapObject> map = new Grid<>(ParseUtil.parseObjectMatrix(data, "", MapObject::fromIcon));

        System.out.println(map);

        long result1 = part1(map);
        System.out.println("Result Part 1 --> " + result1);

        long result2 = findLoops(map);
        System.out.println("Result Part 2 --> " + result2);
    }

    private static long part1(Grid<MapObject> map)
    {
        Coordinate start = map.find(MapObject.START::equals).getFirst();

        var route = findRoute(start, map);
        return route.stream().map(RoutePoint::coordinate).distinct().count();
    }

    private static Set<RoutePoint> findRoute(Coordinate start, Grid<MapObject> map)
    {
        Set<RoutePoint> route = new HashSet<>();

        RoutePoint current = new RoutePoint(start, UP);
        while (!route.contains(current))
        {
            route.add(current);

            //assume no contact with edge for now;
            Direction direction = current.direction();
            Coordinate nextCandidate = current.coordinate().move(direction);

            //exit loop when leaving area
            if (!map.coordinateWithinRange(nextCandidate))
            {
                break;
            }

            int i = 0;
            while (map.getValue(nextCandidate).equals(MapObject.OBJECT))
            {
                direction = rotate(direction);
                nextCandidate = current.coordinate().move(direction);
                //assume no infi-loops
                if (++i == 4) throw new RuntimeException("infiloop!");
            }

            current = new RoutePoint(nextCandidate, direction);
            System.out.println(current);
        }


        return route;
    }

    private static int findLoops(Grid<MapObject> map)
    {
        Set<RoutePoint> route = new HashSet<>();
        Coordinate start = map.find(MapObject.START::equals).getFirst();
        Set<Coordinate> obstacles = new HashSet<>();
        Set<Coordinate> visted = new HashSet<>();

        RoutePoint current = new RoutePoint(start, UP);
        while (!route.contains(current))
        {
            //assume no contact with edge for now;
            Direction direction = current.direction();
            Coordinate nextCandidate = current.coordinate().move(direction);

            //exit loop when leaving area
            if (!map.coordinateWithinRange(nextCandidate))
            {
                break;
            }

            int i = 0;
            while (map.getValue(nextCandidate).equals(MapObject.OBJECT))
            {
                direction = rotate(direction);
                nextCandidate = current.coordinate().move(direction);
                //assume no infi-loops
                if (++i == 4) throw new RuntimeException("infiloop!");
            }

            //Check if placing an obstacle now is going to make it a loop!
            //Obstacle has to be placed, !before! time starts running!
            if (!visted.contains(nextCandidate) && isLoop(new RoutePoint(current.coordinate(), direction), map, nextCandidate))
            {
                System.out.println("makes loop at: " + nextCandidate);
                if (start.equals(nextCandidate))
                {
                    System.out.println("NOT AT STARTING POINT!");
                } else
                {
                    obstacles.add(nextCandidate);
                }
            }
            route.add(current);
            visted.add(current.coordinate());
            current = new RoutePoint(nextCandidate, direction);
        }


        return obstacles.size();
    }


    private static boolean isLoop(RoutePoint current, Grid<MapObject> map, Coordinate obstacleCandiate)
    {
        Set<RoutePoint> route = new HashSet<>();

        int j = 0;
        while (!route.contains(current))
        {
            route.add(current);

            //assume no contact with edge for now;
            Direction direction = current.direction();
            Coordinate nextCandidate = current.coordinate().move(direction);

            //exit loop when leaving area
            if (!map.coordinateWithinRange(nextCandidate))
            {
                return false;
            }

            int i = 0;
            while (obstacleCandiate.equals(nextCandidate) || map.getValue(nextCandidate).equals(MapObject.OBJECT))
            {
                direction = rotate(direction);
                nextCandidate = current.coordinate().move(direction);
                //assume no infi-loops


                if (++i == 4)
                    throw new RuntimeException("infiloop!"); //I guess this would techically count as a loop?? meh assume not for now
            }

            current = new RoutePoint(nextCandidate, direction);
            j++;
        }
        System.out.println("loop after: " + j);

        return true;
    }


    private static Direction rotate(Direction dir)
    {
        return switch (dir)
        {
            case UP -> RIGHT;
            case RIGHT -> DOWN;
            case DOWN -> LEFT;
            case LEFT -> UP;
        };
    }

}