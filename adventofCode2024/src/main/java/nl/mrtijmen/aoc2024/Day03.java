package nl.mrtijmen.aoc2024;

import nl.mrtijmen.aoccommons.util.FileNameUtil;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day03
{

    private static final String regexP1 = "mul\\((\\d{1,3}),(\\d{1,3})\\)";
    private static final String regexP2 = "(?:mul\\((\\d{1,3}),(\\d{1,3})\\))|(do\\(\\))|(don't\\(\\))";


    public static void main(String[] args) throws Exception
    {
        //      List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName(1));
        List<String> data = FileReaderUtil.getStringList(FileNameUtil.dataFileName());

        long result1 = part1(data);
        System.out.println("Result Part 1 --> " + result1);

//        data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName(2));
        long result2 = part2(data);
        System.out.println("Result Part 2 --> " + result2);
    }

    private static long part1(List<String> data)
    {
        Pattern pattern = Pattern.compile(regexP1);

        long result = 0;

        for (String line : data)
        {
            Matcher matcher = pattern.matcher(line);
            while (matcher.find())
            {
                long left = Long.parseLong(matcher.group(1));
                long right = Long.parseLong(matcher.group(2));
                System.out.println(left + " + " + right);
                result += left * right;
            }
        }

        return result;
    }

    private static long part2(List<String> data)
    {
        Pattern pattern = Pattern.compile(regexP2);

        long result = 0;
        boolean toggle = true;
        for (String line : data)
        {
            System.out.println(data);
            Matcher matcher = pattern.matcher(line);
            while (matcher.find())
            {
                if (matcher.group(3) != null)
                {
                    toggle = true;
                    System.out.println(matcher.group(3));
                } else if (matcher.group(4) != null)
                {
                    toggle = false;
                    System.out.println(matcher.group(4));
                } else if (toggle)
                {
                    long left = Long.parseLong(matcher.group(1));
                    long right = Long.parseLong(matcher.group(2));
                    System.out.println(left + " + " + right);
                    result += left * right;
                }
            }
        }

        return result;
    }
}