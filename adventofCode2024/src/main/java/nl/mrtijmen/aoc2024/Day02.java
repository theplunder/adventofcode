package nl.mrtijmen.aoc2024;

import nl.mrtijmen.aoccommons.util.FileNameUtil;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;
import nl.mrtijmen.aoccommons.util.ParseUtil;

import java.util.List;
import java.util.stream.Stream;

import static nl.mrtijmen.aoc2024.Day02.Safety.SAFE;
import static nl.mrtijmen.aoc2024.Day02.Safety.UNSAFE;

public class Day02
{

    public enum Safety
    {SAFE, UNSAFE}

    public enum SlopeDirection
    {
        INCREASING, DECREASING, FLAT;

        public static SlopeDirection determineSlopeDirection(int first, int second)
        {
            if (first < second) return INCREASING;
            if (first > second) return DECREASING;
            return FLAT;
        }
    }


    public static void main(String[] args) throws Exception
    {
        //List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName());
        List<String> data = FileReaderUtil.getStringList(FileNameUtil.dataFileName());

        var levels = ParseUtil.parseMatrix(data, " ");

        long result1 = part1(levels);
        System.out.println("Result Part 1 --> " + result1);

        long result2 = part2(levels);
        System.out.println("Result Part 2 --> " + result2);
    }


    private static long part1(List<List<Integer>> levels)
    {
        return levels.stream()
                     .map(Day02::determineSafety)
                     .filter(s -> s.equals(SAFE))
                     .count();
    }

    private static long part2(List<List<Integer>> levels)
    {
        return levels.stream()
                     .map(Day02::determineSafetyWithDampner)
                     .filter(s -> s.equals(SAFE))
                     .count();
    }


    private static Safety determineSafety(List<Integer> level)
    {
        SlopeDirection direction = null;
        System.out.println(level);
        for (int i = 0; i < level.size() - 1; i++)
        {
            int current = level.get(i);
            int next = level.get(i + 1);
            int diff = Math.abs(current - next);
            if (diff == 0 || diff > 3)
            {
                System.out.println("not safe!");
                System.out.println("slope is : " + diff);
                return Safety.UNSAFE;
            }

            var currentSlopDir = SlopeDirection.determineSlopeDirection(current, next);

            if (direction == null)
            {
                direction = currentSlopDir;
            } else if (direction != currentSlopDir)
            {
                System.out.println("not safe!");
                System.out.println(level);
                return Safety.UNSAFE;
            }

        }
        System.out.println("safe!");
        return SAFE;
    }

    private static Safety determineSafetyWithDampner(List<Integer> level)
    {
        var isSafe = determineSafety(level);
        if (isSafe == SAFE) return SAFE;

        System.out.println(level);
        for (int i = 0; level.size() > i; i++)
        {
        //    System.out.println(i);
            var left = level.subList(0, i);
            var right = level.subList(i + 1, level.size());
          //  System.out.println(left + " + " + right);
            List<Integer> newList = Stream.concat(left.stream(),
                                                  right.stream())
                                          .toList();
          //  System.out.println(newList);

            if (determineSafety(newList) == SAFE) return SAFE;
        }
        return UNSAFE;


    }
}