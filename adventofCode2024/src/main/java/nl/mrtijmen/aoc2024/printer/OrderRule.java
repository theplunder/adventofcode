package nl.mrtijmen.aoc2024.printer;

import static java.lang.Integer.parseInt;

public record OrderRule(int before, int after)
{

    public static OrderRule fromString(String ruleString)
    {
        var splitted = ruleString.split("\\|");
        return new OrderRule(parseInt(splitted[0]), parseInt(splitted[1]));
    }

}
