package nl.mrtijmen.aoc2024;

import nl.mrtijmen.aoccommons.util.FileNameUtil;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;
import nl.mrtijmen.aoccommons.util.ParseUtil;

import java.util.ArrayList;
import java.util.List;

public class Day09
{
    public static void main(String[] args) throws Exception
    {
//        List<Integer> data = ParseUtil.lineToNumberList(FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName())
//                                                                      .getFirst(), "");
        List<Integer> data = ParseUtil.lineToNumberList(FileReaderUtil.getStringList(FileNameUtil.dataFileName())
                                                                      .getFirst(), "");


        var expanded = expand(data);
        var rearanged = rearange(expanded);
        long result1 = checksum(rearanged);
        System.out.println("Result Part 1 --> " + result1);

        System.out.println(expanded);
        var blocks = buildBlocks(data);
        var rearangedblocks = rearangeBlocks(blocks);
      //  rearangedblocks.stream().forEach(System.out::println);

        long result2 = rearangedblocks.stream().mapToLong(Block::checksum).sum();
        System.out.println("Result Part 2 --> " + result2);
    }

    public static List<Block> buildBlocks(List<Integer> data)
    {
        List<Block> blocks = new ArrayList<>();
        int index = 0;
        for (int i = 0; data.size() > i; i++)
        {
            if (i % 2 == 0)
            {
                blocks.add(new Block(index, data.get(i), i / 2));
            }
            index += data.get(i);
        }
    //    blocks.stream().forEach(System.out::println);
        return blocks;

    }

    public static List<Integer> expand(List<Integer> data)
    {
        int total = data.stream().mapToInt(Integer::intValue).sum();
        List<Integer> expanded = new ArrayList<>(total);

        System.out.println(data);
        //use -1 for "."

        int counter = 0;
        for (int i = 0; i < data.size(); i++)
        {
            int val = data.get(i);
            for (int j = 0; j < val; j++)
            {
                if (i % 2 == 0)
                {
                    expanded.add(counter);
                } else
                {
                    expanded.add(-1);
                }
            }
            if (i % 2 == 0) counter++;
        }
        return expanded;
    }

    //assume mutable array!
    public static List<Integer> rearange(List<Integer> exploded)
    {

        System.out.println(exploded.subList(exploded.size() - 20, exploded.size()));
        List<Integer> rearanged = new ArrayList<>(exploded);
        int index = 0;
        int reverseIndex = exploded.size() - 1;


        while (index != reverseIndex)
        {
            if (rearanged.get(index) == -1)
            {
                while (rearanged.get(reverseIndex) == -1)
                {
                    rearanged.set(reverseIndex, 0);
                    reverseIndex--;
                }
                rearanged.set(index, rearanged.get(reverseIndex));
                rearanged.set(reverseIndex, 0);
                reverseIndex--;
            }
            index++;
        }

        if (rearanged.contains(-1))
        {
            int i = rearanged.indexOf(-1);
            rearanged.set(i, 0);  //fix some off by 1 error
        }
        return rearanged;
    }

    public static List<Block> rearangeBlocks(List<Block> blocks)
    {
        System.out.println("lalalal");
        List<Block> rearanged = new ArrayList<>(blocks);
        for (int reverseIndex = blocks.size() - 1; reverseIndex > 0; reverseIndex--)
        {
            //what about size 0 blocks?
            int insertIndex = 0;
            Block newBlock = null;
            int blockSize = blocks.get(reverseIndex).size;
            for (int i = 0; i < rearanged.size() - 1; i++)
            {
                //but no further than current block!
                if (rearanged.get(i).equals(blocks.get(reverseIndex)))
                {
//                    System.out.println(blocks.get(reverseIndex) + " not Moved");
//                    System.out.println();
                    break; //we are at the same block now end!

                }

                int endIndex = (rearanged.get(i).startIndex + rearanged.get(i).size - 1) + blockSize;
//                System.out.println("endIndex: " + endIndex + ", blockSize: " + blockSize + "    " + rearanged.get(i));
//                System.out.println(rearanged.get(i + 1));
                if (endIndex < rearanged.get(i + 1).startIndex)
                {
                    insertIndex = i + 1;
                    newBlock = new Block(rearanged.get(i).startIndex + rearanged.get(i).size, blockSize, blocks.get(reverseIndex).value);
                  //  System.out.println("move block: " + newBlock);
                    break;
                }
            }

            if (newBlock != null)
            {
                rearanged.remove(blocks.get(reverseIndex));
                rearanged.add(insertIndex, newBlock);
            }
       //     System.out.println();

        }
        return rearanged;
    }

    private static long checksum(List<Integer> rearanged)
    {
        long checksum = 0;
        for (int i = 0; i < rearanged.size(); i++)
        {
            checksum += rearanged.get(i) * i;
        }
        return checksum;
    }


    private record Block(int startIndex, int size, int value)
    {
        public long checksum()
        {
            long checksum = 0;
            for (int i = startIndex; i < startIndex + size; i++)
            {
                checksum += (long) value * i;
            }
            return checksum;
        }
    }

}