package nl.mrtijmen.aoc2024;

import nl.mrtijmen.aoccommons.util.FileNameUtil;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;
import nl.mrtijmen.aoccommons.util.ParseUtil;
import nl.mrtijmen.aoccommons.util.grid.Coordinate;
import nl.mrtijmen.aoccommons.util.grid.Grid;

import java.util.ArrayList;
import java.util.List;

public class Day10
{
    private static Grid<Integer> topoMap;

    public static void main(String[] args) throws Exception
    {
        //  List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName(1));
        //List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName(2));
        List<String> data = FileReaderUtil.getStringList(FileNameUtil.dataFileName());

        topoMap = new Grid<>(ParseUtil.parseMatrix(data, ""));
        System.out.println(topoMap);

        var starts = findStartingPints();

        int result = 0;
        int result2 = 0;

        for (Coordinate start : starts)
        {
            List<Coordinate> trailHeads = new ArrayList<>();
            findPath(start, trailHeads);
            result += (int) trailHeads.stream().distinct().count();
            result2 += trailHeads.size();
        }


        long result1 = result;
        System.out.println("Result Part 1 --> " + result1);

        //long result2 = 0;
        System.out.println("Result Part 2 --> " + result2);
    }


    private static List<Coordinate> findStartingPints()
    {
        return topoMap.find(i -> i == 0);
    }

    public static void findPath(Coordinate start, List<Coordinate> trailheads)
    {
        int currentHeight = topoMap.getValue(start);

        if (currentHeight == 9)
        {
            trailheads.add(start);
            return;
        }

        for (Coordinate neighbour : start.findNeighbours())
        {

            if (topoMap.coordinateWithinRange(neighbour) && topoMap.getValue(neighbour) == (currentHeight + 1))
            {
                findPath(neighbour, trailheads);
            }
        }
    }

}