package nl.mrtijmen.aoc2024;

import nl.mrtijmen.aoccommons.util.FileNameUtil;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;
import nl.mrtijmen.aoccommons.util.ParseUtil;
import nl.mrtijmen.aoccommons.util.grid.Coordinate;
import nl.mrtijmen.aoccommons.util.grid.Grid;

import java.util.*;
import java.util.stream.Collectors;

public class Day08
{

    private static final Map<String, List<Coordinate>> antennaMap = new HashMap<>();
    private static final Map<String, Set<Coordinate>> antiNodeMap = new HashMap<>();
    private static final Map<String, Set<Coordinate>> extendedAntiNodeMap = new HashMap<>();


    private static Grid<String> grid;

    public static void main(String[] args) throws Exception
    {
        //  List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName());
        List<String> data = FileReaderUtil.getStringList(FileNameUtil.dataFileName());
        grid = new Grid<>(ParseUtil.parseStringMatrix(data, ""));
        findAntennas(grid);
        buildAntiNodeMap();
        // System.out.println(antiNodeMap);

        long result1 = countAntiNodes();
        System.out.println("Result Part 1 --> " + result1);

        buildExtendedAntiNodeMap();


        long result2 = countExtendedAntiNodes();
        System.out.println("Result Part 2 --> " + result2);
    }


    private static long countAntiNodes()
    {
        Set<Coordinate> antinodes = new HashSet<>();
        antiNodeMap.values().forEach(antinodes::addAll);

        return antinodes.size();
    }

    private static long countExtendedAntiNodes()
    {
        Set<Coordinate> antinodes = new HashSet<>();
        extendedAntiNodeMap.values().forEach(antinodes::addAll);

        return antinodes.size();
    }

    private static void findAntennas(Grid<String> grid)
    {

        grid.visitAllCoordinates((coordinate, value) -> {
            if (value.equals(".")) return;
            if (!antennaMap.containsKey(value))
            {
                antennaMap.put(value, new ArrayList<>());
            }
            antennaMap.get(value).add(coordinate);
        });
    }

    private static void buildAntiNodeMap()
    {
        antennaMap.forEach((key, antennas) -> antiNodeMap.put(key, findAntinodes(antennas, false))
        );

        //visualize
//        antiNodeMap.forEach((key, antennas) -> antennas.forEach(c -> grid.setValue(c, "#")));
//        System.out.println(grid);
    }

    private static void buildExtendedAntiNodeMap()
    {
        antennaMap.forEach((key, antennas) -> extendedAntiNodeMap.put(key, findAntinodes(antennas, true))
        );
        extendedAntiNodeMap.forEach((key, antennas) -> antennas.forEach(c -> grid.setValue(c, "#")));
        System.out.println(grid);
    }


    private static Set<Coordinate> findAntinodes(List<Coordinate> antennaList, boolean extended)
    {
        Set<Coordinate> antiNodes = new HashSet<>();
        for (int first = 0; first < antennaList.size() - 1; first++)
        {
            for (int second = first + 1; second < antennaList.size(); second++)
            {
                if (!extended)
                {
                    antiNodes.addAll(findAntinodePair(antennaList.get(first), antennaList.get(second)));
                } else
                {
                    antiNodes.addAll(findExtendedAntinodesList(antennaList.get(first), antennaList.get(second)));
                }
            }
        }

        return antiNodes.stream().filter(grid::coordinateWithinRange).collect(Collectors.toSet());
    }


    private static List<Coordinate> findAntinodePair(Coordinate first, Coordinate second)
    {
        //System.out.println(first + " " + second);
        Coordinate distance = second.minus(first);
        var antinodes = List.of(first.minus(distance), second.plus(distance));
        //System.out.println(antinodes);
        return antinodes;
    }

    private static List<Coordinate> findExtendedAntinodesList(Coordinate first, Coordinate second)
    {
        //System.out.println(first + " " + second);
        Coordinate distance = second.minus(first);
        List<Coordinate> antinodes = new ArrayList<>();
        //distance of 0
        antinodes.add(first);
        antinodes.add(second);

        var candidate = first.minus(distance);
        while (grid.coordinateWithinRange(candidate))
        {
            antinodes.add(candidate);
            candidate = candidate.minus(distance);
        }

        candidate = second.plus(distance);
        while (grid.coordinateWithinRange(candidate))
        {
            antinodes.add(candidate);
            candidate = candidate.plus(distance);
        }

//        var antinodes = List.of(first.minus(distance), second.plus(distance));
        //System.out.println(antinodes);
        return antinodes;
    }


}