package nl.mrtijmen.aoc2024.ropebridge;

import nl.mrtijmen.aoccommons.util.ParseUtil;

import java.util.List;

public record CalibrationEquation(Long target, List<Long> values)
{

    public static CalibrationEquation parse(String input)
    {
        String[] split = input.split(":");
        Long target = Long.parseLong(split[0]);
        List<Long> values = ParseUtil.lineToLongList(split[1], " ");

        return new CalibrationEquation(target, values);
    }

}
