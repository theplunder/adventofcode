package nl.mrtijmen.aoc2024.ropebridge;

import java.util.function.BiFunction;

public enum OperatorOperation
{
    ADD(Long::sum),
    MULTIPLY((a, b) -> a * b);

    private final BiFunction<Long, Long, Long> funcion;

    public Long apply(Long left, Long right)
    {
        return funcion.apply(left, right);
    }

    OperatorOperation(BiFunction<Long, Long, Long> funcion)
    {
        this.funcion = funcion;
    }
}
