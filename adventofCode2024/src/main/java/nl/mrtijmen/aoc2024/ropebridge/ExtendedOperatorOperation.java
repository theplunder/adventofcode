package nl.mrtijmen.aoc2024.ropebridge;

import java.util.function.BiFunction;

public enum ExtendedOperatorOperation
{
    ADD(Long::sum),
    MULTIPLY((a, b) -> a * b),
    CONCAT(ExtendedOperatorOperation::concat);

    private final BiFunction<Long, Long, Long> funcion;

    public Long apply(Long left, Long right)
    {
        return funcion.apply(left, right);
    }

    ExtendedOperatorOperation(BiFunction<Long, Long, Long> funcion)
    {
        this.funcion = funcion;
    }

    private static Long concat(Long a, Long b)
    {
        double power = Math.ceil(Math.log10(b+1));  // https://www.reddit.com/r/adventofcode/comments/1h8lz1a/2024_day_7_part_2_think_there_must_be_a_way_to/
        // stupid me! Log10(100) = 2, but I want it to be 3 !! whoops.
        Long result = Math.round(Math.pow(10, power)) * a + b;
        //System.out.println("contact " + a + " ||  " + b + " = " + result);
        return result;
    }

    private static Long slowConcat(Long a, Long b)
    {
        return Long.parseLong(a.toString() + b.toString());
    }


}
