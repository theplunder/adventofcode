package nl.mrtijmen.aoc2024;

import nl.mrtijmen.aoccommons.util.FileNameUtil;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;
import nl.mrtijmen.aoccommons.util.ParseUtil;
import nl.mrtijmen.aoccommons.util.grid.Coordinate;
import nl.mrtijmen.aoccommons.util.grid.Direction;
import nl.mrtijmen.aoccommons.util.grid.Grid;
import nl.mrtijmen.aoccommons.util.object.Pair;

import java.util.List;

import static nl.mrtijmen.aoccommons.util.grid.Direction.*;

public class Day04
{
    private static final List<Pair<Direction, Direction>> DIAGONALS =
            List.of(Pair.from(UP, RIGHT),
                    Pair.from(RIGHT, DOWN),
                    Pair.from(DOWN, LEFT),
                    Pair.from(LEFT, UP));

    private static final String target = "XMAS";

    public static void main(String[] args) throws Exception
    {
        //List<String> data = FileReaderUtil.getStringList(FileNameUtil.exampleDataFileName());
        List<String> data = FileReaderUtil.getStringList(FileNameUtil.dataFileName());

        Grid<String> crossword = new Grid<>(ParseUtil.parseStringMatrix(data, ""));
        System.out.println(crossword);

        long result1 = countXMAS(crossword);
        System.out.println("Result Part 1 --> " + result1);

        long result2 = countCROSSMAS(crossword);
        System.out.println("Result Part 2 --> " + result2);
    }


    private static int countXMAS(Grid<String> crossword)
    {
        var candidateStartCoorindates = crossword.find(s -> s.equals("X"));
        System.out.println("found " + candidateStartCoorindates.size() + " candidates");

        int count = 0;
        for (var candidate : candidateStartCoorindates)
        {
            for (Direction direction : Direction.values())
            {
                var nextPoint = candidate;
                System.out.println("start at: " + candidate);
                //START LOOP
                for (int step = 1; step < target.length(); step++)
                {
                    nextPoint = direction.travel(nextPoint);
                    System.out.println(nextPoint);
                    if (!crossword.coordinateWithinRange(nextPoint))
                    {
                        System.out.println("not within range");
                        break;
                    }
                    var val = crossword.getValue(nextPoint);
                    if (target.charAt(step) != val.charAt(0))
                    {
                        System.out.println("was not " + val.charAt(0));
                        break;
                    }
                    if (step == 3)
                    {
                        count++;
                        System.out.println("found XMAS at: " + candidate + " in DIR " + direction);
                    }
                }
            }
            //DIAGONALS, not very elegant
            for (Pair<Direction, Direction> direction : DIAGONALS)
            {
                var nextPoint = candidate;
                System.out.println("start at: " + candidate);
                //START LOOP
                for (int step = 1; step < target.length(); step++)
                {
                    nextPoint = direction.second().travel(direction.first().travel(nextPoint));

                    System.out.println(nextPoint);
                    if (!crossword.coordinateWithinRange(nextPoint))
                    {
                        System.out.println("not within range");
                        break;
                    }
                    var val = crossword.getValue(nextPoint);
                    if (target.charAt(step) != val.charAt(0))
                    {
                        System.out.println("was not " + val.charAt(0));
                        break;
                    }
                    if (step == 3)
                    {
                        count++;
                        System.out.println("found XMAS at: " + candidate + " in DIR " + direction);
                    }
                }
            }

        }
        return count;
    }

    private static long countCROSSMAS(Grid<String> crossword)
    {
        var centrePoints = crossword.find(s -> s.equals("A"));
        System.out.println("found " + centrePoints.size() + " candidates");
        int count = 0;

        for (Coordinate centre : centrePoints)
        {
            if (isCROSSMASS(crossword, centre))
            {
                count++;
                System.out.println("valid at " + centre);
            }
        }

        return count;
    }

    private static boolean isCROSSMASS(Grid<String> crossword, Coordinate centre)
    {
        var valid = List.of("MMSS", "SMMS", "SSMM", "MSSM");
        StringBuilder start = new StringBuilder();
        for (Pair<Direction, Direction> direction : DIAGONALS)
        {
            var point = direction.second().travel(direction.first().travel(centre));
            if (!crossword.coordinateWithinRange(point))
            {
                return false;
            }
            start.append(crossword.getValue(point));
        }

        return valid.contains(start.toString());

    }


}