package nl.mrtijmen.aoc2020.tickets;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public record Field(String name, Range range1, Range range2)
{
    private static final Pattern pattern = Pattern.compile("^(\\w+(?:\\s\\w+)*):\\s(\\d+)-(\\d+)\\sor\\s(\\d+)-(\\d+)$");

    //fieldsname: from1-until1 or from2-until2
    public static Field fromString(String line){
        line = line.strip();
        Matcher machter = pattern.matcher(line);
        if (!machter.find()){
            return null;
        }
        String name = machter.group(1);
        int from1 = Integer.parseInt(machter.group(2));
        int until1 = Integer.parseInt(machter.group(3));
        int from2 = Integer.parseInt(machter.group(4));
        int until2 = Integer.parseInt(machter.group(5));

        return new Field(name, new Range(from1, until1), new Range(from2, until2));
    }

    public boolean isInRange(int i){
        return range1.inRange(i) || range2.inRange(i);
    }
}
