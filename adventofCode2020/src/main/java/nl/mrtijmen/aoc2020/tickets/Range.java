package nl.mrtijmen.aoc2020.tickets;

//including
public record Range(int from, int until)
{

    public boolean inRange(int i){
        return i>=from && i <= until;
    }
}
