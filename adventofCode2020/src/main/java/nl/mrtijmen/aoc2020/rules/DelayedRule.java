package nl.mrtijmen.aoc2020.rules;

import java.util.Map;

public class DelayedRule implements Rule
{
    int rule1Id;
    int rule2Id;
    int rule3Id;
    int rule4Id;
    int rule5Id = -1;
    RuleType ruleType;

    public DelayedRule(RuleType ruleType, int rule1Id, int rule2Id, int rule3Id, int rule4Id)
    {
        this.ruleType = ruleType;
        this.rule1Id = rule1Id;
        this.rule2Id = rule2Id;
        this.rule3Id = rule3Id;
        this.rule4Id = rule4Id;
    }

    public DelayedRule(RuleType ruleType, int rule1Id, int rule2Id, int rule3Id, int rule4Id, int rule5Id)
    {
        this.ruleType = ruleType;
        this.rule1Id = rule1Id;
        this.rule2Id = rule2Id;
        this.rule3Id = rule3Id;
        this.rule4Id = rule4Id;
        this.rule5Id = rule5Id;
    }


    public Rule init(Map<Integer, DelayedRule> ruleStore, Map<Integer, Rule> initializedStore)
    {

        switch (ruleType)
        {
            case AGGREGATE:
            {
                Rule rule1 = initRule(ruleStore, initializedStore, rule1Id);
                Rule rule2 = initRule(ruleStore, initializedStore, rule2Id);
                return new AggregateRule(rule1, rule2);
            }
            case AGGREGATE3D:
            {
                Rule rule1 = initRule(ruleStore, initializedStore, rule1Id);
                Rule rule2 = initRule(ruleStore, initializedStore, rule2Id);
                Rule rule3 = initRule(ruleStore, initializedStore, rule3Id);
                return new TrippleAggregateRule(rule1, rule2, rule3);
            }
            case OR:
            {
                Rule rule1 = initRule(ruleStore, initializedStore, rule1Id);
                Rule rule2 = initRule(ruleStore, initializedStore, rule2Id);
                if (rule3Id != -1)
                {
                    if (rule4Id != -1)
                    {
                        if (rule5Id != -1) //special case: new Rule 11
                        {
                            Rule rule3 = initRule(ruleStore, initializedStore, rule3Id);
                            Rule rule5 = initRule(ruleStore, initializedStore, rule5Id);
                            return new Rule11(rule1, rule2, rule3, rule5);
                        }
                            else
                        {
                            Rule rule3 = initRule(ruleStore, initializedStore, rule3Id);
                            Rule rule4 = initRule(ruleStore, initializedStore, rule4Id);
                            return new OrRule(new AggregateRule(rule1, rule2), new AggregateRule(rule3, rule4));
                        }
                    }
                    else{//special case: new Rule 8
                       return new Rule8(rule1, rule2);
                    }
                }
                return new OrRule(rule1, rule2);
            }
            case FORWARD:{
                Rule rule1 = initRule(ruleStore, initializedStore, rule1Id);
                return new ForwardRule(rule1);
            }

        }
        throw new RuntimeException("should not happen");
    }

    private Rule initRule(Map<Integer, DelayedRule> ruleStore, Map<Integer, Rule> initializedStore, int ruleId)
    {
        if (!initializedStore.containsKey(ruleId))
        {
            DelayedRule delayedRule = ruleStore.get(ruleId);
            if (delayedRule==null){
                System.out.println( "rule: " + ruleId +" is null");
            }
            Rule rule = delayedRule.init(ruleStore, initializedStore);


            initializedStore.put(ruleId, rule);
        }
        return initializedStore.get(ruleId);
    }

    @Override
    public boolean matches(String s)
    {
        throw new UnsupportedOperationException("this is a trick");
    }

}
