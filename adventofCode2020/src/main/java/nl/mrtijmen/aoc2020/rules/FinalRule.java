package nl.mrtijmen.aoc2020.rules;

public record FinalRule(String finalChar) implements Rule
{
    @Override
    public boolean matches(String s)
    {
        return finalChar.equals(s);
    }
}
