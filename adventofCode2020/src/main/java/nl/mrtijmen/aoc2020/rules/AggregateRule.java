package nl.mrtijmen.aoc2020.rules;

import java.util.HashMap;
import java.util.Map;

public class AggregateRule implements Rule
{
    private final Rule rule1;
    private final Rule rule2;
    private final Map<String,Boolean> cache = new HashMap<>();

    public AggregateRule(Rule rule1, Rule rule2)
    {
        this.rule1 = rule1;
        this.rule2 = rule2;
    }

    @Override
    public boolean matches(String s)
    {
        if (cache.containsKey(s))
        {
            return cache.get(s);
        }
        for (int i = 1; i < s.length(); i++)
        {
            String part1 = s.substring(0, i);
            String part2 = s.substring(i);
            boolean canMatch = rule1.matches(part1) && rule2.matches(part2);
            if (canMatch)
            {
                cache.put(s,true);
                return true;
            }
        }
        cache.put(s,false);
        return false;
    }
}
