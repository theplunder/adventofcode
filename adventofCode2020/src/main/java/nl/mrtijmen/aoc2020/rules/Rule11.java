package nl.mrtijmen.aoc2020.rules;

public class Rule11 implements Rule
{
    private final Rule rule;

    public Rule11(Rule rule1, Rule rule2, Rule rule3, Rule rule4)
    {
        rule = new OrRule(new AggregateRule(rule1, rule2), new TrippleAggregateRule(rule3, this, rule4));
    }

    @Override
    public boolean matches(String s)
    {
        return rule.matches(s);
    }

}
