package nl.mrtijmen.aoc2020.rules;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class RuleFactory
{
    /*- eg:
    0: 4 1 5
    2: 4 4 | 5 5
    3: 4 5 | 5 4
    4: "a"
     */
    private static Map<Integer, DelayedRule> ruleStore = new HashMap<>();
    private static Map<Integer, Rule> initializedStore = new HashMap<>();

    public void fromLines(List<String> lines)
    {
        lines.forEach(this::fromLine);
    }

    private void fromLine(String line)
    {
        String[] lineSplit = line.split(" ");
        int id = Integer.parseInt(lineSplit[0].strip().replace(":", ""));

        System.out.println(line);
        if (line.contains("\""))
        {
            char c = lineSplit[1].strip().charAt(1);
            Rule finalRule = new FinalRule(String.valueOf(c));
            initializedStore.put(id, finalRule);
            return;
        }

        List<Integer> ruleIds = Arrays.stream(Arrays.copyOfRange(lineSplit, 1, lineSplit.length))
                                      .map(String::strip)
                                      .filter(s -> !s.equals("|"))
                                      .map(Integer::parseInt)
                                      .collect(Collectors
                                              .toList());
        if (ruleIds.size() == 1)
        {
            ruleStore.put(id, new DelayedRule(RuleType.FORWARD, ruleIds.get(0), -1, -1, -1));
            return;
        }


        if (line.contains("|"))
        {
            switch (ruleIds.size())
            {
                case 2:
                    ruleStore.put(id, new DelayedRule(RuleType.OR, ruleIds.get(0), ruleIds.get(1), -1, -1));
                    break;
                case 3:
                    ruleStore.put(id, new DelayedRule(RuleType.OR, ruleIds.get(0), ruleIds.get(1), ruleIds.get(2), -1));
                    break;
                case 4:
                    ruleStore.put(id, new DelayedRule(RuleType.OR, ruleIds.get(0), ruleIds.get(1), ruleIds.get(2), ruleIds
                            .get(3)));
                    break;
                case 5:
                    ruleStore.put(id, new DelayedRule(RuleType.OR, ruleIds.get(0), ruleIds.get(1), ruleIds.get(2), ruleIds
                            .get(3), ruleIds.get(4)));
                    break;
                default:
                    System.out.println("wrong size");
            }

        } else
        {
            if (ruleIds.size() == 2)
            {
                ruleStore.put(id, new DelayedRule(RuleType.AGGREGATE, ruleIds.get(0), ruleIds.get(1), -1, -1));
            }
            else
            {
                ruleStore.put(id, new DelayedRule(RuleType.AGGREGATE3D, ruleIds.get(0), ruleIds.get(1), ruleIds.get(2), -1));
            }
        }

    }

    public Rule getRuleById(int id)
    {
        if (!initializedStore.containsKey(id))
        {
            Rule rule = ruleStore.get(id).init(ruleStore, initializedStore);
            initializedStore.put(id, rule);
        }
        return initializedStore.get(id);
    }

    public void printStore()
    {
        System.out.println(initializedStore);
    }
}
