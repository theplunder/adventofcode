package nl.mrtijmen.aoc2020;

import nl.mrtijmen.aoc2020.circularList.CircularList;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class Day23
{

    public static void main(String[] args) throws Exception
    {
       // String numbers = "389125467";
        String numbers = "853192647";
        int steps = 10000000;
        //   part1(numbers, steps);

        part2(numbers, steps);

    }

    private static void part2(String numbers, int steps)
    {
        //  int maxval = 1000000;
        int maxval = 1000000;
        final int firstValue = Integer.parseInt(Character.toString(numbers.charAt(0)));
        CircularList list = new CircularList(firstValue);
        numbers = numbers.substring(1);
        numbers.chars().mapToObj(Character::toString).map(Integer::parseInt).forEach(list::addAfterCursorAndMove);
      //  System.out.println(list);
//
        IntStream.range(10, maxval + 1).forEach(list::addAfterCursorAndMove);

  //      System.out.println(list);


        int targetValue = firstValue;

        list.moveCursorToValue(targetValue);

        for (int step = 0; step < steps; step++)
        {

            if (step % 10000 == 0)
            {
                System.out.println("step: " + step);
            }

//            System.out.println();
//            System.out.println("---- Move " + (step + 1) + "----");
//            System.out.println("Current Cup " + list.getCursorValue());
//            System.out.println(list);

            targetValue = list.getCursorValue();

            //remove cups
            List<Integer> removedCups = new ArrayList<>();

            for (int i = 0; i < 3; i++)
            {
                removedCups.add(list.removeNext());
            }

            //      System.out.println("removed: " + removedCups);

            // destination cup
            int destination = targetValue;
            do
            {
                destination--;
                if (destination < 1)
                {
                    destination = maxval;
                }
            }
            while (removedCups.contains(destination));

            //          System.out.println("destination: " + destination);

            var stashCurrent = list.getCursor();
            list.moveCursorToValue(destination);

            for (int i = 0; i < 3; i++)
            {
                list.addAfterCursor(removedCups.get(i), i);
            }

            list.setCursor(stashCurrent.getNext());


        }


        System.out.println("done");

        list.moveCursorToValue(1);
        long val1 = list.moveCursor();
        long val2 = list.moveCursor();

        System.out.println(val1 + " * " + val2 + " = " + (val1 * val2));
        //
//            currentCupIndex = (list.indexOf(currentCup) + 1) % fullListSize;
//
//        }
//
//        int index = list.indexOf(1);
//        Integer first = list.get(index + 1);
//        Integer second = list.get(index + 2);
//
//        System.out.println(first);
//        System.out.println(second);
//        System.out.println(first * second);

    }


    private static void part1(String numbers, int steps)
    {
        int maxval = 9;
        List<Integer> list = new ArrayList<>();
        numbers.chars().mapToObj(Character::toString).map(Integer::parseInt).forEach(list::add);
        int fullListSize = list.size();

        //find first cup
        int currentCupIndex = 0;

        for (int step = 0; step < steps; step++)
        {
            System.out.println();
            System.out.println("---- Move " + (step + 1) + "----");
            System.out.println("cups: " + list);

            int currentCup = list.get(currentCupIndex);

            //remove cups
            List<Integer> removedCups = new ArrayList<>();
            int removeIndex = currentCupIndex + 1;
            for (int i = 0; i < 3; i++)
            {
                if (removeIndex >= list.size())
                {
                    removeIndex = 0;
                }
                removedCups.add(list.remove(removeIndex));
            }
            System.out.println("removed: " + removedCups);

            // destination cup
            int target = currentCup;
            do
            {
                target--;
                if (target < 1)
                {
                    target = maxval;
                }
            }
            while (removedCups.contains(target));
            System.out.println("target: " + target);

            int targetIndex = list.indexOf(target) + 1;
            if (targetIndex > list.size())
            {
                targetIndex = 1;
            }
            list.addAll(targetIndex, removedCups);

            currentCupIndex = (list.indexOf(currentCup) + 1) % fullListSize;

        }

        System.out.println(list);
        int index = list.indexOf(1);
        for (int i = 1; i < fullListSize; i++)
        {
            System.out.print(list.get((index + i) % fullListSize));

        }
    }
}
