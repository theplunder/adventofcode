package nl.mrtijmen.aoc2020.pocketDimension;

public enum Status
{
    INACTIVE('.'),
    ACTIVE('#');

    private final char token;
    Status(char token){
        this.token = token;
    }

    public static Status fromToken(char c){
        return switch (c)
                {
                    case '.' -> INACTIVE;
                    case '#' -> ACTIVE;
                    default -> null;
                };

    }

    @Override
    public String toString()
    {
        return String.valueOf(token);
    }
}
