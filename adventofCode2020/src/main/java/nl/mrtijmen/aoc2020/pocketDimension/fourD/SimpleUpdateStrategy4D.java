package nl.mrtijmen.aoc2020.pocketDimension.fourD;

import nl.mrtijmen.aoc2020.pocketDimension.Status;

import java.util.ArrayList;
import java.util.List;

public class SimpleUpdateStrategy4D
{

    private PocketDimension4D pocketDimension;
    private int stateChange;

    public PocketDimension4D apply(PocketDimension4D pocketDimension)
    {
        this.pocketDimension = pocketDimension;
        this.stateChange = 0;

        List<List<List<List<Status>>>> newGrid = new ArrayList<>(pocketDimension.getWRange()+2);

        for (int w = -1; w < pocketDimension.getWRange()+1; w++)
        {
            List<List<List<Status>>> cube = new ArrayList<>(pocketDimension.getZRange() + 2);
            for (int z = -1; z < pocketDimension.getZRange() + 1; z++)
            {
                List<List<Status>> slice = new ArrayList<>(pocketDimension.getYRange() + 2);
                for (int y = -1; y < pocketDimension.getYRange() + 1; y++)
                {
                    List<Status> gridLine = new ArrayList<>(pocketDimension.getXRange() + 2);
                    for (int x = -1; x < pocketDimension.getXRange() + 1; x++)
                    {
                        Status newStatus = findNewFeature(y, x, z, w);
                        gridLine.add(newStatus);
                    }

                    slice.add(gridLine);
                }
                cube.add(slice);
            }
            newGrid.add(cube);
        }

        return new PocketDimension4D(newGrid);
    }

    private Status findNewFeature(int y, int x, int z, int w)
    {
        return switch (pocketDimension.getFeatureAt(x, y, z, w))
                {
                    case INACTIVE -> {
                        if (countNeighboursWith(x, y, z,w, Status.ACTIVE) == 3)
                        {
                            stateChange++;
                            yield Status.ACTIVE;
                        }
                        yield Status.INACTIVE;
                    }
                    case ACTIVE -> {
                        int activeCount = countNeighboursWith(x, y, z,w, Status.ACTIVE);
                        if (activeCount == 2 || activeCount ==3)
                        {

                            yield Status.ACTIVE;
                        }
                        stateChange++;
                        yield Status.INACTIVE;
                    }
                };
    }

    public boolean isStateChanged()
    {
        return stateChange > 0;
    }

    public int countNeighboursWith(int x, int y, int z,int w, Status status)
    {
      int counter = 0;
        for (int iw = -1; iw <= 1; iw++)
        {
            for (int iz = -1; iz <= 1; iz++)
            {
                for (int iy = -1; iy <= 1; iy++)
                {
                    for (int ix = -1; ix <= 1; ix++)
                    {
                        if (ix == 0 && iy == 0 && iz == 0 && iw==0) continue;
                        if (pocketDimension.getFeatureAt(x + ix, y + iy, z + iz, w + iw).equals(status))
                        {
                            counter++;
                        }
                    }
                }
            }
        }
        return counter;
    }

}
