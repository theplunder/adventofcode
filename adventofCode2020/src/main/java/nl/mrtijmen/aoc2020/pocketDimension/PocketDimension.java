package nl.mrtijmen.aoc2020.pocketDimension;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class PocketDimension
{
    private final List<List<List<Status>>> grid;

    public PocketDimension(List<List<List<Status>>> grid){
        this.grid = grid;
    }

    public Status getFeatureAt(int x, int y, int z){
        try
        {
            return grid.get(z).get(y).get(x);
        } catch (Exception e) //for out of range
        {
            return Status.INACTIVE;
        }
    }

    public int getXRange(){
        return grid.get(0).get(0).size();
    }

    public int getYRange(){
        return grid.get(0).size();
    }

    public int getZRange(){
        return grid.size();
    }

    public int countPositionsWith(Status status){
        return (int) grid.stream()
                         .mapToLong(slice ->
                                 slice.stream().mapToLong(row ->
                                         row.stream().filter(Predicate.isEqual(status)).count()
                                    ).sum())
                         .sum();

    }


    public static PocketDimension fromTokenList(List<String> tokens){
        List<List<List<Status>>> grid = new ArrayList<>();
        List<List<Status>> slice = new ArrayList<>();
        for (String line: tokens){
            List<Status> gridLine = new ArrayList<>();
            for (char c: line.toCharArray())
            {
                gridLine.add(Status.fromToken(c));
            }
            slice.add(gridLine);
        }
        grid.add(slice);
        return new PocketDimension(grid);
    }


    private void printSlice (int i)
    {

        grid.get(i).stream().forEach(row -> {
            row.stream().forEach(System.out::print);
            System.out.println();
        });
    }


    public void printArea()
    {
        for(int  i = 0; i <grid.size(); i++){
            printSlice(i);
            System.out.println();
        }
    }
}
