package nl.mrtijmen.aoc2020.cardgame;

public class CardStack
{
    CardStackNode top;
    CardStackNode bottom;
    private int size = 0;

    public Card pop()
    {
        size--;
        Card card = top.getCard();
        top = top.getNext();
        if (size > 0)
        {
            top.setPrevious(null);
        } else
        {
            bottom = null;
        }
        return card;
    }

    public void addToBottom(Card card)
    {
        size++;
        //this logic should go to the Node?
        CardStackNode node = new CardStackNode(card);
        if (bottom != null)
        {
            bottom.setNext(node);
            node.setPrevious(bottom);
            bottom = node;
        } else
        {
            top = node;
            bottom = node;
        }

    }

    public void addToTop(Card card)
    {
        size++;
        CardStackNode node = new CardStackNode(card);
        if (top != null)
        {
            top.setPrevious(node);
            node.setNext(top);
            top = node;
        } else
        {
            top = node;
            bottom = node;
        }

    }

    public int getSize()
    {
        return size;
    }

    public boolean isEmpty()
    {
        if ((top == null && bottom != null)
                || (top != null && bottom == null))
        {
            throw new RuntimeException("empty and not empty!");
        }
        return size == 0;
    }

    public CardStack subStack(int cardsFromTop)
    {
        if (cardsFromTop > size)
        {
            throw new RuntimeException("subStack cannot be bigger than original");
        }

        CardStack cardStack = new CardStack();
        CardStackNode node = top;

        for (int counter = 0; counter < cardsFromTop; counter++)
        {
            cardStack.addToBottom(node.getCard());
            node = node.getNext();
        }
        return cardStack;
    }

    @Override
    public String toString()
    {
        String cards = "";
        CardStackNode node = top;

        for (int counter = 0; counter < size; counter++)
        {
            cards += node.getCard().toString();
            node = node.getNext();
        }

        return "CardStack{" +
                "size=" + size +
                ", cards= " + cards +
                '}';
    }

}
