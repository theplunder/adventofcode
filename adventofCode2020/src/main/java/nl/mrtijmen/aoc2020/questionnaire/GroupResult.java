package nl.mrtijmen.aoc2020.questionnaire;

import java.util.List;

public class GroupResult
{

    private final String groupResultString;
    private List<PersonResult> personResultList;
    public GroupResult(List<PersonResult> personResultList ){
        String result = personResultList.stream()
                                        .map(PersonResult::getResultString)
                                        .flatMapToInt(String::codePoints)
                                        .distinct()
                                        .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                                        .toString();
        groupResultString = result;
        this.personResultList = personResultList;

    }


    public boolean allHaveYesFor(char c)
    {
        for (PersonResult personResult : personResultList)
        {
            if (!personResult.hasYesFor(c))
            {
                return false;
            }
        }
        return true;
    }

    public String getGroupResultString()
    {
        return groupResultString;
    }
}
