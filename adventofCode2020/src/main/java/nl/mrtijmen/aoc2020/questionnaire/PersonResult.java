package nl.mrtijmen.aoc2020.questionnaire;

public class PersonResult
{

    private final String resultString;

    PersonResult(String resultString){
        this.resultString = resultString;
    }

    public boolean hasYesFor(char c){
        return resultString.contains(String.valueOf(c));
    }

    public String getResultString()
    {
        return resultString;
    }
}
