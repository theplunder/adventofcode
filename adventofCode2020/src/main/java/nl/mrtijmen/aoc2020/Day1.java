package nl.mrtijmen.aoc2020;

import nl.mrtijmen.aoccommons.util.FileReaderUtil;

import java.util.List;

public class Day1 {


	public static void main(String... args) throws Exception {
		List<Integer> list = FileReaderUtil.getNumberList("./src/main/resources/day1Input.csv");
		System.out.println(find2AddsUpTo2020(list));
		System.out.println(find3AddsUpTo2020(list));
	}

	private static Integer find2AddsUpTo2020(List<Integer> list) throws Exception {

		for (int i = 0; i < list.size(); i++) {
			for (int j = 0; j < list.size(); j++) {
				if (i == j) {
					continue;
				}
				if (list.get(i) + list.get(j) == 2020) {
					return list.get(i) * list.get(j);
				}
			}
		}
		throw new Exception("No Result Found");
	}

	private static Integer find3AddsUpTo2020(List<Integer> list) throws Exception {
		for (int i = 0; i < list.size(); i++) {
			for (int j = i; j < list.size(); j++) {
				if (i == j) {
					continue;
				}
				for (int k = j; k < list.size(); k++) {
					if (i == k || j == k) {
						continue;
					}
					if (list.get(i) + list.get(j) + list.get(k) == 2020) {
						return list.get(i) * list.get(j) * list.get(k);
					}
				}
			}
		}
		throw new Exception("No Result Found");
	}
}
