package nl.mrtijmen.aoc2020.bag;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BagRuleProcessor
{
    Pattern BAG_RULE_PATTERN = Pattern.compile("^(\\d+)\\s(\\w+)\\s(\\w+)\\s\\w+.*$");
    private final BagManager manager;

    public BagRuleProcessor(BagManager manager){
        this.manager=manager;
    }

    public void process(List<String> rules){
        for (String rule: rules){
            processRule(rule);
        }
    }

    private void processRule(String ruleLine){
        String[] bagWithRules = ruleLine.split("bags contain");
        //build nl.mrtijmen.aoc2020.bag
        String[] newBagString = bagWithRules[0].split(" ");
        Bag newBag = manager.getBag(newBagString[0].strip(), newBagString[1].strip());

        //process Rules
        String[] rules = bagWithRules[1].split(",");

        for(String rule:rules){
            rule = rule.strip();
            if (rule.equals("no other bags.")){
                continue;
            }
            Matcher matcher = BAG_RULE_PATTERN.matcher(rule);
            if (matcher.find())
            {
                int count = Integer.parseInt(matcher.group(1));
                String adjective = matcher.group(2);
                String color = matcher.group(3);

                Bag bag = manager.getBag(adjective, color);
                newBag.addContent(bag, count);
            }
        }
    }
}
