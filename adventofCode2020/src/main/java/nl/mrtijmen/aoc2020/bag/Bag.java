package nl.mrtijmen.aoc2020.bag;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Bag
{
    private final String adjective;
    private final String color;

    private final HashMap<Bag, Integer> content = new HashMap<>();

    public Bag(String adjective, String color)
    {
        this.adjective = adjective;
        this.color = color;
    }

    public int contains(Bag probeBag){
        int count = 0;
        if(content.containsKey(probeBag))
        {
            count += count(probeBag);
        }

        for(Bag bag:content.keySet()){
            count += bag.contains(probeBag);
        }
        return count;
    }

    public void addContent(Bag bag, int count){
        if (count == 0){
            return;
        }
        content.put(bag, count);
    }

    public int count(Bag bag){
        if (!content.containsKey(bag)){
            return 0;
        }
        return content.get(bag);
    }

    public long countBagsInside(){
        long count = 0;
        for(Map.Entry<Bag, Integer> bagEntry :content.entrySet()){
            count += bagEntry.getValue() +
                    bagEntry.getValue() * bagEntry.getKey().countBagsInside();
        }

        return count;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bag bag = (Bag) o;
        return Objects.equals(adjective, bag.adjective) && Objects.equals(color, bag.color);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(adjective, color);
    }

    @Override
    public String toString()
    {
        return adjective +" " + color + "bag";
    }

}
