package nl.mrtijmen.aoc2020.seating;

public interface SeatingStrategy
{
    SeatingArea apply(SeatingArea seatingArea);

    boolean isStateChanged();
}
