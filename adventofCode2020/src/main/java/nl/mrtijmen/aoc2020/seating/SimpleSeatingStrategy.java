package nl.mrtijmen.aoc2020.seating;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class SimpleSeatingStrategy implements SeatingStrategy
{

    private SeatingArea seatingArea;
    private int stateChange;

    public SeatingArea apply(SeatingArea seatingArea){
        this.seatingArea = seatingArea;
        this.stateChange = 0;

        List<List<Feature>> newGrid = new ArrayList<>();

        for (int y = 0; y < seatingArea.getYRange(); y++){
            List<Feature> gridLine = new ArrayList<>();
            for (int x = 0; x < seatingArea.getXRange(); x++)
            {
                Feature newFeature = findNewFeature(seatingArea, y, x);
                gridLine.add(newFeature);
            }

            newGrid.add(gridLine);


            }

        return new SeatingArea(newGrid);
    }

    private Feature findNewFeature(SeatingArea seatingArea, int y, int x)
    {
        return switch (seatingArea.getFeatureAt(x, y))
                {
                    case FLOOR -> Feature.FLOOR;
                    case EMPTY_SEAT -> { if (countAdjecentSeatsWith(x, y, Feature.OCCUPIED_SEAT) == 0){
                        stateChange++;
                        yield Feature.OCCUPIED_SEAT ;}
                        yield Feature.EMPTY_SEAT;
                    }
                    case OCCUPIED_SEAT -> { if (countAdjecentSeatsWith(x, y, Feature.OCCUPIED_SEAT) >= 4){
                        stateChange++;
                        yield Feature.EMPTY_SEAT ;}
                        yield Feature.OCCUPIED_SEAT;
                    }
                };
    }

    public boolean isStateChanged(){
        return stateChange > 0;
    }

    public int countAdjecentSeatsWith(int x, int y, Feature feature){

        List<Feature> collector = new ArrayList<>();
        collector.add(seatingArea.getFeatureAt(x - 1, y - 1));
        collector.add(seatingArea.getFeatureAt(x, y - 1));
        collector.add(seatingArea.getFeatureAt(x + 1, y - 1));
        collector.add(seatingArea.getFeatureAt(x - 1, y));
        collector.add(seatingArea.getFeatureAt(x + 1, y));
        collector.add(seatingArea.getFeatureAt(x - 1, y+1));
        collector.add(seatingArea.getFeatureAt(x , y+1));
        collector.add(seatingArea.getFeatureAt(x + 1, y+1));

        return (int) collector.stream().filter(Predicate.isEqual(feature)).count();
    }

}
