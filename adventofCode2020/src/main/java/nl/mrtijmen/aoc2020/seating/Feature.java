package nl.mrtijmen.aoc2020.seating;

public enum Feature
{
    FLOOR('.'),
    EMPTY_SEAT('L'),
    OCCUPIED_SEAT('#');

    private final char token;
    Feature(char token){
        this.token = token;
    }

    public static Feature fromToken(char c){
        return switch (c)
                {
                    case '.' -> FLOOR;
                    case 'L' -> EMPTY_SEAT;
                    case '#' -> OCCUPIED_SEAT;
                    default -> null;
                };

    }

    @Override
    public String toString()
    {
        return String.valueOf(token);
    }
}
