package nl.mrtijmen.aoc2020.ingredients;

import java.util.Objects;

public class Allergen
{
    private final String name;

    public Allergen(String name)
    {
        this.name = name;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Allergen allergen = (Allergen) o;
        return Objects.equals(name, allergen.name);
    }

    public String getName()
    {
        return name;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(name);
    }

    @Override
    public String toString()
    {
        return "Allergen{" +
                "name='" + name + '\'' +
                '}';
    }
}
