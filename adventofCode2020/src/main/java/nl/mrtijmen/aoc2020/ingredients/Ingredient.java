package nl.mrtijmen.aoc2020.ingredients;

import java.util.Objects;

public class Ingredient
{
    private final String name;
    private Allergen allergen = null;

    public Ingredient(String name)
    {
        this.name = name;
    }

    public Allergen getAllergen()
    {
        return allergen;
    }

    public void setAllergen(Allergen allergen)
    {
        if (allergen!= null){
            throw new RuntimeException(name + " already has an allergen!");
        }
        this.allergen = allergen;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ingredient that = (Ingredient) o;
        return name.equals(that.name);
    }

    public String getName()
    {
        return name;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(name);
    }

    @Override
    public String toString()
    {
        return "Ingredient{" +
                "name='" + name + '\'' +
                (allergen!=null?(", allergen=" + allergen):"")  +
                '}';
    }
}
