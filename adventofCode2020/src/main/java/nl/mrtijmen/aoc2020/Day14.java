package nl.mrtijmen.aoc2020;

import nl.mrtijmen.aoc2020.bitmasking.BitMasker;
import nl.mrtijmen.aoc2020.bitmasking.MemAdressBitMasker;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Day14
{

    public static void main(String[] args) throws Exception
    {
        List<String> list = FileReaderUtil.getStringList("./src/main/resources/day14Input.txt");

        solvePart1(list);

        //dont't run this with example 1
        solvePart2(list);
    }

    private static void solvePart2(List<String> list)
    {
        MemAdressBitMasker bitMasker = new MemAdressBitMasker("X".repeat(36));
        Map<Long, Long> values = new HashMap<>();
        for (String instruction : list)
        {
            String[] keyValue = instruction.split("=");
            if (keyValue[0].strip().equals("mask"))
            {
                bitMasker = new MemAdressBitMasker(keyValue[1].strip());

            } else
            {
                String indexStr = keyValue[0].substring(4, keyValue[0].length() - 2).strip();
                int index = Integer.parseInt(indexStr);
                long value = Long.parseLong(keyValue[1].strip());
                List<Long> newValue = bitMasker.apply(index);
                newValue.forEach(l -> values.put(l, value));
            }
        }
        long result = values.values().stream().mapToLong(i -> i).sum();
        System.out.println("part 2 -> " + result);
    }

    private static void solvePart1(List<String> list)
    {
        BitMasker bitMasker = new BitMasker("X".repeat(36));
        Map<Integer, Long> values = new HashMap<>();
        for (String instruction : list)
        {
            String[] keyValue = instruction.split("=");
            if (keyValue[0].strip().equals("mask"))
            {
                bitMasker = new BitMasker(keyValue[1].strip());

            } else
            {
                String indexStr = keyValue[0].substring(4, keyValue[0].length() - 2).strip();
                int index = Integer.parseInt(indexStr);
                long value = Long.parseLong(keyValue[1].strip());
                long newValue = bitMasker.apply(value);
                values.put(index, newValue);
            }
        }
        long result = values.values().stream().mapToLong(i -> i).sum();
        System.out.println("part 1 -> " + result);
    }
}
