package nl.mrtijmen.aoc2020;

import nl.mrtijmen.aoc2020.consolecompiler.Compiler;
import nl.mrtijmen.aoc2020.consolecompiler.Instructrion;
import nl.mrtijmen.aoc2020.consolecompiler.Processor;
import nl.mrtijmen.aoc2020.consolecompiler.Runner;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;

import java.util.List;

public class Day8
{
    public static void main(String[] args) throws Exception
    {
        List<String> list = FileReaderUtil.getStringList("./src/main/resources/day8Input.txt");
        List<Instructrion> byteCode = new Compiler().compile(list);

        Processor processor = new Processor();
        new Runner(processor).run(byteCode);

        System.out.println("memory before looping: " + processor.getMemory());

        for(int i = 0; i< byteCode.size(); i++){
            Processor processor2 = new Processor();
            if (new Runner(processor2).instructionSwap(i).run(byteCode)){
                System.out.println("memory on clean Exit: " + processor2.getMemory());
                break;
            }

        }


    }
}
