package nl.mrtijmen.aoc2020.circularList;

public class ListNode
{
    private final int value;

    private ListNode next;

    private ListNode(int value)
    {
        this.value = value;
    }

    protected ListNode(int value, ListNode next)
    {
        this.value = value;
        this.next = next;
    }

    //first node
    public static ListNode buildFirst(int value)
    {
        ListNode node = new ListNode(value);
        node.setNext(node);
        return node;
    }

    protected ListNode addNewNode(int value)
    {
        ListNode node = new ListNode(value);
        node.setNext(this.getNext());
        this.setNext(node);
        return node;
    }

    public ListNode getNext()
    {
        return next;
    }

    protected void setNext(ListNode next)
    {
        this.next = next;
    }

    public int getValue()
    {
        return value;
    }
}
