package nl.mrtijmen.aoc2020;

import nl.mrtijmen.aoc2020.image.*;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;

import java.util.*;
import java.util.stream.Collectors;

public class Day20
{

    static Set<Long> usedRepresentations = new TreeSet<>();
    static BorderRepresentation[][] tiledImage;
    static List<BorderRepresentation> borderRepresentations;
    static int dimensions;
    static int currentPosition = 0;
    static int iteration = 0;

    public static void main(String[] args) throws Exception
    {
        List<String> list = FileReaderUtil.getStringList("./src/main/resources/day20Input.txt");
        Map<Long, ImageTile> imageTiles = new HashMap<>();
        int previous = 0;
        for (int i = 0; i < list.size(); i++)
        {
            if (list.get(i).strip().isBlank() || i == list.size() - 1)
            {
                if (i == list.size() - 1)
                {
                    i = list.size();
                }
                ImageTile tile = ImageTile.fromTokenList(list.subList(previous, i));
                imageTiles.put(tile.getId(), tile);
                System.out.println("read tileId: " + tile.getId());

                tile.print();
                previous = i + 1;
            }
        }


        borderRepresentations = imageTiles.values()
                                          .stream()
                                          .map(ImageTile::getBorderRepresentation)
                                          .collect(Collectors.toList());

        dimensions = (int) Math.sqrt(borderRepresentations.size());

        tiledImage = new BorderRepresentation[dimensions][dimensions];

        if (!testNext())
        {
            System.out.println("unsolved!");
            return;
        }


        List<List<Pixel>> finalImage = new ArrayList<>();
        for (int i = 0; i < dimensions; i++)
        {
            List<List<Pixel>> lines = new ArrayList<>();
            for (int j = 0; j < dimensions; j++)
            {
                var tileBorder = tiledImage[i][j];
                var imageTile = imageTiles.get(tileBorder.getId());
                tileBorder.replayHistory(imageTiles.get(tileBorder.getId()));


                for (int y = 1; y < imageTile.getYRange() - 1; y++)
                {
                    if (j == 0)
                    {
                        lines.add(new ArrayList<>());
                    }
                    lines.get(y - 1).addAll(imageTile.getLine(y).subList(1, imageTile.getXRange() - 1));
                }
            }
            finalImage.addAll(lines);
        }


        var finalImagetile = new ImageTile(0L, finalImage);

        finalImagetile.print();

        findMonster(finalImagetile);

        finalImagetile.print();

        int count = 0;
        for (int i = 0; i < finalImagetile.getYRange(); i++){
            for(Pixel pixel: finalImagetile.getLine(i)){
                if (pixel.equals(Pixel.ONE)) count++;

            }

        }
        System.out.println("result part2 -> " + count);


//finalImagetile.print();
//        var imageTile = imageTiles.get(tileBorder.getId());
//
//        for (int y =0; y < imageTile.getYRange(); y ++){
//            lines.get(y).addAll(imageTile.getLine(y));

    }


    private static void findMonster(ImageTile tile){
        var monsterDetector = new MonsterDetector(tile);
        if(monsterDetector.detect()) return;
        tile.rotate();
        if(monsterDetector.detect()) return;
        tile.rotate();
        if(monsterDetector.detect()) return;
        tile.rotate();
        if(monsterDetector.detect()) return;
        tile.rotate();

        tile.flipVertical();
        if(monsterDetector.detect()) return;
        tile.rotate();
        if(monsterDetector.detect()) return;
        tile.rotate();
        if(monsterDetector.detect()) return;
        tile.rotate();
        if(monsterDetector.detect()) return;
        tile.rotate();


        tile.flipHorizontal();
        if(monsterDetector.detect()) return;
        tile.rotate();
        if(monsterDetector.detect()) return;
        tile.rotate();
        if(monsterDetector.detect()) return;
        tile.rotate();
        if(monsterDetector.detect()) return;
        tile.rotate();

        tile.flipVertical();
        if(monsterDetector.detect()) return;
        tile.rotate();
        if(monsterDetector.detect()) return;
        tile.rotate();
        if(monsterDetector.detect()) return;
        tile.rotate();
        if(monsterDetector.detect()) return;
        tile.rotate();

}


    private static boolean testNext()
    {
        int x = currentPosition % dimensions;
        int y = currentPosition / dimensions;
        for (BorderRepresentation orderRepresentation : borderRepresentations)
        {
            if (usedRepresentations.contains(orderRepresentation.getId()))
                continue;
            //    System.out.println(usedRepresentations);
            //  System.out.println(++iteration);
//            if (currentPosition >= (dimensions*dimensions-1))
//            {
//                System.out.println("almost!");
//                printConfiguration();
//
//                System.out.println();
//                System.out.println(usedRepresentations);
//
//                System.out.println(orderRepresentation);
////                System.out.println(nl.mrtijmen.aoc2020.image[dimensions-2][dimensions-2]);
////                System.out.println(nl.mrtijmen.aoc2020.image[dimensions-1][dimensions-3]);
//
//           //     System.out.println(nl.mrtijmen.aoc2020.image[1][0]);
//
//            }

            usedRepresentations.add(orderRepresentation.getId());
            tiledImage[y][x] = orderRepresentation;

            if (testOrientation(y, x)) return true;
            orderRepresentation.rotate();
            if (testOrientation(y, x)) return true;
            orderRepresentation.rotate();
            if (testOrientation(y, x)) return true;
            orderRepresentation.rotate();
            if (testOrientation(y, x)) return true;
            orderRepresentation.rotate();

            orderRepresentation.flipVertical();
            if (testOrientation(y, x)) return true;
            orderRepresentation.rotate();
            if (testOrientation(y, x)) return true;
            orderRepresentation.rotate();
            if (testOrientation(y, x)) return true;
            orderRepresentation.rotate();
            if (testOrientation(y, x)) return true;
            orderRepresentation.rotate();

            orderRepresentation.flipHorizontal();
            if (testOrientation(y, x)) return true;
            orderRepresentation.rotate();
            if (testOrientation(y, x)) return true;
            orderRepresentation.rotate();
            if (testOrientation(y, x)) return true;
            orderRepresentation.rotate();
            if (testOrientation(y, x)) return true;
            orderRepresentation.rotate();

            orderRepresentation.flipVertical();
            if (testOrientation(y, x)) return true;
            orderRepresentation.rotate();
            if (testOrientation(y, x)) return true;
            orderRepresentation.rotate();
            if (testOrientation(y, x)) return true;
            orderRepresentation.rotate();
            if (testOrientation(y, x)) return true;
            orderRepresentation.rotate();


            usedRepresentations.remove(orderRepresentation.getId());
            tiledImage[y][x] = null;
        }
        currentPosition--;
        return false;
    }

    private static boolean testOrientation(int y, int x)
    {

        boolean result = test(tiledImage);
        if (!result)
        {
            return false;
        } else
        {
            if ((x == dimensions - 1) && (y == dimensions - 1))
            {
                System.out.println("Solved!");

                printConfiguration();

//                borderRepresentations.forEach(System.out::println);

                System.out.println("answer part1 -> " + (tiledImage[0][0].getId() * tiledImage[0][dimensions - 1].getId() * tiledImage[dimensions - 1][0]
                        .getId() * tiledImage[dimensions - 1][dimensions - 1].getId()));
                return true;
            }
            currentPosition++;
            return testNext();
        }
    }

    private static void printConfiguration()
    {
        int x = dimensions - 1;
        int y = dimensions - 1;
        for (int i = 0; i <= x; i++)
        {
            for (int j = 0; j <= y; j++)
            {
                String id;
                if (tiledImage[i][j] == null)
                {
                    id = null;
                } else
                {
                    id = String.valueOf(tiledImage[i][j].getId());
                }

                System.out.print(id + " ");
            }
            System.out.println();
        }
    }


    private static boolean test(BorderRepresentation[][] image)
    {
        for (int x = 0; x < image.length; x++)
        {
            for (int y = 0; y < image.length; y++)
            {
                if (image[y][x] == null) continue;

                //test top
                if (y != 0)
                {
                    var current = image[y][x];
                    var next = image[y - 1][x];
                    if (current.getHashForSide(Side.TOP) != next.getHashForSide(Side.BOTTOM))
                        return false;
                }

                //test left
                if (x != 0)
                {
                    var current = image[y][x];
                    var previous = image[y][x - 1];
                    if (current.getHashForSide(Side.LEFT) != previous.getHashForSide(Side.RIGHT))
                        return false;
                }
            }
        }
        return true;
    }

}
