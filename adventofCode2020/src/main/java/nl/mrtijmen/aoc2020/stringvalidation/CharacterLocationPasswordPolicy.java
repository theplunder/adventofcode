package nl.mrtijmen.aoc2020.stringvalidation;

import java.util.regex.Pattern;

public class CharacterLocationPasswordPolicy implements PasswordPolicy {


	/* For example, suppose you have the following list:
	 * Each policy actually describes two positions in the password, where 1 means the first character, 2 means the
	 * second character, and so on. (Be careful; Toboggan Corporate Policies have no concept of "index zero"!) Exactly
	 *  one of these positions must contain the given letter. Other occurrences of the letter are irrelevant for the
	 * purposes of policy enforcement.
	 *
	 * Given the same example list from above:
	 *
	 * 1-3 a: abcde is valid: position 1 contains a and position 3 does not.
	 * 1-3 b: cdefg is invalid: neither position 1 nor position 3 contains b.
	 * 2-9 c: ccccccccc is invalid: both position 2 and position 9 contain c.
	 */

	private final int should;
	private final int elseShould;
	private final char character;

	CharacterLocationPasswordPolicy(int should, int elseShould, char character) {
		this.should = should;
		this.elseShould = elseShould;
		this.character = character;
	}

	//eg: "2-12 a"
	public static CharacterLocationPasswordPolicy fromPolicyString(String policy) {
		policy = policy.strip();

		String regex = "(\\d*)-(\\d*)\\s(\\w)";
		Pattern pattern = Pattern.compile(regex);

		var matcher = pattern.matcher(policy);

		if (!matcher.find()) {
			throw new RuntimeException(String.format("malformed policy: %s", policy));
		}

		int should = Integer.parseInt(matcher.group(1)) - 1;
		int elseShould = Integer.parseInt(matcher.group(2)) - 1;
		char character = matcher.group(3).charAt(0);

		return new CharacterLocationPasswordPolicy(should, elseShould, character);
	}

	@Override
	public boolean test(String target) {
		if (target.length() < should || target.length() < elseShould) {
			return false;
		}
		boolean first = target.charAt(should) == character;
		boolean second = target.charAt(elseShould) == character;

		if (first && second){
			return false;
		}
		return first || second;
	}


	@Override
	public String toString() {
		return "PasswordPolicy{" + "should=" + should + ", shouldNot=" + elseShould + ", character=" + character + '}';
	}
}
