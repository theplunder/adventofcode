package nl.mrtijmen.aoc2020.stringvalidation;

public interface PasswordPolicy {
	boolean test(String target);
}
