package nl.mrtijmen.aoc2020.boarding;

import java.util.List;

public class BoardingPass
{

    private static final List<Integer> decodeKey = List.of(64,32,16,8,4,2,1);
    private final String boardingCode;
    private int seatId;
    private int rowNr;
    private int seatNr;

    public BoardingPass(String boardingCode)
    {
        this.boardingCode = boardingCode;
        processBoardingCode();
    }


    private void processBoardingCode()
    {
        int row = 0;
        for(int i = 0; i <= 6; i++){
            if (boardingCode.charAt(i) == 'B')
            {
                row += decodeKey.get(i);
            }
        }
        rowNr = row;

        int seat = 0;
        for(int i = 0; i <=2; i++){
            if (boardingCode.charAt(i+7) == 'R')
            {
                seat += decodeKey.get(i+4);
            }
        }
        seatNr = seat;
        seatId = rowNr *8 + seatNr;

    }

    public String getBoardingCode()
    {
        return boardingCode;
    }

    public int getSeatId()
    {
        return seatId;
    }

    public void setSeatId(int seatId)
    {
        this.seatId = seatId;
    }

    public int getRowNr()
    {
        return rowNr;
    }

    public void setRowNr(int rowNr)
    {
        this.rowNr = rowNr;
    }

    public int getSeatNr()
    {
        return seatNr;
    }

    public void setSeatNr(int seatNr)
    {
        this.seatNr = seatNr;
    }

    @Override
    public String toString()
    {
        return "BoardingPass{" +
                "boardingCode='" + boardingCode + '\'' +
                ", seatId=" + seatId +
                ", rowNr=" + rowNr +
                ", seatNr=" + seatNr +
                '}';
    }
}
