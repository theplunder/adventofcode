package nl.mrtijmen.aoc2020.hexagon;

import java.util.Arrays;

public enum HexagonDirection
{
    EAST("e"),
    SOUTH_EAST("se"),
    SOUTH_WEST("sw"),
    WEST("w"),
    NORTH_WEST("nw"),
    NORTH_EAST("ne");

    private final String code;

    HexagonDirection(String code)
    {
        this.code = code;
    }


    public static HexagonDirection fromCode(String code)
    {
        return Arrays.stream(HexagonDirection.values())
                     .filter(h -> h.code.equals(code))
                     .findAny()
                     .orElseThrow(() -> new IllegalArgumentException("cant find direction with code: " + code));
    }
}
