package nl.mrtijmen.aoc2020.hexagon;

public record HexagonCoordinate(int x, int y)
{

    /*
      use a diagonal x = 0 line.
     */

    public HexagonCoordinate move(HexagonDirection direction)
    {
        return switch (direction)
                {
                    case EAST -> new HexagonCoordinate(x + 1, y);
                    case SOUTH_EAST -> new HexagonCoordinate(x + 1, y - 1);
                    case SOUTH_WEST -> new HexagonCoordinate(x, y - 1);
                    case WEST -> new HexagonCoordinate(x - 1, y);
                    case NORTH_WEST -> new HexagonCoordinate(x - 1, y + 1);
                    case NORTH_EAST -> new HexagonCoordinate(x, y + 1);
                };
    }
}
