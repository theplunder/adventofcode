package nl.mrtijmen.aoc2020.location;

public record Position(int x, int y) {

    public int getManhattanDistance(){
        return Math.abs(x)+ Math.abs(y);
    }
}
