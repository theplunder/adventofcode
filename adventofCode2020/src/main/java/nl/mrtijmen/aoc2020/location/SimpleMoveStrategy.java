package nl.mrtijmen.aoc2020.location;

public class SimpleMoveStrategy implements MoveStrategy {

	private final int shiftX;
	private final int shiftY;

	public SimpleMoveStrategy(int shiftX, int shiftY) {
		this.shiftX = shiftX;
		this.shiftY = shiftY;
	}

	public Position move(Position currentPosition) {
		return new Position(currentPosition.x() + shiftX,
							currentPosition.y() + shiftY);
	}

}
