package nl.mrtijmen.aoc2020.location;

public interface MoveStrategy {

	Position move(Position position);
}
