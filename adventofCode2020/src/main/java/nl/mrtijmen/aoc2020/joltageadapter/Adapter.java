package nl.mrtijmen.aoc2020.joltageadapter;

public record Adapter(int joltage) implements Comparable<Adapter>
{
    public boolean canTake(int otherJoltage){
        return otherJoltage > (joltage-3) && otherJoltage < joltage;
    }

    @Override
    public int compareTo(Adapter o)
    {
        return Integer.compare(joltage,o.joltage);
    }
}
