package nl.mrtijmen.aoc2020.consolecompiler;

import java.util.HashSet;
import java.util.Set;

public class LoopDetector
{
    private Set<Integer> instructionPoints = new HashSet<>();

    public boolean isLoop(int codePoint){
        if(instructionPoints.contains(codePoint)){
            return true;
        }
        instructionPoints.add(codePoint);
        return false;

    }

}
