package nl.mrtijmen.aoc2020.consolecompiler;

import java.util.List;

public class Runner
{
    public final Processor processor;
    public int instructionSwap=-1;

    public Runner(Processor processor){
        this.processor = processor;
    }

    public Runner instructionSwap(int i){
        instructionSwap=i;
        return this;
    }


    public boolean run(List<Instructrion> instructrions){
        int pointer = 0;
        int counter = 0;
        LoopDetector loopDetector = new LoopDetector();
        while (true){
            if (loopDetector.isLoop(pointer)){
               return false;
            }

            Instructrion instructrion = instructrions.get(pointer);
            if(instructionSwap!= -1 && instructionSwap == pointer){
                instructrion = swap(instructrion);
            }
            pointer+=processor.process(instructrion);

            if (pointer == instructrions.size()){
                return true;
            }

            if (pointer >= instructrions.size() || pointer < 0){
                return false;
            }

            if (counter++ > instructrions.size()){
              throw new RuntimeException("oops!");
            }
        }
    }

    private Instructrion swap(Instructrion instructrion)
    {
        return switch (instructrion.operation()){
            case ACCUMULATE -> instructrion;
            case JUMP -> new Instructrion(Operation.NO_OPERATION, instructrion.value());
            case NO_OPERATION -> new Instructrion(Operation.JUMP, instructrion.value());
        };
    }


}
