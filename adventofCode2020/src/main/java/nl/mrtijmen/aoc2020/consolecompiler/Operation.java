package nl.mrtijmen.aoc2020.consolecompiler;

public enum Operation
{
    ACCUMULATE("acc"),
    JUMP("jmp"),
    NO_OPERATION("nop");

    private final String code;

    Operation(String code)
    {
        this.code = code;
    }

    public static Operation fromCode(String code)
    {
        return switch (code)
                {
                    case "acc" -> ACCUMULATE;
                    case "jmp" -> JUMP;
                    case "nop" -> NO_OPERATION;
                    default -> null;
                };
    }
}
