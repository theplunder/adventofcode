package nl.mrtijmen.aoc2020;

import nl.mrtijmen.aoc2020.stringvalidation.CharacterCountRangePasswordPolicy;
import nl.mrtijmen.aoc2020.stringvalidation.CharacterLocationPasswordPolicy;

import java.util.List;
import java.util.stream.Collectors;

import static nl.mrtijmen.aoccommons.util.FileReaderUtil.getStringList;

public class Day2 {

	public static void main(String... args) throws Exception {
		List<String> list = getStringList("./src/main/resources/day2Input.txt");

		List<String[]> tuples = list.stream().map(string -> string.split(":")).collect(Collectors.toList());

		int counter = validRangeCountPasswords(tuples);
		System.out.println("# valid passwords: " + counter);

		int counter2 = validLocationCountPasswords(tuples);
		System.out.println("# valid passwords: " + counter2);

	}

	private static int validLocationCountPasswords(List<String[]> tuples) {
		int counter = 0;
		for (String[] strings: tuples){
			if (CharacterLocationPasswordPolicy.fromPolicyString(strings[0]).test(strings[1])){
				counter++;
			}
		}
		return counter;
	}

	private static int validRangeCountPasswords(List<String[]> tuples) {
		int counter = 0;
		for (String[] strings: tuples){
			if (CharacterCountRangePasswordPolicy.fromPolicyString(strings[0]).test(strings[1].strip())){
				counter++;
			}
		}
		return counter;
	}

}
