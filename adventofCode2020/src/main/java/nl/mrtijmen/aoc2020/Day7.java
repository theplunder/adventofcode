package nl.mrtijmen.aoc2020;

import nl.mrtijmen.aoc2020.bag.Bag;
import nl.mrtijmen.aoc2020.bag.BagManager;
import nl.mrtijmen.aoc2020.bag.BagRuleProcessor;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;

import java.util.List;
import java.util.Set;

public class Day7
{
    public static void main(String[] args) throws Exception
    {
        List<String> list = FileReaderUtil.getStringList("./src/main/resources/day7Input.txt");

        BagManager manager = new BagManager();
        BagRuleProcessor bagRuleProcessor = new BagRuleProcessor(manager);
        bagRuleProcessor.process(list);

        Bag searchBag = manager.getBag("shiny", "gold");
        Set<Bag> bags = manager.getBags();

        int counter = 0;
        for (Bag bag : bags)
        {
            if (bag.contains(searchBag) != 0)
            {
                counter++;
            }
        }

        System.out.println("# bags can eventually contain " + searchBag + " : " + counter);

        long counter2 = searchBag.countBagsInside();

        System.out.println("# bags contained in " + searchBag + " : " + counter2);
    }
}
