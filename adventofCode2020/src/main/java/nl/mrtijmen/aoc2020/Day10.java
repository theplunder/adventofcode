package nl.mrtijmen.aoc2020;

import nl.mrtijmen.aoc2020.joltageadapter.Adapter;
import nl.mrtijmen.aoc2020.joltageadapter.Tribonacci;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Day10
{

    public static void main(String[] args) throws Exception
    {
        List<Integer> list = FileReaderUtil.getNumberList("./src/main/resources/day10Input.txt");

        List<Adapter> adapterList = new ArrayList<>();
        adapterList.add(new Adapter(0));
        adapterList.addAll(list.stream().map(Adapter::new).sorted().collect(Collectors.toList()));

        int highest = adapterList.get(adapterList.size() - 1).joltage();
        //my device!
        adapterList.add(new Adapter(highest + 3));

        System.out.println(adapterList);

        int counters[] =new int[3];
   //     counters[adapterList.get(0).joltage()-1]++;

        for (int i = 0; i < adapterList.size() - 1; i++)
        {
            int diff = adapterList.get(i + 1).joltage() - adapterList.get(i).joltage();
            counters[diff-1]++;
        }

        System.out.println("diff 1 = " + counters[0]);
        System.out.println("diff 2 = " + counters[1]);
        System.out.println("diff 3 = " + counters[2]);
        System.out.println("answer ->" + counters[0] * counters[2]);

        //arrangements
        Tribonacci tribonacci = new Tribonacci();
        //count chains? diff3 == end of chain

        BigInteger totCombination = BigInteger.valueOf(1);
        int subgroupCount = 1;
        for (int i = 0; i < adapterList.size() - 1; i++)
        {

            int diff = adapterList.get(i + 1).joltage() - adapterList.get(i).joltage();
            if (diff ==1){
                subgroupCount++;
            }
            if (diff ==3){
                int cominations = tribonacci.getForN(subgroupCount);
                totCombination = totCombination.multiply(BigInteger.valueOf(cominations));
                subgroupCount=1;
            }
        }

        System.out.println("total number of combinations: " + totCombination);
    }

}
