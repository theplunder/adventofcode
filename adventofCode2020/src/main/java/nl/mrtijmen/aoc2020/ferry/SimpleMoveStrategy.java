package nl.mrtijmen.aoc2020.ferry;

import nl.mrtijmen.aoc2020.location.Position;

public class SimpleMoveStrategy implements ShipMoveStrategy
{

    public Ship.Orientation move(Instruction instruction, Ship.Orientation orientation){
        Direction direction = orientation.direction();
        Position position =
                switch (instruction.instructionType()){
                    case NORTH_DIR -> Direction.NORTH.move(orientation.position(), instruction.value());
                    case EAST_DIR -> Direction.EAST.move(orientation.position(), instruction.value());
                    case SOUTH_DIR ->  Direction.SOUTH.move(orientation.position(), instruction.value());
                    case WEST_DIR -> Direction.WEST.move(orientation.position(), instruction.value());
                    case ROTATE_RIGHT -> {
                        direction = orientation.direction().rotate(instruction.value());
                        yield orientation.position();
                    }
                    case ROTATE_LEFT -> {
                        direction = orientation.direction().rotate(-1*instruction.value());
                        yield orientation.position();
                    }
                    case FORWARD -> orientation.direction().move(orientation.position(), instruction.value());
                };
        return new Ship.Orientation(position, direction);

    }
}
