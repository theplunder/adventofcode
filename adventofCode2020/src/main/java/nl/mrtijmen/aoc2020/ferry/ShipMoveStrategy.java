package nl.mrtijmen.aoc2020.ferry;

public interface ShipMoveStrategy
{
   Ship.Orientation move(Instruction instruction, Ship.Orientation orientation);
}
