package nl.mrtijmen.aoc2020.ferry;

public record Instruction(InstructionType instructionType, int value)
{
    public Instruction(char token, int value)
    {
        this(InstructionType.fromToken(token), value);
    }

    public static Instruction fromString(String line)
    {
        line = line.strip();
        char token = line.charAt(0);
        int value = Integer.parseInt(line.substring(1));

        return new Instruction(token, value);
    }
}
