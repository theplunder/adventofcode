package nl.mrtijmen.aoc2020.ferry;

import nl.mrtijmen.aoc2020.location.Position;

public class Ship
{
   public static record Orientation(Position position, Direction direction){}

    private Orientation orientation;
    private final ShipMoveStrategy moveStrategy;

    public Ship(Direction startingDirection, ShipMoveStrategy moveStrategy){
        Position position = new Position(0,0);
        Direction direction = startingDirection;
        orientation = new Orientation(position, direction);
        this.moveStrategy = moveStrategy;
    }

    public Orientation move(Instruction instruction){
        this.orientation = moveStrategy.move(instruction, orientation);
        return orientation;
    }

    public Position getPosition(){
        return orientation.position();
    }

}
