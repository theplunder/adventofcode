package nl.mrtijmen.aoc2020;

import java.util.*;
import java.util.stream.Collectors;

public class Day15
{

    public static void main(String[] args)
    {
//          List<Integer> list = Arrays.stream("0,3,6".split(",")).map(Integer::parseInt).collect(Collectors.toList());
        //    List<Integer> list = Arrays.stream("1,2,3".split(",")).map(Integer::parseInt).collect(Collectors.toList());
        List<Integer> list = Arrays.stream("6,19,0,5,7,13,1".split(","))
                                   .map(Integer::parseInt)
                                   .collect(Collectors.toList());

        solvePart1(list, 2020);

        solvePart2(list, 30000000);

    }


    private static void solvePart1(List<Integer> startingNumbers, int targetnr)
    {
        startingNumbers = new ArrayList<>(startingNumbers);
        int value = startingNumbers.get(startingNumbers.size() - 1);

        for (int i = startingNumbers.size() - 1; i <= targetnr; i++)
        {
            value = findLastOccurence(startingNumbers, value);
            startingNumbers.add(value);
        }

        System.out.println("result -> " + startingNumbers.get(targetnr - 1));
    }

    private static int findLastOccurence(List<Integer> list, int value)
    {

        for (int i = list.size() - 2; i >= 0; i--)
        {
            if (list.get(i) == value)
            {
                return list.size() - i - 1;
            }

        }
        return 0;

    }


    private static void solvePart2(List<Integer> startingNumbers, int targetnr)
    {
        startingNumbers = new ArrayList<>(startingNumbers);

        Map<Integer, Integer> numberIndex = new HashMap<>();
        for (int i = 0; i < startingNumbers.size() - 1; i++)
        {
            numberIndex.put(startingNumbers.get(i), i);
        }

        int lastValue = startingNumbers.get(startingNumbers.size() - 1);

        for (int j = startingNumbers.size() - 1; j < targetnr-1; j++)
        {
            if (numberIndex.containsKey(lastValue))
            {
                int lastIndex = numberIndex.get(lastValue);
                numberIndex.put(lastValue, j);
                lastValue = j - lastIndex;
            }
            else{
                numberIndex.put(lastValue, j);
                lastValue = 0;
            }
        }
        System.out.println("result -> " + lastValue);
    }

}
