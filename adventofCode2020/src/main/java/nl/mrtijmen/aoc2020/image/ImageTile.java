package nl.mrtijmen.aoc2020.image;

import nl.mrtijmen.aoccommons.util.ArrayUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ImageTile
{
    private final long id;
    private List<List<Pixel>> image;

    private static final List<Integer> mask =  IntStream.range(0, 10)
                                                        .map(i -> (int) Math.pow(2, i))
                                                        .boxed()
                                                        .collect(Collectors
                                                                .toList());

    public ImageTile(long id, List<List<Pixel>> image)
    {
        this.id = id;
        this.image = image;
    }

    public Pixel getPixelAt(int x, int y){
        return image.get(y).get(x);
    }

    void setPixelAt(int x, int y, Pixel pixel)
    {
        image.get(y).set(x, pixel);
    }

    public List<Pixel> getLine(int y){
        return Collections.unmodifiableList(image.get(y));
    }

    public int getXRange(){
        return image.get(0).size();
    }

    public int getYRange(){
        return image.size();
    }

    public BorderRepresentation getBorderRepresentation(){

        List<Pixel> right = new ArrayList<>();
        List<Pixel> left = new ArrayList<>();

        for (int i = 0; i <image.get(0).size(); i++){
            right.add(image.get(i).get(image.get(0).size()-1));
            left.add(image.get(i).get(0));
        }

        List<List<Pixel>> borders =  new ArrayList<>();
        borders.add(image.get(0));
        borders.add(right);
        borders.add(image.get(image.size()-1));
        borders.add(left);
        return new BorderRepresentation(id, borders, this);
    }


    public List<Integer> borderHashList(){

        Integer[] hashArray = new Integer[4];
        int hash0 = 0;
        int hash2 = 0;
        //horizonal
        for(int i = 0; i< getXRange(); i++){
            hash0 += mask.get(i)*image.get(0).get(i).getValue();
            hash2 += mask.get(i)*image.get(getYRange()-1).get(i).getValue();
        }
        hashArray[0] =hash0;
        //TODO: think; multiply with -1?
        hashArray[2]= hash2;

        //vertical
        int hash1 = 0;
        int hash3 = 0;
        for(int i = 0; i< getYRange(); i++){
            hash1 += mask.get(i)*image.get(i).get(0).getValue();
            hash3 += mask.get(i)*image.get(i).get(getXRange()-1).getValue();
        }
        hashArray[1] =hash1;
        //TODO: think; multiply with -1?
        hashArray[3]= hash3;
        List<Integer> mutableList = new ArrayList<>();
        mutableList.addAll(Arrays.asList(hashArray));
        return mutableList;
    }

    public long getId()
    {
        return id;
    }

    public void rotate(){
        image = ArrayUtil.rotate(image);
    }

    //flip along horizontal axis
    public void flipHorizontal(){
        image = ArrayUtil.flipHorizontal(image);
    }

    //flip along vertical axis
    public void flipVertical(){
        image = ArrayUtil.flipVertical(image);
    }

    public static ImageTile fromTokenList(List<String> tokens){

        String idLine = tokens.get(0);
        int id = Integer.parseInt(idLine.substring(5,idLine.length()-1).strip());

        List<List<Pixel>> grid = new ArrayList<>();

        for (String line: tokens.subList(1,tokens.size())){
            List<Pixel> gridLine = new ArrayList<>();
            for (char c: line.toCharArray())
            {
                gridLine.add(Pixel.fromToken(c));
            }
            grid.add(gridLine);
        }

        return new ImageTile(id, grid);
    }

    public void print()
    {
        System.out.println("-- id: " + id + "--");
        image.forEach(row -> {
            row.forEach(System.out::print);
            System.out.println();
        });
        System.out.println();
    }

}
