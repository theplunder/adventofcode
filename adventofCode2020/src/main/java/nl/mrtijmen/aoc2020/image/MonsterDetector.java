package nl.mrtijmen.aoc2020.image;

import java.util.List;

public class MonsterDetector
{
    List<List<Integer>> monsterPoints = List.of(
            List.of(18),
            List.of(0, 5, 6, 11, 12, 17, 18, 19),
            List.of(1, 4, 7, 10, 13, 16)
    );

    private int monsterLength = 20;
    private ImageTile image;

    /*
                  #
#    ##    ##    ###
 #  #  #  #  #  #
012345678901234567890
     */

    public MonsterDetector(ImageTile image)
    {
        this.image = image;
    }

    public boolean detect()
    {
        boolean result = false;
        for (int y = 0; y < image.getYRange()-2; y++)
        {
            for (int x = 0; x <= image.getXRange() - 20; x++)
            {
                boolean subResult = detectMoster(x,y);
                if (subResult){
                    result = true;
                }
            }

        }
        return result;

    }

    private boolean detectMoster(int x, int y)
    {
        for (int i = 0; i < monsterPoints.size(); i++)
        {
            for (int monsterX : monsterPoints.get(i))
            {
                if (image.getPixelAt(x+monsterX, y+i) != Pixel.ONE)
                    return false;

            }
        }

        for (int i = 0; i < monsterPoints.size(); i++)
        {
            for (int monsterX : monsterPoints.get(i))
            {
                image.setPixelAt(x+monsterX, y+i, Pixel.TWO);
            }
        }

        return true;
    }
}
