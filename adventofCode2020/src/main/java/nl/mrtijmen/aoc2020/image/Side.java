package nl.mrtijmen.aoc2020.image;

public enum Side
{
    TOP(0),
    RIGHT(1),
    BOTTOM(2),
    LEFT(3);

    private int index;
    Side(int index){
        this.index = index;
    }
}
