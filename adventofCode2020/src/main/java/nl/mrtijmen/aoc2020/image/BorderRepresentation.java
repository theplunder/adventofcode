package nl.mrtijmen.aoc2020.image;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class BorderRepresentation
{
    private static final List<Integer> mask = IntStream.range(0, 10)
                                                       .map(i -> (int) Math.pow(2, i))
                                                       .boxed()
                                                       .collect(Collectors
                                                               .toList());

    private final Map<Side, List<Integer>> map = new HashMap<>();

    private final Map<Side, List<Integer>> originalMap;

    private List<Operation> history = new ArrayList<>();

    private final long id;

    private ImageTile imageTile;

    public BorderRepresentation(long id, List<List<Pixel>> pixels, ImageTile tile)
    {
        this.id = id;
        map.put(Side.TOP, getHashes(pixels.get(0)));
        map.put(Side.RIGHT, getHashes(pixels.get(1)));
        map.put(Side.BOTTOM, getHashes(pixels.get(2)));
        map.put(Side.LEFT, getHashes(pixels.get(3)));
        originalMap = new HashMap<>(map);

        this.imageTile = tile;
    }

    private List<Integer> getHashes(List<Pixel> pixels)
    {
        List<Integer> hashes = new ArrayList<>();
        int hash1 = 0;
        int hash2 = 0;
        for (int i = 0; i < pixels.size(); i++)
        {
            hash1 += mask.get(i) * pixels.get(i).getValue();
            hash2 += mask.get(i) * pixels.get(pixels.size() - i - 1).getValue();
        }
        hashes.add(hash1);
        hashes.add(hash2);
        return hashes;
    }

    public int getHashForSide(Side side)
    {
        return map.get(side).get(0);
    }

    public void flipHorizontal()
    {
        reverse(map.get(Side.LEFT));
        reverse(map.get(Side.RIGHT));

        var temp = map.put(Side.TOP, map.get(Side.BOTTOM));
        map.put(Side.BOTTOM, temp);

     //   imageTile.flipHorizontal();
        registerOperation(Operation.FLIP_H);
    }

    public void flipVertical()
    {
        reverse(map.get(Side.TOP));
        reverse(map.get(Side.BOTTOM));

        var temp = map.put(Side.LEFT, map.get(Side.RIGHT));
        map.put(Side.RIGHT, temp);

       // imageTile.flipVertical();
        registerOperation(Operation.FLIP_V);
    }

    //clockwise
    public void rotate()
    {
        var temp = map.put(Side.RIGHT, map.get(Side.TOP));
        reverse(temp);
        temp = map.put(Side.BOTTOM, temp);

        temp = map.put(Side.LEFT, temp);
        reverse(temp);
        map.put(Side.TOP, temp);
      //  imageTile.rotate();

        registerOperation(Operation.ROTATE);
    }

    private void reverse(List<Integer> hashpair)
    {
        int hash = hashpair.get(0);
        hashpair.set(0, hashpair.get(1));
        hashpair.set(1, hash);
    }

    private void registerOperation(Operation operation){
        if (map.equals(originalMap)){
            history = new ArrayList<>();
        }else {
            history.add(operation);
        }
    }

    public ImageTile replayHistory(ImageTile tile){
        for (Operation operation: history){
            switch (operation){
                case FLIP_H -> tile.flipHorizontal();
                case FLIP_V -> tile.flipVertical();
                case ROTATE -> tile.rotate();
            }
        }
        return tile;
    }

    public long getId()
    {
        return id;
    }

    public void print(){
        System.out.println( toString());
    }

    @Override
    public String toString()
    {
        String out = """ 
                         %s              
                %s  %s  %s
                         %s                
                """;
        return String.format(out, map.get(Side.TOP),map.get(Side.LEFT), id ,map.get(Side.RIGHT) ,map.get(Side.BOTTOM));

    }

    private enum Operation
    {
        ROTATE, FLIP_H, FLIP_V;
    }

}
