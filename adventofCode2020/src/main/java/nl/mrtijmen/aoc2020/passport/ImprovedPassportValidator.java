package nl.mrtijmen.aoc2020.passport;

import java.util.Map;

public class ImprovedPassportValidator implements PassportValidator
{
    @Override
    public boolean isValid(Map<PassportField, String> passportFields)
    {

        for(PassportField field: PassportField.values()){
            if (field == PassportField.COUNTRY_ID){
                continue;
            }

            if ((passportFields.get(field) == null) || passportFields.get(field) .isBlank())
            {
                return false;
            }
        }
        return true;
    }
}
