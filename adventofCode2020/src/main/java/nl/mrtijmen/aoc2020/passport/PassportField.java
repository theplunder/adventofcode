package nl.mrtijmen.aoc2020.passport;

public enum PassportField
{
    BIRTH_YEAR("byr", "Birth Year"),
    ISSUE_YEAR("iyr", "Issue Year"),
    EXPIRATION_YEAR("eyr", "Expiration Year"),
    HEIGHT("hgt", "Height"),
    HAIR_COLOR("hcl", "Hair Color"),
    EYE_COLOR("ecl", "Eye Color"),
    PASSPORT_ID("pid", "Passport ID"),
    COUNTRY_ID("cid", "Country ID");

    private final String code;
    private final String description;

    PassportField(String code, String description)
    {
        this.code = code;
        this.description = description;
    }

    public static PassportField fromCode(String code){
        return switch (code){
            case "byr" -> BIRTH_YEAR;
            case "iyr" -> ISSUE_YEAR;
            case "eyr" -> EXPIRATION_YEAR;
            case "hgt" -> HEIGHT;
            case "hcl" -> HAIR_COLOR;
            case "ecl" -> EYE_COLOR;
            case "pid" -> PASSPORT_ID;
            case "cid" -> COUNTRY_ID;
            default -> throw new RuntimeException("illegal Passportfield code: " + code);
        };
    }

    public String getCode()
    {
        return code;
    }

    public String getDescription()
    {
        return description;
    }
}
