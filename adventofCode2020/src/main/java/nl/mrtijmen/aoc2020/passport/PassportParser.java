package nl.mrtijmen.aoc2020.passport;

import java.util.List;

public class PassportParser
{

    public Passport parse(List<String> passportStrings){
        final var passport = new Passport();

        passportStrings.stream().filter(s->!s.isBlank()).forEach(field ->addField(passport, field));

        return passport;
    }

    private Passport addField(Passport passport, String fieldDesciption){
        String[] keyValue = fieldDesciption.strip().split(":");
        var field = PassportField.fromCode(keyValue[0]);

        passport.addField(field, keyValue[1]);
        return passport;
    }

}
