package nl.mrtijmen.aoc2020;

import nl.mrtijmen.aoc2020.rules.*;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;

import java.util.List;

public class Day19
{

    public static void main(String[] args) throws Exception
    {
        List<String> list = FileReaderUtil.getStringList("./src/main/resources/day19Input.txt");

        int split = list.indexOf("");
        List<String> rules = list.subList(0, split);
        List<String> messages = list.subList(split + 1, list.size());

        part2(rules, messages);

    }

    private static void part2(List<String> rules, List<String> messages)
    {
        for(int i = 0; i < rules.size(); i++){
            if (rules.get(i).startsWith("8:")){
                rules.set(i, "8: 42 | 42 8");
            }
            if (rules.get(i).startsWith("11:")){
                rules.set(i, "11: 42 31 | 42 11 31");
            }
        }
        part1(rules, messages);


    }

    private static void part1(List<String> rules, List<String> messages)
    {
        RuleFactory ruleFactory = new RuleFactory();
        ruleFactory.fromLines(rules);
        Rule rule0 = ruleFactory.getRuleById(0);

        long result = 0;
        int i = 0;
        for (String message : messages)
        {
            boolean matches = rule0.matches(message);
            System.out.println(i++ +":" + messages.size() + " -> " + message + " : " + matches);
            if (matches) result++;
        }

        System.out.println("Part1: " + result);
    }

    private static void simpleTest1()
    {
        Rule four = new FinalRule("a");
        Rule five = new FinalRule("b");
        Rule three = new OrRule(new AggregateRule(four, five), new AggregateRule(five, four));
        Rule two = new OrRule(new AggregateRule(four, four), new AggregateRule(five, five));
        Rule one = new OrRule(new AggregateRule(two, three), new AggregateRule(three, two));
        Rule zero = new TrippleAggregateRule(four, one, five);


        System.out.println(zero.matches("ababbb"));
        System.out.println(zero.matches("bababa"));
        System.out.println(zero.matches("abbbab"));
        System.out.println(zero.matches("aaabbb"));
        System.out.println(zero.matches("aaaabbb"));
    }

}
