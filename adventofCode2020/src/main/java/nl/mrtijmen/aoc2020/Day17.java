package nl.mrtijmen.aoc2020;

import nl.mrtijmen.aoc2020.pocketDimension.PocketDimension;
import nl.mrtijmen.aoc2020.pocketDimension.SimpleUpdateStrategy;
import nl.mrtijmen.aoc2020.pocketDimension.Status;
import nl.mrtijmen.aoc2020.pocketDimension.UpdateStrategy;
import nl.mrtijmen.aoc2020.pocketDimension.fourD.PocketDimension4D;
import nl.mrtijmen.aoc2020.pocketDimension.fourD.SimpleUpdateStrategy4D;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;

import java.util.List;

public class Day17
{
    public static void main(String[] args) throws Exception
    {
        List<String> list = FileReaderUtil.getStringList("./src/main/resources/day17Input.txt");
    //    part1(list);


        PocketDimension4D pocketDimension4d = PocketDimension4D.fromTokenList(list);
        SimpleUpdateStrategy4D updateStrategy4d = new SimpleUpdateStrategy4D();
        pocketDimension4d.printArea();
        for (int i = 0; i <6; i++)
        {
            pocketDimension4d = updateStrategy4d.apply(pocketDimension4d);
//            System.out.println("---------------------");
//            pocketDimension4d.printArea();
        }

        int activeCount4d = pocketDimension4d.countPositionsWith(Status.ACTIVE);
        System.out.println("Part2: active regions -> " + activeCount4d);
    }

    private static void part1(List<String> list)
    {
        PocketDimension pocketDimension = PocketDimension.fromTokenList(list);
        UpdateStrategy updateStrategy = new SimpleUpdateStrategy();
        pocketDimension.printArea();

        for (int i = 0; i < 1; i++)
        {
            pocketDimension = updateStrategy.apply(pocketDimension);
//            System.out.println("---------------------");
//            nl.mrtijmen.aoc2020.pocketDimension.printArea();
        }

        int activeCount = pocketDimension.countPositionsWith(Status.ACTIVE);
        System.out.println("Part1: active regions -> " + activeCount);
    }
}
