package nl.mrtijmen.aoc2020;

import nl.mrtijmen.aoc2020.seating.*;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;

import java.util.List;

public class Day11
{
    public static void main(String[] args) throws Exception
    {
        List<String> list = FileReaderUtil.getStringList("./src/main/resources/day11Input.txt");

        int seatCounter = solve(list, new SimpleSeatingStrategy());
        System.out.println("Part 1: end Occumpied seats->" + seatCounter);

        int seatCounter2 = solve(list, new SmartSeatingStrategy());
        System.out.println("Part 2: end Occumpied seats->" + seatCounter2);

    }

    private static int solve(List<String> list, SeatingStrategy strategy)
    {
        SeatingArea area = SeatingArea.fromTokenList(list);
    //    area.printArea();
        int maxLoop = 100;
        int counter = 0;

        do{
            area = strategy.apply(area);
         //   area.printArea();
        //    System.out.println();
            if (counter++ > maxLoop) {
           //     System.out.println("protected against infinite loop");
                break;}
        }
        while (strategy.isStateChanged());

       return area.countSeatsWith(Feature.OCCUPIED_SEAT);
    }



}
