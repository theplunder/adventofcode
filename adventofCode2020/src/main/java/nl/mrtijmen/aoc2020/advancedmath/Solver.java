package nl.mrtijmen.aoc2020.advancedmath;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Solver
{
    private final Evaluator evaluator;
    public static final Pattern PATTERN = Pattern.compile("\\(([^\\(\\)]+)\\)");

    public Solver(Evaluator evaluator)
    {
        this.evaluator = evaluator;
    }

    public long solve(String expression)
    {
        //System.out.println(expression);
        while (expression.contains("("))
        {
            Matcher matcher = PATTERN.matcher(expression);
            expression = matcher.replaceAll(matchResult -> {
                String group = matchResult.group();
                return String.valueOf(evaluator.evaluate(group.substring(1, group.length() - 1)));
            });
          //  System.out.println(expression);
        }

        return evaluator.evaluate(expression);
    }
}
