package nl.mrtijmen.aoc2020.advancedmath;

public interface Evaluator
{
    long evaluate(String expression);
}
