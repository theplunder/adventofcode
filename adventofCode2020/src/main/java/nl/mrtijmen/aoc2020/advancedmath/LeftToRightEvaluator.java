package nl.mrtijmen.aoc2020.advancedmath;

import java.util.Arrays;
import java.util.stream.Collectors;

public class LeftToRightEvaluator implements Evaluator
{
    public long evaluate(String sum)
    {
        var operators = Arrays.stream(sum.split("\\d+"))
                              .filter(s -> !s.isBlank())
                              .map(String::strip)
                              .collect(Collectors.toList());
        var numbers = Arrays.stream(sum.split("[+\\-*]"))
                            .map(String::strip)
                            .map(Long::parseLong)
                            .collect(Collectors.toList());

        long runningSum = numbers.get(0);
        for (int i = 0; i < operators.size(); i++)
        {
            long num = numbers.get(i + 1);
            runningSum = solve(runningSum, operators.get(i), num);


        }
        return runningSum;
    }

    private static long solve(long runningSum, String operator, long num)
    {

        return switch (operator)
                {
                    case "+" -> runningSum + num;
                    case "-" -> runningSum - num;
                    case "*" -> runningSum * num;
                    default -> throw new IllegalStateException("Unexpected value: " + operator);
                };

    }
}
