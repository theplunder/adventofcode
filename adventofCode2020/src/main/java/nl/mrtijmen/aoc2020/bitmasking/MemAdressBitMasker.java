package nl.mrtijmen.aoc2020.bitmasking;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class MemAdressBitMasker
{

    private final String bitMask;

    public MemAdressBitMasker(String bitMask)
    {
        this.bitMask = bitMask;
    }

    public List<Long> apply(long number)
    {
        String binaryString = Long.toBinaryString(number);
        binaryString = "0".repeat(bitMask.length() - binaryString.length()) + binaryString;
        char[] numberArray = binaryString.toCharArray();
        char[] bitMaskArray = bitMask.toCharArray();

        //preprocess
        applyMask(numberArray, bitMaskArray);

        List<char[]> exploded = explode(numberArray);
        return exploded.stream().map(array -> Long.parseLong(String.valueOf(array),2)).collect(Collectors.toList());

    }

    private void applyMask(char[] numberArray, char[] bitMaskArray)
    {
        for (int i = 0; i < numberArray.length; i++ ){
            if (bitMaskArray[i] != '0')
            {
                numberArray[i] = bitMaskArray[i];
            }
        }
    }

    public List<char[]> explode (char[] bitmask){
        for (int i = 0; i < bitmask.length; i++ ){
            if (bitmask[i] =='X'){
                char[] bitmask0 = Arrays.copyOf(bitmask, bitmask.length);
                char[] bitmask1 = Arrays.copyOf(bitmask, bitmask.length);
                bitmask0[i]='0';
                bitmask1[i]='1';
                List<char[]> list = explode(bitmask0);
                list.addAll(explode(bitmask1));
                return list;
            }
        }
        List<char[]> result = new ArrayList<>();
        result.add(bitmask);
        return result;
    }


    private void print(char[] array){
        for(char c: array){
            System.out.print(c);
        }
        System.out.println();

    }

}
