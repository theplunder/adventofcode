package nl.mrtijmen.aoc2020;

import nl.mrtijmen.aoc2020.hexagon.HexagonCoordinate;
import nl.mrtijmen.aoc2020.hexagon.HexagonDirection;
import nl.mrtijmen.aoc2020.hexagon.HexagonGameOfLife;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;

import java.util.*;
import java.util.stream.Collectors;

public class Day24
{

    public static void main(String[] args) throws Exception
    {
        List<String> list = FileReaderUtil.getStringList("./src/main/resources/day24Input.txt");
        //List<String> list = FileReaderUtil.getStringList("./src/main/resources/day24InputExample.txt");
        List<List<HexagonDirection>> instructions = list.stream().map(Day24::parseInstructions).toList();
        List<HexagonCoordinate> results = instructions.stream()
                                                      .map(instr -> followPath(new HexagonCoordinate(0, 0), instr))
                                                      .toList();

        HashMap<HexagonCoordinate, Integer> count = new HashMap<>();
        for (HexagonCoordinate coordinate: results){
            if (count.containsKey(coordinate)){
                count.replace(coordinate, count.get(coordinate)+1);
            }
            else {
                count.put(coordinate, 1);
            }
        }

        long result = count.values().stream().filter(i -> i%2!=0).count();
        System.out.println("Result Part 1: " + result);


        Set<HexagonCoordinate> coordinates = count.entrySet().stream().filter(e->e.getValue()%2!=0).map(Map.Entry::getKey).collect(Collectors.toSet());
        System.out.println("Result Part 1: " + coordinates.size());

        HexagonGameOfLife gameOfLife = new HexagonGameOfLife(coordinates);
        System.out.println(gameOfLife.blackTileCount());
        int days=100;
        for( int i =1 ; i <= days; i++)
        {
            System.out.println("Day " + i);
            gameOfLife = gameOfLife.calculateNewState();
            System.out.println(gameOfLife.blackTileCount());
        }
    }


    private static List<HexagonDirection> parseInstructions(String line)
    {
        List<HexagonDirection> instructions = new ArrayList<>();

        System.out.println(line);
        for (int i = 0; i < line.length(); i++)
        {
            char first = line.charAt(i);
            String directionCode;
            if ((first == 'n' || first == 's') && i != line.length() - 1)
            {
                //peek
                char second = line.charAt(i+1);
                if (second == 'e' || second == 'w')
                {
                    //valid
                    directionCode = Character.toString(first) + Character.toString(second);
                    i++;
                } else
                {
                    directionCode = Character.toString(first);

                }
            } else
            {
                directionCode = Character.toString(first);
            }
           // System.out.println(directionCode);
            instructions.add(HexagonDirection.fromCode(directionCode));
        }

        return instructions;
    }


    private static HexagonCoordinate followPath(HexagonCoordinate startingPoint, List<HexagonDirection> instructions)
    {
        HexagonCoordinate result = startingPoint;
        for (HexagonDirection direction : instructions)
        {
            result = result.move(direction);
        }
        return result;
    }


}
