package nl.mrtijmen.aoc2020.sledding;

public enum SlopeFeature {
	SNOW('.'),
	TREE('#');

	char token;
	SlopeFeature(char token){
		this.token = token;
	}

	static SlopeFeature fromToken(char token){
		return switch (token)
				{
					case '.' -> SNOW;
					case '#' -> TREE;
					default -> null;
				};
	}

	@Override
	public String toString() {
		return String.valueOf(token);
	}
}
