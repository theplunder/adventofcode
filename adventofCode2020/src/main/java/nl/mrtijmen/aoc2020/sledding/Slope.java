package nl.mrtijmen.aoc2020.sledding;

import nl.mrtijmen.aoc2020.location.Position;

import java.util.ArrayList;
import java.util.List;

public class Slope {

	private final int rangeX;
	private final int rangeY;
	private final List<List<SlopeFeature>> slope;

	public Slope(List<String> list)
	{
		rangeY = list.size();
		rangeX = list.get(0).length();
		slope = new ArrayList<>();

		for (String slopeLineString:list){
			List<SlopeFeature> slopeLine = new ArrayList<>();

			for(char token: slopeLineString.toCharArray()){
				slopeLine.add(SlopeFeature.fromToken(token));
			}
			slope.add(slopeLine);
		}
	}

	public SlopeFeature featureAt(Position position){
		int x = position.x()%rangeX;
		int y = position.y();
		return slope.get(y).get(x);
	}

	public int getRangeX() {
		return rangeX;
	}

	public int getRangeY() {
		return rangeY;
	}

	public void printSlope(){
		slope.stream().forEach( line -> {
					line.stream().forEach(System.out::print);
					System.out.println();
				}
		);
	}

}
