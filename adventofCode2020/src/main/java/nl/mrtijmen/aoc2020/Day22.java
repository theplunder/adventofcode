package nl.mrtijmen.aoc2020;

import nl.mrtijmen.aoc2020.cardgame.Card;
import nl.mrtijmen.aoc2020.cardgame.CardStack;
import nl.mrtijmen.aoc2020.cardgame.Player;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;

import java.util.*;

import static nl.mrtijmen.aoc2020.cardgame.Player.PLAYER1;
import static nl.mrtijmen.aoc2020.cardgame.Player.PLAYER2;

public class Day22
{
    static int depth = 0;
    private static Set<String> deckStringsP1 = new HashSet<>();
    private static Set<String> deckStringsP2 = new HashSet<>();

//    public static void playGame1(Map<Player, CardStack> decks)
//    {
//        CardStack player1Deck = decks.get(PLAYER1);
//        CardStack player2Deck = decks.get(PLAYER2);
//
//        while (!player1Deck.isEmpty() && !player2Deck.isEmpty())
//        {
//            Card player1Card = player1Deck.pop();
//            Card player2Card = player2Deck.pop();
//            if (player1Card.value() > player2Card.value())
//            {
//                player1Deck.addToBottom(player1Card);
//                player1Deck.addToBottom(player2Card);
//            } else
//            {
//                player2Deck.addToBottom(player2Card);
//                player2Deck.addToBottom(player1Card);
//            }
//           // System.out.println("Sizes: " + player1Deck.getSize() + ", " + player2Deck.getSize());
//        }
//
//
////        System.out.println(player1Deck.toString());
////        System.out.println(player2Deck.toString());
//
//        //who wins?
//
//        CardStack winnerStack = player1Deck.isEmpty() ? player2Deck : player1Deck;
//
//        //calculate points
//        int score = 0;
//        int multiplier = winnerStack.getSize();
//        while (!winnerStack.isEmpty())
//        {
//            score += winnerStack.pop().value() * multiplier;
//            multiplier--;
//        }
//        System.out.println("Score: " + score);
//
//
//    }

    public static void main(String[] args) throws Exception
    {
        //   List<String> list = FileReaderUtil.getStringList("./src/main/resources/day22InputExample.txt");
          List<String> list = FileReaderUtil.getStringList("./src/main/resources/day22Input.txt");
        //    List<String> list = FileReaderUtil.getStringList("./src/main/resources/day22InputExampleSanityCheck.txt");


        Map<Player, CardStack> playerDecks = new HashMap<>();

        CardStack deck = new CardStack();
        Player player = null;

        for (String input : list)
        {
            if (input.strip().isEmpty())
            {
                continue;
            }
            if (input.startsWith("Player"))
            {
                deck = new CardStack();
                if (player == null)
                {
                    player = PLAYER1;
                } else
                {
                    player = PLAYER2;
                }

                playerDecks.put(player, deck);
                continue;
            }
            deck.addToBottom(new Card(Integer.parseInt(input)));
        }

        System.out.println(playerDecks.get(PLAYER1).toString());
        System.out.println(playerDecks.get(PLAYER2).toString());

        //playGame1(playerDecks);
        playGame2(playerDecks);

    }

    public static void playGame2(Map<Player, CardStack> decks)
    {
        CardStack player1Deck = decks.get(PLAYER1);
        CardStack player2Deck = decks.get(PLAYER2);

        Player winner = playGame(player1Deck, player2Deck);
        //start a match
        CardStack winnerStack = winner == PLAYER1 ? player1Deck : player2Deck;

        //calculate points
        int score = 0;
        int multiplier = winnerStack.getSize();
        while (!winnerStack.isEmpty())
        {
            score += winnerStack.pop().value() * multiplier;
            multiplier--;
        }
        System.out.println("Score: " + score);

    }

    private static Player playGame(CardStack player1Deck, CardStack player2Deck)
    {
        depth++;
        System.out.println("going in depth: " + depth + " sizes: " + player1Deck.getSize() + ", " + player2Deck.getSize());
        Set<String> deckStringsP1 = new HashSet<>();
        while (!player1Deck.isEmpty() && !player2Deck.isEmpty())
        {
            //avoid infinite loop
            String p1String = player1Deck.toString() + player2Deck.toString();
            //   String p2String = player2Deck.toString();
            if (deckStringsP1.contains(p1String))// || deckStringsP2.contains(p2String))
            {
                depth--;
                System.out.println("avoid infinite loop");
                return PLAYER1;
            }
            deckStringsP1.add(p1String);
            //deckStringsP2.add(p2String);


            Card player1Card = player1Deck.pop();
            Card player2Card = player2Deck.pop();


            //check if we need a sub-match
            if (player1Card.value() <= player1Deck.getSize()
             && player2Card.value() <= player2Deck.getSize())
            {

                var substackP1 = player1Deck.subStack(player1Card.value());
                var substackP2 = player2Deck.subStack(player2Card.value());
//                System.out.println("---subgame---");
                //                System.out.println(player1Card.value() + " "  + substackP1);
//                System.out.println(player2Card.value() + " "  + substackP2);

                Player subWinner = playGame(substackP1, substackP2);



               if (subWinner == PLAYER1)
                {
                    player1Deck.addToBottom(player1Card);
                    player1Deck.addToBottom(player2Card);
                } else
                {
                    player2Deck.addToBottom(player2Card);
                    player2Deck.addToBottom(player1Card);
                }

            } else
            {
                if (player1Card.value() > player2Card.value())
                {
                    player1Deck.addToBottom(player1Card);
                    player1Deck.addToBottom(player2Card);
                } else
                {
                    player2Deck.addToBottom(player2Card);
                    player2Deck.addToBottom(player1Card);
                }
                //  System.out.println("Sizes: " + player1Deck.getSize() + ", " + player2Deck.getSize());
            }

        }
//        System.out.println(player1Deck);
//        System.out.println(player2Deck);

        depth--;
        return player1Deck.isEmpty() ? PLAYER2 : PLAYER1;
    }


}
