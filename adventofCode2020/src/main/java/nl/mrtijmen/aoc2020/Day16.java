package nl.mrtijmen.aoc2020;

import nl.mrtijmen.aoc2020.tickets.Field;
import nl.mrtijmen.aoccommons.util.FileReaderUtil;

import java.util.*;
import java.util.stream.Collectors;

public class Day16
{
    public static void main(String[] args) throws Exception
    {
        List<String> list = FileReaderUtil.getStringList("./src/main/resources/day16Input.txt");

        List<Field> fields = new ArrayList<>();
        List<Integer> myTicket = new ArrayList<>();
        List<List<Integer>> otherTickets = new ArrayList<>();

        int state = 0;
        for (String line : list)
        {
            if (line.isBlank())
            {
                state++;
                continue;
            }
            if (line.strip().equals("your ticket:") || line.strip().equals("nearby nl.mrtijmen.aoc2020.tickets:"))
            {
                continue;
            }
            if (state == 0)
            {
                fields.add(Field.fromString(line));
            } else if (state == 1)
            {
                myTicket = Arrays.stream(line.split(",")).map(Integer::parseInt).collect(Collectors.toList());
                System.out.println(myTicket);
            } else //state == 3
            {
                otherTickets.add(Arrays.stream(line.split(",")).map(Integer::parseInt).collect(Collectors.toList()));
            }
        }

        solvePart1(fields, otherTickets);

        solvePart2(fields, myTicket, otherTickets);

    }

    private static void solvePart1(List<Field> fields, List<List<Integer>> otherTickets)
    {

        int errorRate = otherTickets.stream()
                                    .flatMap(Collection::stream)
                                    .filter(i -> !isValidForAnyField(fields, i))
                                    .mapToInt(i -> i)
                                    .sum();
    }

    private static boolean isValidForAnyField(List<Field> fields, int i)
    {
        for (Field field : fields)
        {
            if (field.isInRange(i))
            {
                return true;
            }
        }
        return false;
    }

    private static boolean ticketValidForAnyField(List<Field> fields, List<Integer> ticket)
    {
        for (Integer number : ticket)
        {
            if (!isValidForAnyField(fields, number))
            {
                return false;
            }
        }
        return true;
    }


    private static void solvePart2(List<Field> fields, List<Integer> myTicket, List<List<Integer>> otherTickets)
    {
        //discard invalid nl.mrtijmen.aoc2020.tickets
        otherTickets = otherTickets.stream()
                                   .filter(list -> ticketValidForAnyField(fields, list))
                                   .collect(Collectors.toList());

        //"rotate nl.mrtijmen.aoc2020.tickets"
        List<List<Integer>> collumns = rotate(otherTickets);
        List<Field> lefOverFields = new ArrayList<>(fields);

        Map<Field, Integer> resultMap = new HashMap<>();

        boolean change = true;
        while (change)
        {
            change = false;
            for (int i = 0; i < collumns.size(); i++)
            {
                List<Integer> collumn = collumns.get(i);
                List<Field> lefOver = matchPossibleFields(lefOverFields, collumn);
                if (lefOver.size() == 1)
                {
                    Field field = lefOver.get(0);
                    lefOverFields.remove(field);
                    resultMap.put(field, i);
                    change = true;
                }
            }

        }
        long result = 1;
        for(var entry: resultMap.entrySet()){
            if (!entry.getKey().name().startsWith("departure")){
                continue;
            }
            System.out.println( entry.getKey().name() + ": " + myTicket.get(entry.getValue()));
            result*= (long)myTicket.get(entry.getValue());

        }
        System.out.println("solution part 2 ->" + result);


    }

    private static List<List<Integer>> rotate(List<List<Integer>> otherTickets)
    {
        List<List<Integer>> collumns = new ArrayList<>();
        //setup
        for (Integer list : otherTickets.get(0))
        {
            collumns.add(new ArrayList<>());
        }

        int jRange = otherTickets.get(0).size();

        for (List<Integer> otherTicket : otherTickets)
        {
            for (int j = 0; j < jRange; j++)
            {
                collumns.get(j).add(otherTicket.get(j));
            }
        }
        return collumns;
    }

    private static List<Field> matchPossibleFields(List<Field> fields, List<Integer> numbers)
    {

        List<Field> leftOver = fields.stream()
                                     .filter(field -> fieldRangeMaches(field, numbers))
                                     .collect(Collectors.toList());

        return leftOver;
    }

    private static boolean fieldRangeMaches(Field field, List<Integer> numbers)
    {
        for (Integer number : numbers)
        {
            if (!field.isInRange(number))
            {
                return false;
            }
        }
        return true;
    }

}
