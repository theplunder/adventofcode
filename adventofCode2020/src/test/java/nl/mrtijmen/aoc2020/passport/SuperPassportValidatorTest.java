package nl.mrtijmen.aoc2020.passport;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class SuperPassportValidatorTest
{
    @Test
    void testValidLength(){
        var validator = new SuperPassportValidator();
        assertAll(
                () -> assertTrue(validator.validateHeight("60in"), "60in"),
                () -> assertTrue(validator.validateHeight("190cm"), "190cm"),
                () -> assertFalse(validator.validateHeight("190in"), "190in"),
                () -> assertFalse(validator.validateHeight("190"), "190"),
                () -> assertFalse(validator.validateHeight("aa60in"), "aa60in"),
                () -> assertFalse(validator.validateHeight("190cmad"), "190cmad")

        );
    }

    @Test
    void testValidNumber(){
        var validator = new SuperPassportValidator();
        assertAll(
                () -> assertTrue(validator.validateNumber("60" , 59, 79), "60"),
                () -> assertTrue(validator.validateNumber("59" , 59, 79), "59"),
                () -> assertTrue(validator.validateNumber("79" , 59, 79), "79"),
                () -> assertFalse(validator.validateNumber("58" , 59, 79), "58"),
                () -> assertFalse(validator.validateNumber("80" , 59, 79), "80"),
                () -> assertFalse(validator.validateNumber("12.42", 10, 20), "12.42"),
                () -> assertFalse(validator.validateNumber("12,42", 10, 20), "12.42"),
                () -> assertFalse(validator.validateNumber("konijn", 10, 20), "konijn")


        );
    }

    @Test
    void testValidHairColor()
    {
        var validator = new SuperPassportValidator();
        assertAll(
                () -> assertTrue(validator.validateHairColor("#123abc"), "123abc"),
                () -> assertTrue(validator.validateHairColor("#1030bc"), "1030bc"),
                () -> assertFalse(validator.validateHairColor("#123abz"), "#123abz"),
                () -> assertFalse(validator.validateHairColor("123abc"), "123abc"),
                () -> assertFalse(validator.validateHairColor("aa1#23abc"), "aa1#23abc"),
                () -> assertFalse(validator.validateHairColor("#123abcd"), "#123abcd")
        );
    }


}
