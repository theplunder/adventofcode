package nl.mrtijmen.aoc2020.boarding;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class BoardingPassTest
{

    //BFFFBBFRRR: row 70, column 7, seat ID 567.
    @Test
    void correctSeating1(){
        BoardingPass pass = new BoardingPass("BFFFBBFRRR");
        assertThat(pass.getBoardingCode()).isEqualTo("BFFFBBFRRR");
        assertThat(pass.getRowNr()).isEqualTo(70);
        assertThat(pass.getSeatNr()).isEqualTo(7);
        assertThat(pass.getSeatId()).isEqualTo(567);
    }

    //FFFBBBFRRR: row 14, column 7, seat ID 119.
    @Test
    void correctSeating2(){
        BoardingPass pass = new BoardingPass("FFFBBBFRRR");
        assertThat(pass.getBoardingCode()).isEqualTo("FFFBBBFRRR");
        assertThat(pass.getRowNr()).isEqualTo(14);
        assertThat(pass.getSeatNr()).isEqualTo(7);
        assertThat(pass.getSeatId()).isEqualTo(119);
    }

    //BBFFBBFRLL: row 102, column 4, seat ID 820.
    @Test
    void correctSeating3(){
        BoardingPass pass = new BoardingPass("BBFFBBFRLL");
        assertThat(pass.getBoardingCode()).isEqualTo("BBFFBBFRLL");
        assertThat(pass.getRowNr()).isEqualTo(102);
        assertThat(pass.getSeatNr()).isEqualTo(4);
        assertThat(pass.getSeatId()).isEqualTo(820);
    }

}
