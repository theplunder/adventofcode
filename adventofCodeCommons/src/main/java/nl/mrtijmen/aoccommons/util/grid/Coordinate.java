package nl.mrtijmen.aoccommons.util.grid;

import java.util.List;

//Cartesian coordinate
//within our grid: y directions are reversed!
public record Coordinate(long x, long y)
{
    public static boolean UP_IS_NEGATIVE_Y = false;

    public List<Coordinate> findNeighbours()
    {
        return findNeighbours(false);
    }

    public List<Coordinate> findNeighbours(boolean includeDiagonals)
    {
        if (includeDiagonals)
        {
            return List.of(
                    up(),
                    new Coordinate(x + 1, y + 1),
                    right(),
                    new Coordinate(x + 1, y - 1),
                    down(),
                    new Coordinate(x - 1, y - 1),
                    left(),
                    new Coordinate(x - 1, y + 1));
        }
        return List.of(
                up(),
                right(),
                down(),
                left());

    }

    public Coordinate left()
    {
        return new Coordinate(x - 1, y);
    }

    public Coordinate up()
    {
        if (UP_IS_NEGATIVE_Y)
        {
            return new Coordinate(x, y - 1);
        }
        return new Coordinate(x, y + 1);

    }

    public Coordinate down()
    {
        if (UP_IS_NEGATIVE_Y)
        {
            return new Coordinate(x, y + 1);
        }
        return new Coordinate(x, y - 1);
    }

    public Coordinate right()
    {
        return new Coordinate(x + 1, y);
    }

    public Coordinate move(Direction direction)
    {
        return switch (direction)
        {
            case UP -> up();
            case DOWN -> down();
            case LEFT -> left();
            case RIGHT -> right();
        };
    }

    public Coordinate plus(Coordinate other)
    {
        return new Coordinate(x + other.x, y + other.y);
    }

    public Coordinate minus(Coordinate other)
    {
        return new Coordinate(x - other.x, y - other.y);
    }

}
