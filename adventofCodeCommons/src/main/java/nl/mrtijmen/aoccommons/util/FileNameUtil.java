package nl.mrtijmen.aoccommons.util;

public class FileNameUtil
{

    public static String dataFileName(){
        Class<?> caller = StackWalker.getInstance(StackWalker.Option.RETAIN_CLASS_REFERENCE).getCallerClass();
        String name = caller.getSimpleName();
        return String.format("./src/main/resources/%sInput.txt", name) ;
    }

    public static String exampleDataFileName(){
        Class<?> caller = StackWalker.getInstance(StackWalker.Option.RETAIN_CLASS_REFERENCE).getCallerClass();
        String name = caller.getSimpleName();
        return String.format("./src/main/resources/%sInputExample.txt", name) ;
    }

    public static String exampleDataFileName(int exampleCount){
        Class<?> caller = StackWalker.getInstance(StackWalker.Option.RETAIN_CLASS_REFERENCE).getCallerClass();
        String name = caller.getSimpleName();
        return String.format("./src/main/resources/%sInputExample%d.txt", name, exampleCount) ;
    }

}
