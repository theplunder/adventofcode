package nl.mrtijmen.aoccommons.util;

import java.io.*;
import java.util.List;

public class FileReaderUtil {

	public static List<Integer> getNumberList(String filePath) throws Exception {
		InputStream stream = getInputStream(filePath);

		List<Integer> result;
		try (BufferedReader in = new BufferedReader(new InputStreamReader(stream))) {
			result = in.lines().map(String::strip).map(Integer::parseInt).toList();
		}
		return result;
	}

	public static List<Long> getLongNumberList(String filePath) throws Exception {
		InputStream stream = getInputStream(filePath);

		List<Long> result;
		try (BufferedReader in = new BufferedReader(new InputStreamReader(stream))) {
			result = in.lines().map(String::strip).map(Long::parseLong).toList();
		}
		return result;
	}

	public static List<String> getStringList(String filePath) throws Exception {
		InputStream stream = getInputStream(filePath);

		List<String> result;
		try (BufferedReader in = new BufferedReader(new InputStreamReader(stream))) {
			result = in.lines().map(String::strip).toList();
		}
		return result;
	}

	private static InputStream getInputStream(String filePath){
		File file = null;
		try {
			file = new File(filePath);
			return new FileInputStream(file);
		}
		catch (FileNotFoundException e) {
			System.out.println(file.getAbsolutePath());
			throw new RuntimeException(e);
		}
	}
}
