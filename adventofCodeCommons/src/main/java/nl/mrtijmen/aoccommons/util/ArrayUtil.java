package nl.mrtijmen.aoccommons.util;

import nl.mrtijmen.aoccommons.util.grid.Coordinate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;

public class ArrayUtil {
    /**
     * Rotates clockwise
     */
    public static <T> List<List<T>> rotate(List<List<T>> matrix) {
        List<List<T>> modified = new ArrayList<>();
        //setup
        int yRange = matrix.size();
        int xRange = matrix.get(0).size();


        //setup proper shape
        for (int i = 0; i < xRange; i++) {
            modified.add(i, new ArrayList<>(Collections.nCopies(yRange, matrix.get(0).get(0))));
        }


        for (int i = 0; i < yRange; i++) {
            for (int j = 0; j < xRange; j++) {
                T value = matrix.get(i).get(j);
                modified.get(j).set(yRange - i - 1, value);
            }
        }
        return modified;
    }

    public static <T> List<List<T>> flipVertical(List<List<T>> matrix) {
        List<List<T>> modified = new ArrayList<>(matrix);
        //setup
        int yRange = matrix.size();

        for (int i = 0; i < yRange; i++) {
            modified.set(i, new ArrayList<>(matrix.get(i)));
        }

        int xRange = matrix.get(0).size();

        for (int i = 0; i < yRange; i++) {
            for (int j = 0; j < xRange; j++) {
                T value = matrix.get(i).get(j);
                modified.get(i).set(yRange - j - 1, value);
            }
        }
        return modified;
    }

    public static <T> List<List<T>> flipHorizontal(List<List<T>> matrix) {
        List<List<T>> modified = new ArrayList<>(matrix);
        //setup
        int yRange = matrix.size();

        for (int i = 0; i < yRange; i++) {
            modified.set(i, new ArrayList<>(matrix.get(i)));
        }
        int xRange = matrix.get(0).size();

        for (int i = 0; i < yRange; i++) {
            for (int j = 0; j < xRange; j++) {
                T value = matrix.get(i).get(j);
                modified.get(yRange - 1 - i).set(j, value);
            }
        }
        return modified;
    }

    public static <OUT, IN> List<List<OUT>> convert(List<List<IN>> matrix, Function<IN, OUT> converter) {

        List<List<OUT>> modified = new ArrayList<>();
        for (List<IN> ins : matrix) {
            List<OUT> newLine = new ArrayList<>();
            modified.add(newLine);
            int xRange = matrix.get(0).size();
            for (int j = 0; j < xRange; j++) {
                IN value = ins.get(j);
                OUT converted = converter.apply(value);
                newLine.add(j, converted);
            }
        }

        return modified;
    }


    public static <OUT, IN> List<List<OUT>> poitionAwareConvert(List<List<IN>> matrix, BiFunction<IN, Coordinate, OUT> converter) {
        int xSize = matrix.get(0).size();
        int ySize = matrix.size();
        List<List<OUT>> modified = new ArrayList<>();

        for (int i = 0; i < ySize; i++) {
            List<OUT> newLine = new ArrayList<>();
            modified.add(newLine);
            for (int j = 0; j < xSize; j++) {
                IN value = matrix.get(i).get(j);
                OUT converted = converter.apply(value, new Coordinate(j, i));
                newLine.add(j, converted);
            }
        }

        return modified;
    }

}
