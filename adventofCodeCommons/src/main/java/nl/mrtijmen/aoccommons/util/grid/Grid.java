package nl.mrtijmen.aoccommons.util.grid;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.*;
import java.util.stream.Collectors;

public class Grid<T>
{
    private final List<List<T>> gridMatrix;

    private final Map<Coordinate, T> objectMap = new HashMap<>();

    private final int xSize;
    private final int ySize;

    public Grid(int xSize, int ySize, Class<T> clazz)
    {
        this(xSize, ySize, (T) null);
    }

    public Grid(int xSize, int ySize, T defaultVal)
    {
        this.xSize = xSize;
        this.ySize = ySize;
        gridMatrix = new ArrayList<>();
        for (int i = 0; i < ySize; i++)
        {
            List<T> line = new ArrayList<>();
            gridMatrix.add(line);
            for (int j = 0; j < xSize; j++)
            {
                line.add(defaultVal);
                objectMap.put(new Coordinate(j, i), defaultVal);
            }
        }
    }


    public Grid(int xSize, int ySize, Function<Coordinate, T> initializer)
    {
        this.xSize = xSize;
        this.ySize = ySize;
        gridMatrix = new ArrayList<>();
        for (int i = 0; i < ySize; i++)
        {
            List<T> line = new ArrayList<>();
            gridMatrix.add(line);
            for (int j = 0; j < xSize; j++)
            {
                T value = initializer.apply(new Coordinate(j, i));
                line.add(value);
                objectMap.put(new Coordinate(j, i), value);
            }
        }
    }

    public Grid(List<List<T>> list)
    {
        //sanity check? all x-lists same size?
        this.xSize = list.get(0).size();
        this.ySize = list.size();

        gridMatrix = new ArrayList<>();
        for (int i = 0; i < ySize; i++)
        {
            List<T> line = new ArrayList<>(list.get(i));
            gridMatrix.add(line);
            for (int j = 0; j < xSize; j++)
            {
                objectMap.put(new Coordinate(j, i), line.get(j));
            }
        }
    }


    public T setValue(Coordinate coordinate, T value)
    {
        //sanity check? coordinate is < MAX_INT
        gridMatrix.get((int) coordinate.y()).set((int) coordinate.x(), value);
        return objectMap.put(coordinate, value);
    }

    public T getValue(Coordinate coordinate)
    {
        //sanity check? coordinate is < MAX_INT
        return objectMap.get(coordinate);
    }


    public T getValue(int x, int y)
    {
        //sanity check? coordinate is < MAX_INT
        return objectMap.get(new Coordinate(x, y));
    }


    public void visitAllCoordinates(BiConsumer<Coordinate, T> visitor)
    {
        int y = 0;
        for (List<T> line : gridMatrix)
        {
            int x = 0;
            for (T value : line)
            {
                visitor.accept(new Coordinate(x, y), value);
                x++;
            }
            y++;
        }
    }

    public void visitAndUpdateAllCoordinates(BiFunction<Coordinate, T, T> visitor)
    {
        for (int y = 0; y < ySize; y++)
        {
            List<T> line = gridMatrix.get(y);
            for (int x = 0; x < xSize; x++)
            {
                T oldValue = line.get(x);
                Coordinate coordinate = new Coordinate(x, y);
                T newValue = visitor.apply(coordinate, oldValue);
                setValue(coordinate, newValue);
            }
        }
    }

    public void update(Coordinate coordinate, UnaryOperator<T> operator)
    {
        T oldValue = gridMatrix.get((int) coordinate.y()).get((int) coordinate.x());
        T newValue = operator.apply(oldValue);
        setValue(coordinate, newValue);
    }

    public List<Coordinate> find(Predicate<T> condition)
    {
        return objectMap.entrySet().stream()
                        .filter(entry -> condition.test(entry.getValue()))
                        .map(Map.Entry::getKey)
                        .toList();
    }

    public List<Coordinate> find(BiPredicate<Coordinate, T> condition)
    {
        return objectMap.entrySet().stream()
                        .filter(entry -> condition.test(entry.getKey(), entry.getValue()))
                        .map(Map.Entry::getKey)
                        .toList();
    }

    public boolean coordinateWithinRange(Coordinate coordinate)
    {
        return coordinate.x() >= 0 && coordinate.y() >= 0 && coordinate.x() < xSize && coordinate.y() < ySize;
    }

    public long size()
    {
        return xSize * (long) ySize;
    }

    public int xSize()
    {
        return xSize;
    }

    public int ySize()
    {
        return ySize;
    }


    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        for (List<T> line : gridMatrix)
        {
            String lineString = line.stream().map(Object::toString).collect(Collectors.joining(" ", "", "\n"));
            builder.append(lineString);
        }
        return builder.toString();
    }
}
