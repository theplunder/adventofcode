package nl.mrtijmen.aoccommons.util.grid;

public record Line(Coordinate start, Coordinate end)
{
}
