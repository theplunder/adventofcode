package nl.mrtijmen.aoccommons.util;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MathUtil {

    public static double[] abc(double a, double b, double c) {

        //x = (-b +- sqrt(b^2 - 4ac) )/2a
        //TODO sometimes no solution
        double x1 = (-1 * b + Math.sqrt(b * b - 4 * a * c)) / (2 * a);
        double x2 = (-1 * b - Math.sqrt(b * b - 4 * a * c)) / (2 * a);

        return new double[]{x1, x2};
    }

    public static Map<Long, Long> primeFactors(long number) {
        Map<Long, Long> trace = new HashMap<>();

        for (long i = 2; i < number; i++) {
            while (number % i == 0) {
                trace.compute(i, (k, v) -> v == null ? 1 : v + 1);
                number = number / i;
            }
        }
        if (number > 2) {
            trace.put(number, 1l);
        }

        return trace;
    }


    public static Long leastCommonMultiple(long... numbers) {
        List<Map<Long, Long>> primeFactors = Arrays.stream(numbers).mapToObj(MathUtil::primeFactors).toList();

        Map<Long, Long> biggestFactors = new HashMap<>();

        for (Map<Long, Long> factors : primeFactors) {
            factors.forEach((key, value) -> {
                long number = key;
                biggestFactors.compute(number, (k, v) -> v == null ? value : Math.max(value, v));
            });
        }

        System.out.println(biggestFactors);

        long result = 1;
        for (Map.Entry<Long, Long> entry : biggestFactors.entrySet()) {
            result *= Math.round(Math.pow(entry.getKey(), entry.getValue()));
        }

        return result;
    }


    public record PrimeFactor(int num, int pow) {

        public double value() {
            return Math.pow(num, pow);
        }


    }


}
