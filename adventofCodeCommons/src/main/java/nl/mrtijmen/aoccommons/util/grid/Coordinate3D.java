package nl.mrtijmen.aoccommons.util.grid;

import java.util.List;

//Cartesian coordinate
public record Coordinate3D(long x, long y, long z)
{
    public List<Coordinate3D> findNeighbours()
    {
        return findNeighbours(false);
    }

    public List<Coordinate3D> findNeighbours(boolean includeDiagonals)
    {
        System.out.println("NOT FULLY IMPLEMENTED YET!");

        if (includeDiagonals)
        {
            return List.of(
                    new Coordinate3D(x, y + 1, z),
                    new Coordinate3D(x + 1, y + 1, z),
                    new Coordinate3D(x + 1, y, z),
                    new Coordinate3D(x + 1, y - 1, z),
                    new Coordinate3D(x, y - 1, z),
                    new Coordinate3D(x - 1, y - 1, z),
                    new Coordinate3D(x - 1, y, z),
                    new Coordinate3D(x - 1, y + 1, z));
        }
        return List.of(
                new Coordinate3D(x, y + 1, z),
                new Coordinate3D(x + 1, y, z),
                new Coordinate3D(x, y - 1, z),
                new Coordinate3D(x - 1, y, z));

    }

    /**
     * Increases z!
     *
     * @return
     */
    public Coordinate3D up()
    {
        return new Coordinate3D(x(), y(), z() + 1);
    }

    /**
     * Decreases z!
     *
     * @return
     */
    public Coordinate3D down()
    {
        return new Coordinate3D(x(), y(), z() - 1);
    }

}
